//
//  NotesManager.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/7/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import Foundation
import SwiftyJSON

struct  NotesManager  {
    
    let url = RKAPI.notesURL
    
    
    func setFinalArrayOfModels(data:Data) throws ->[Note] {
        let json = getJson(data: data)
        let parsedJsonData = generateDic(for: json)
    //    print(parsedJsonData)
      //  print(parsedJsonData)

        if let nonOptionalJsonData = parsedJsonData {
            do {
                let finalArrayOfModels = try notesModel(with: nonOptionalJsonData)
//                print(finalArrayOfModels)
                return finalArrayOfModels
                
            } catch Error.notesIsEmpty {
                throw Error.notesIsEmpty
            }
            
        } else {
            throw Error.notes
        }
    }
    
    private func notesInitializer (data:[String:Any]) -> Note {
        let title = data["Title"] as? String
        let description = data["Description"] as? String
        let creationDate = data["CreationDate"] as? String
        let labelId = data["LabelID"] as? String
        let noteId = data["NoteID"] as? String
        let category = data["Category"] as? Int
        let model = Note(title: title, description: description, creationDate: creationDate, labelId: labelId, noteId: noteId , category :category)
        return model
    }
    
    private  func getJson(data:Data) -> JSON {
        let json = JSON(data)
        return json
    }
    private   func generateDic(for jsonData:JSON) -> [[String:Any]]? {
        
        if let dic = jsonData.arrayObject as? [[String:Any]] {
            return dic
        }else {
            return nil
        }
    }
    private func notesModel(with dic:[[String:Any]]) throws -> [Note] {
        var models = [Note]()
        if !dic.isEmpty {
            let counter = dic.count - 1
            for i in 0...counter {
                let j = dic[i]
                let modeledData = notesInitializer(data: j)
                models.append(modeledData)
            }
            return models
        } else {
            throw Error.notesIsEmpty
        }
            
        
    }
}
