//
//  NotesModel.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/7/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import Foundation
class Note: NSObject, NSCoding {
    var title : String?
    var noteDescription :String?
    var creationDate :String?
    var labelId :String?
    var noteId :String?
    var category :Int?
    var reminderDate: Date?
    init(title: String?, description: String?, creationDate: String?, labelId: String?, noteId: String?, category: Int?) {
        self.title = title
        self.noteDescription = description
        self.creationDate = creationDate
        self.labelId = labelId
        self.noteId = noteId
        self.category = category
    }
    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(noteDescription, forKey: "description")
        coder.encode(creationDate, forKey: "creationDate")
        coder.encode(labelId, forKey: "labelId")
        coder.encode(noteId, forKey: "noteId")
        coder.encode(category, forKey: "category")
        coder.encode(reminderDate, forKey: "reminderDate")
    }
    required init?(coder: NSCoder) {
        title = coder.decodeObject(forKey: "title") as! String?
        noteDescription = coder.decodeObject(forKey: "description") as! String?
        creationDate = coder.decodeObject(forKey: "creationDate") as! String?
        labelId = coder.decodeObject(forKey: "labelId") as! String?
        noteId = coder.decodeObject(forKey: "noteId") as! String?
        category = coder.decodeObject(forKey: "category") as! Int?
        reminderDate = coder.decodeObject(forKey: "reminderDate") as! Date?
        super.init()
    }
}
