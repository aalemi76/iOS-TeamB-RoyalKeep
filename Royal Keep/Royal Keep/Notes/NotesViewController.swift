//
//  NotesViewController.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
let notesManager = NotesManager()
class NotesViewController: SharedViewController, Storyboarded {
    //MARK:- Variables
    var coordinator: NoteCoordinator?
    var notesModels = [Note]()
    var mainNotes = [Note]()
    let addButton = UIButton(type: .system)
    var labelsDictionary = [String: Label]()
    var labels = [Label]()
    var mainIndex: Int?
    let searchController = UISearchController(searchResultsController: nil)
    var searchResult = [Note]()
    var inSearchMode: Bool = false {
        didSet {
            searchResult.removeAll()
            tableView.reloadData()
        }
    }
    let editView = EditView()
    var editButton, selectButton, archiveButton: UIButton!
    var isAllSelected: Bool = false {
        willSet {
            if newValue {
                selectButton.setTitle("غیر فعال کردن همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                selectButton.setTitle("انتخاب همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            }
        }
    }
    var inEditingMode: Bool = false {
        willSet {
            addButton.isHidden = newValue
            let nav = navigationController as? RKNavigationController
            nav?.menuIcon.isHidden = newValue
            searchController.searchBar.isHidden = newValue
            archiveButton.isHidden = !newValue
            selectButton.isHidden = !newValue
            newValue ? editButton.setTitle("انصراف", for: .normal) : editButton.setTitle("ویرایش", for: .normal)
        }
    }
    var selectedItems = [Int]()
    //MARK:- Actions
    @objc func addButtonDidTap(sender: UIButton) {
        let vc = AddNewNoteViewController.instantiate()
        vc.labels = Array(labelsDictionary.values)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    @objc func didTapEditButton(sender: UIButton) {
        selectedItems.removeAll()
        isAllSelected = false
        inEditingMode = !inEditingMode
        tableView.reloadData()
    }
    @objc func didTapSelectButton(sender: UIButton) {
        guard notesModels.count != 0 else {return}
        if selectedItems.count < notesModels.count {
            selectedItems = Array(0..<notesModels.count)
            isAllSelected = true
            tableView.reloadData()
        } else {
            selectedItems.removeAll()
            isAllSelected = false
            tableView.reloadData()
        }
    }
    @objc func didTapArchiveButton(sender: UIButton) {
        guard selectedItems.count != 0 else {return}
        let ids = getNoteID()
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.notesSetCategory, method: .post, parameters: ["ItemIDs": ids, "Category": 2], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    func presentAlert() {
        let alert = UIAlertController(title: "افزودن به آرشیو", message: "یادداشت ها با موفقیت به آرشیو اضافه شد.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            self.inEditingMode = false
            self.request()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Controller Life Cycle
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyNotesLbl: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() { 
        super.viewDidLoad()
        sideMenuVC.delegate = self
        configureSideMenu()
        configureEditView()
        setViewComponents(forViews: [emptyNotesLbl ,tableView, addButton, editView])
        emptyNotesLbl.center = view.center
        configureButton()
        tableView.topAnchor.constraint(equalTo: editView.bottomAnchor).isActive = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: K.notesCellIdentifier)
        configureSearchController()
        getLabels()
        request()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    //MARK:- Helper Methods
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "یادداشت ها"
        navigationController.menuIcon.isHidden = false
        navigationController.addMenuIcon()
        navigationController.backIcon.isHidden = true
        navigationController.editIcon.isHidden = true
    }
    func configureButton() {
        addButton.setBackgroundImage(UIImage(systemName: "plus.circle.fill"), for: .normal)
        addButton.tintColor = GlobalSettings.shared().mainColor
        addButton.addTarget(self, action: #selector(addButtonDidTap(sender:)), for: .touchUpInside)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(addButton)
        NSLayoutConstraint.activate([
            addButton.widthAnchor.constraint(equalToConstant: 50),
            addButton.heightAnchor.constraint(equalToConstant: 50),
            addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)])
    }
    private func sortByDate(for array:[Note?]) ->[Note?] {
        let arrayForPass = array.sorted(by: {$0!.creationDate! < $1!.creationDate!})
        return arrayForPass
    }
    func configureEmptyNoteMessage(isHidden: Bool) {
        self.tableView.isHidden = !isHidden
        self.emptyNotesLbl.isHidden = isHidden
        self.emptyNotesLbl.text = "موردی برای نمایش یافت نشد"
        self.emptyNotesLbl.font = GlobalSettings.shared().boldSystemFont(size: 15)
        self.emptyNotesLbl.textColor = GlobalSettings.shared().darkGray
        self.emptyNotesLbl.textAlignment = .center
        self.emptyNotesLbl.sizeToFit()
    }
    func handleNotification() {
        guard let mainIndex = mainIndex else {return}
        let note = mainNotes[mainIndex]
        let index = notesModels.firstIndex(of: note)
        let indexPath = IndexPath(row: index!, section: 0)
        self.mainIndex = nil
        let label = labelsDictionary[note.labelId ?? ""]
        let labels = Array(labelsDictionary.values)
        coordinator?.toNoteDetailsView(self, noteId: note.noteId!, indexPath: indexPath, noteLabel: label, labels: labels, mainIndex: mainIndex)
    }
    //MARK:- Web API
    func request() {
        AF.request(notesManager.url , headers: HTTPHeaders(["username": User.globalUsername])).responseJSON { (data) in
            do {
                self.mainNotes = try notesManager.setFinalArrayOfModels(data: data.data!)
                self.mainNotes = self.mainNotes.sorted(by: {$0.creationDate! < $1.creationDate!})
                ReminderCollection.setReminder(noteList: self.mainNotes)
                ReminderCollection.update(noteList: self.mainNotes)
                self.notesModels = self.mainNotes.filter { (note: Note) -> Bool in
                    return note.category! != 2
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.configureEmptyNoteMessage(isHidden: !self.notesModels.isEmpty)
                    self.handleNotification()
                }
            }catch Error.notesIsEmpty {
                self.configureEmptyNoteMessage(isHidden: false)
            }
            catch {
                print(error)
            }
        }
    }
    func getLabels() {
        let url = RKAPI.labelsURL
        AF.request(url, method: .get, headers: RKAPI.username).responseString { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                let n = jsonObject.count
                for i in 0..<n {
                    if let title = jsonObject[i]["Title"].string, let color = jsonObject[i]["Color"].int, let labelId = jsonObject[i]["LabelID"].string {
                        let label = Label(Title: title, Color: color, LabelID: labelId)
                        self.labelsDictionary[label.LabelID] = label
                    }
                }
                self.labels = Array(self.labelsDictionary.values)
            case let .failure(error):
                print(error)
                
            }
        }
    }
    //MARK:- Edit View
    func configureEditView() {
        editView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(editView)
        NSLayoutConstraint.activate([
            editView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            editView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            editView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            editView.heightAnchor.constraint(equalToConstant: 40)])
        editButton = editView.editButton
        editButton.addTarget(self, action: #selector(didTapEditButton(sender:)), for: .touchUpInside)
        selectButton = editView.selectButton
        selectButton.isHidden = true
        selectButton.addTarget(self, action: #selector(didTapSelectButton(sender:)), for: .touchUpInside)
        editView.addArchiveButton()
        archiveButton = editView.archiveButton
        archiveButton.isHidden = true
        archiveButton.addTarget(self, action: #selector(didTapArchiveButton(sender:)), for: .touchUpInside)
    }
    func getNoteID() -> [String] {
        var ids = [String]()
        for i in selectedItems {
            ids.append(notesModels[i].noteId!)
        }
        return ids
    }
    //MARK:- Search Configurations
    func configureSearchController() {
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.definesPresentationContext = true
        searchController.searchBar.semanticContentAttribute = .forceRightToLeft
        searchController.searchBar.setValue("انصراف", forKey:"cancelButtonText")
        searchController.searchBar.tintColor = GlobalSettings.shared().mainColor
        searchController.searchBar.searchTextField.textAlignment = .right
        searchController.searchBar.searchTextField.font = GlobalSettings.shared().mediumSystemFont(size: 15)
        searchController.searchBar.searchTextField.textColor = GlobalSettings.shared().darkGray
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "جستجو"
        searchController.searchBar.searchBarStyle = .minimal
        tableView.tableHeaderView = searchController.searchBar
        tableView.tableHeaderView?.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        tableView.tableHeaderView?.layer.borderWidth = 1
    }
    func search() {
        searchResult = notesModels.filter { (note: Note) -> Bool in
            guard let rawTitle = searchController.searchBar.text else { return false }
            let title = rawTitle.trimmingCharacters(in: .whitespacesAndNewlines)
            guard let noteTitle = note.title else { return false }
            return noteTitle.lowercased().contains(title.lowercased())
        }
        tableView.reloadData()
    }
}
