//
//  NoteTableViewCell.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/7/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import UIKit
protocol NoteTableViewCellDelegate: class {
    func checkBoxButtonTouchUpInside(_ cell: NoteTableViewCell)
}
class NoteTableViewCell: UITableViewCell {
    //MARK: - Properties
    weak var delegate: NoteTableViewCellDelegate?
    let label = UIView()
    let reminder = UIImageView(image: UIImage(systemName: "bell.circle.fill"))
    var hasReminder: Bool = false {
        willSet {
            reminder.isHidden = !newValue
        }
    }
    var isCompleted: Bool = false {
        willSet {
            if newValue {
                checkBox.setBackgroundImage(UIImage(systemName: "checkmark.square"), for: .normal)
            } else {
                checkBox.setBackgroundImage(UIImage(systemName: "square"), for: .normal)
            }
        }
    }
    var inEditingMode: Bool = false {
        willSet {
            selectImage.isHidden = !newValue
        }
    }
    var isCellSelected: Bool = false {
        willSet {
            newValue ? (selectImage.image = UIImage(systemName: "checkmark.circle")) :  (selectImage.image = UIImage(systemName: "circle"))
        }
    }
    let selectImage = UIImageView(image: UIImage(systemName: "circle"))
    //MARK: - View Initis
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    @IBOutlet weak var checkBox: UIButton!
    @objc func checkBoxButtonDidTap(sender: UIButton) {
        isCompleted = !isCompleted
        delegate?.checkBoxButtonTouchUpInside(self)
    }
    //MARK: - Handlers
    override func awakeFromNib() {
        super.awakeFromNib()
        configureTitleLabel()
        configureReminder()
        configureLabel()
        configureSelectImage()
        configureDescriptionText()
        configureCheckBox()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configureReminder() {
        reminder.translatesAutoresizingMaskIntoConstraints = false
        reminder.tintColor = GlobalSettings.shared().mainColor
        NSLayoutConstraint.activate([
        reminder.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
        reminder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
        reminder.widthAnchor.constraint(equalToConstant: 30),
        reminder.heightAnchor.constraint(equalToConstant: 30)])
    }
    func configureLabel() {
        label.backgroundColor = GlobalSettings.shared().lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: reminder.centerXAnchor),
            label.bottomAnchor.constraint(equalTo: descriptionText.bottomAnchor),
            label.widthAnchor.constraint(equalToConstant: 30),
            label.heightAnchor.constraint(equalToConstant: 30)])
        label.layer.cornerRadius = 5
        label.layer.masksToBounds = true
        label.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        label.layer.borderWidth = 0.5
    }
    func configureTitleLabel() {
        addSubview(reminder)
        titleLabel.adjustsFontSizeToFitWidth = false
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = GlobalSettings.shared().boldSystemFont(size: 17)
        titleLabel.textColor = .black
        titleLabel.textAlignment = .right
        NSLayoutConstraint.activate([
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: reminder.trailingAnchor, constant: 15)])
    }
    func configureDescriptionText() {
        descriptionText.numberOfLines = 3
        descriptionText.adjustsFontSizeToFitWidth = false
        descriptionText.lineBreakMode = .byTruncatingTail
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.backgroundColor = .clear
        descriptionText.font = GlobalSettings.shared().systemFont(size: 15)
        descriptionText.textColor = GlobalSettings.shared().darkGray
        descriptionText.textAlignment = .right
        NSLayoutConstraint.activate([
            descriptionText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            descriptionText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            descriptionText.heightAnchor.constraint(equalToConstant: 90),
            descriptionText.leadingAnchor.constraint(equalTo: selectImage.trailingAnchor, constant: 15)])
    }
    func configureCheckBox() {
        checkBox.tintColor = GlobalSettings.shared().mainColor
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        addSubview(checkBox)
        NSLayoutConstraint.activate([
            checkBox.centerXAnchor.constraint(equalTo: reminder.centerXAnchor),
            checkBox.centerYAnchor.constraint(equalTo: centerYAnchor),
            checkBox.widthAnchor.constraint(equalToConstant: 30),
            checkBox.heightAnchor.constraint(equalToConstant: 30)])
        checkBox.addTarget(self, action: #selector(checkBoxButtonDidTap(sender:)), for: .touchUpInside)
    }
    func configureSelectImage() {
        selectImage.isHidden = true
        selectImage.tintColor = GlobalSettings.shared().mainColor
        selectImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(selectImage)
        NSLayoutConstraint.activate([
            selectImage.centerYAnchor.constraint(equalTo: label.centerYAnchor),
            selectImage.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 10),
            selectImage.widthAnchor.constraint(equalToConstant: 30),
            selectImage.heightAnchor.constraint(equalToConstant: 30)])
    }
}
