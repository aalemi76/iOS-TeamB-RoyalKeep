//
//  NoteDetailsViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/4/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol NoteDetailsViewControllerDelegate: class {
    func backIconTouchUpInisde(isEdited: Bool)
}
class NoteDetailsViewController: UIViewController, Storyboarded {
    //MARK:- Variables
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let scrollView = UIScrollView()
    let contentView = UIView()
    let dateLabel = UILabel()
    let reminderLabel = UILabel()
    let reminderButton = UIButton(type: .system)
    let titleLabel = UILabel()
    let noteTitle = UITextView()
    let descriptionLabel = UILabel()
    let noteDescription = UITextView()
    var viewLabel: RKLabelIndicator!
    let viewLabelTitle = UILabel()
    weak var coordinator: NoteCoordinator?
    var titleView: TitleView!
    var editIcon: UIImageView!
    var backIcon: UIImageView!
    var note: Note!
    var noteId: String!
    var noteLabel: Label?
    var indexPath: IndexPath!
    var mainIndex: Int!
    var labels = [Label]()
    var date: Date!
    var dueDate: Date? {
        willSet {
            if let newDate = newValue {
                reminderButton.setTitle(persianLongDF.string(from: newDate), for: .normal)
            } else {
                reminderButton.setTitle("بدون یادآور", for: .normal)
            }
        }
    }
    weak var delegate: NoteDetailsViewControllerDelegate?
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let persianDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .short
        formatter.dateFormat = "dd - MMMM - yyyy"
        return formatter
    }()
    let persianLongDF: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .long
        formatter.dateFormat = "dd - MMMM - yyyy - HH:mm"
        return formatter
    }()
    var isNoteEdited: Bool = false
    let archiveButton = UIButton(type: .system)
    //MARK:- Actions
    @objc func didTapEditIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        isNoteEdited = true
        let vc = AddNewNoteViewController.instantiate()
        vc.labels = labels
        vc.delegate = self
        vc.itemToEdit = note
        vc.indexPath = indexPath
        vc.noteLabel = noteLabel
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.backIconTouchUpInisde(isEdited: isNoteEdited)
        navigationController?.popViewController(animated: true)
    }
    @objc func didTapReminderButton(tapGestureRecognizer: UITapGestureRecognizer) {
        isNoteEdited = true
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { granted, error in
            if granted {
                print("User gave permission")
            } else {
                return
            }
        }
        let vc = CustomDatePickerViewController()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        if let dueDate = dueDate {
            vc.date = dueDate
        }
        present(vc, animated: true, completion: nil)
        vc.cancelCompletion = { picker in
            picker.dismiss(animated: true, completion: nil)
        }
        vc.removeCompletion = { picker in
            picker.dismiss(animated: true) {
                self.dueDate = nil
                self.note.reminderDate = nil
                ReminderCollection.removeNotification(note: self.note, indexPath: self.mainIndex)
                ReminderCollection.saveNotes()
            }
        }
        vc.doneCompletion = { picker, date in
            picker.dismiss(animated: true) {
                self.dueDate = date
                self.note.reminderDate = date
                ReminderCollection.scheduleNotification(note: self.note, indexPath: self.mainIndex)
                ReminderCollection.saveNotes()
            }
        }
    }
    @objc func didTapArchive(sender: UIButton) {
        isNoteEdited = true
        let category: Int
        note.category == 2 ? (category = 0) : (category = 2)
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.notesSetCategory, method: .post, parameters: ["ItemIDs": [self.note.noteId!], "Category": category], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                        self.note.category = 2
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    func presentAlert() {
        let alert = UIAlertController(title: "افزودن به آرشیو", message: "یادداشت با موفقیت به آرشیو ها اضافه شد.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            guard let tap = self.backIcon.gestureRecognizers?[0] as? UITapGestureRecognizer else {return}
            self.didTapBackIcon(tapGestureRecognizer: tap)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureScrollView()
        getNote()
        configureContentView()
        configureDateLabel()
        configureReminderComponent()
        configureTitleLabel()
        configureNoteTitle()
        configureViewLabel()
        configureDescriptionLabel()
        configureNoteDescription()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard note.reminderDate != nil else {return}
        ReminderCollection.notes.updateValue(note, forKey: mainIndex)
        ReminderCollection.saveNotes()
    }
    //MARK:- Web API
    func getNote() {
        guard let noteId = noteId else {return}
        if let note = ReminderCollection.getNote(indexPath: mainIndex) {
            self.dueDate = note.reminderDate
        }
        var activityView: RKActivityIndicatorView? = RKActivityIndicatorView()
        cofigureActivityIndicator(activityView: activityView!)
        activityView?.showSpinner()
        let url = RKAPI.noteDetailsURL
        AF.request(url, method: .get, parameters: ["NoteID": noteId], encoding: URLEncoding(destination: .queryString), headers: RKAPI.username).responseJSON { response in
            switch response.result {
            case .success:
                activityView?.showCheckmark()
                let jsonObject = JSON(response.data ?? Data())
                if let title = jsonObject["Title"].string, let noteId = jsonObject["NoteID"].string, let creationDate = jsonObject["CreationDate"].string {
                    let description = jsonObject["Description"].string
                    let labelId = jsonObject["LabelID"].string
                    let category = jsonObject["Category"].int
                    self.note = Note(title: title, description: description, creationDate: creationDate, labelId: labelId, noteId: noteId, category: category)
                    self.note.reminderDate = self.dueDate
                    DispatchQueue.main.async {
                        self.date = self.dateFormatter.date(from: self.note.creationDate!)
                        self.dateLabel.text = self.persianDateFormatter.string(from: self.date)
                        self.noteTitle.text = self.note.title
                        self.noteDescription.text = self.note.noteDescription
                        self.configureArchiveButton()
                        if self.noteDescription.text == "" {
                            self.noteDescription.text = "-"
                        }
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        activityView?.dismiss()
                        activityView?.removeFromSuperview()
                        activityView = nil
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.title = "جزيیات یادداشت"
        navigationController.menuIcon.isHidden = true
        navigationController.addBackIcon()
        backIcon = navigationController.backIcon
        let backTap = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon(tapGestureRecognizer:)))
        backIcon.addGestureRecognizer(backTap)
        navigationController.addEditIcon()
        editIcon = navigationController.editIcon
        let editTap = UITapGestureRecognizer(target: self, action: #selector(didTapEditIcon(tapGestureRecognizer:)))
        editIcon.addGestureRecognizer(editTap)
        navigationController.backIcon.isHidden = false
        navigationController.editIcon.isHidden = false
    }
    func configureScrollView() {
        scrollView.layoutIfNeeded()
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    func configureContentView() {
        contentView.sizeToFit()
        contentView.backgroundColor = GlobalSettings.shared().lightGray
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor),
            ])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: view.bounds.height)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
    }
    func cofigureActivityIndicator(activityView: UIView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityView.widthAnchor.constraint(equalToConstant: 80),
            activityView.heightAnchor.constraint(equalToConstant: 80)])
    }
    func configureDateLabel() {
        dateLabel.font = GlobalSettings.shared().mediumSystemFont(size: 20)
        dateLabel.textColor = GlobalSettings.shared().darkGray
        dateLabel.textAlignment = .center
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(dateLabel)
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 25),
            dateLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
    }
    func configureArchiveButton() {
        archiveButton.tintColor = GlobalSettings.shared().mainColor
        if note.category == 2 {
            archiveButton.setBackgroundImage(UIImage(systemName: "archivebox.fill"), for: .normal)
        } else {
            archiveButton.setBackgroundImage(UIImage(systemName: "archivebox"), for: .normal)
        }
        archiveButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(archiveButton)
        NSLayoutConstraint.activate([
            archiveButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            archiveButton.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            archiveButton.widthAnchor.constraint(equalToConstant: 40),
            archiveButton.heightAnchor.constraint(equalToConstant: 40)])
        archiveButton.addTarget(self, action: #selector(didTapArchive(sender:)), for: .touchUpInside)
    }
    func configureReminderComponent() {
        reminderLabel.text = "تنظیمات یادآور:"
        reminderLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        reminderLabel.textColor = GlobalSettings.shared().darkGray
        reminderLabel.textAlignment = .right
        reminderLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reminderLabel)
        NSLayoutConstraint.activate([
            reminderLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10),
            reminderLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)])
        reminderButton.titleLabel?.font = GlobalSettings.shared().mediumSystemFont(size: 17)
        reminderButton.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        reminderButton.tintColor = GlobalSettings.shared().mainColor
        reminderButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reminderButton)
        if let dueDate = dueDate {
            reminderButton.setTitle(persianLongDF.string(from: dueDate), for: .normal)
        } else {
            reminderButton.setTitle("بدون یادآور", for: .normal)
        }
        NSLayoutConstraint.activate([
            reminderButton.trailingAnchor.constraint(equalTo: reminderLabel.leadingAnchor, constant: -10),
            reminderButton.centerYAnchor.constraint(equalTo: reminderLabel.centerYAnchor)])
        reminderButton.addTarget(self, action: #selector(didTapReminderButton(tapGestureRecognizer:)), for: .touchUpInside)
    }
    func configureTitleLabel() {
        titleLabel.text = "عنوان:"
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        titleLabel.textColor = GlobalSettings.shared().darkGray
        titleLabel.textAlignment = .right
        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: reminderLabel.bottomAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
    }
    func configureNoteTitle() {
        noteTitle.sizeToFit()
        noteTitle.isScrollEnabled = false
        noteTitle.backgroundColor = .clear
        noteTitle.isUserInteractionEnabled = false
        noteTitle.translatesAutoresizingMaskIntoConstraints = false
        noteTitle.font = GlobalSettings.shared().boldSystemFont(size: 18)
        noteTitle.textColor = .black
        noteTitle.textAlignment = .right
        contentView.addSubview(noteTitle)
        NSLayoutConstraint.activate([
            noteTitle.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            noteTitle.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            noteTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        makeRound(noteTitle)
    }
    func configureViewLabel() {
        viewLabelTitle.text = "برچسب:"
        viewLabelTitle.translatesAutoresizingMaskIntoConstraints = false
        viewLabelTitle.font = GlobalSettings.shared().boldSystemFont(size: 18)
        viewLabelTitle.textColor = GlobalSettings.shared().darkGray
        viewLabelTitle.textAlignment = .right
        contentView.addSubview(viewLabelTitle)
        NSLayoutConstraint.activate([
            viewLabelTitle.topAnchor.constraint(equalTo: noteTitle.bottomAnchor, constant: 50),
            viewLabelTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        if let label = noteLabel {
            viewLabel = RKLabelIndicator(color: label.Color, title: label.Title)
            viewLabel.isHidden = false
        } else {
            viewLabel = RKLabelIndicator(color: GlobalSettings.shared().lightGrayInt, title: "بدون برچسب")
            viewLabel.isHidden = true
        }
        viewLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(viewLabel)
         NSLayoutConstraint.activate([
            viewLabel.centerYAnchor.constraint(equalTo: viewLabelTitle.centerYAnchor),
            viewLabel.trailingAnchor.constraint(equalTo: viewLabelTitle.leadingAnchor, constant: 0)
           ])
    }
    func configureDescriptionLabel() {
        descriptionLabel.text = "متن یادداشت :"
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        descriptionLabel.textColor = GlobalSettings.shared().darkGray
        descriptionLabel.textAlignment = .right
        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: viewLabelTitle.bottomAnchor, constant: 25),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)])
    }
    func configureNoteDescription() {
        noteDescription.sizeToFit()
        noteDescription.layoutIfNeeded()
        noteDescription.isScrollEnabled = false
        noteDescription.backgroundColor = .clear
        noteDescription.isUserInteractionEnabled = false
        noteDescription.translatesAutoresizingMaskIntoConstraints = false
        noteDescription.font = GlobalSettings.shared().boldSystemFont(size: 15)
        noteDescription.textColor = GlobalSettings.shared().darkGray
        noteDescription.textAlignment = .right
        contentView.addSubview(noteDescription)
        NSLayoutConstraint.activate([
            noteDescription.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10),
            noteDescription.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            noteDescription.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
        noteDescription.bottomAnchor.constraint(greaterThanOrEqualTo: contentView.bottomAnchor, constant: -20)])
        makeRound(noteDescription)
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
}
//extension NoteDetailsViewController: UITextViewDelegate {
//    func textViewDidChange(_ textView: UITextView) {
//        let fixedWidth = textView.frame.size.width
//        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
//        print(newSize.height)
//    }
//}

extension NoteDetailsViewController: AddNewNoteViewControllerDelegate {
    func addNewNote(_ controller: AddNewNoteViewController, didAddItem item: Note?, at indexPath: IndexPath?) {
        getNote()
        for label in labels {
            if label.LabelID == item?.labelId {
                noteLabel = label
                viewLabel.setLabel(color: label.Color, title: label.Title)
                viewLabel.isHidden = false
                break
            } else {
                viewLabel.setLabel(color: GlobalSettings.shared().lightGrayInt, title: "بدون برچسب")
                viewLabel.isHidden = true
                noteLabel = nil
            }
        }
    }
}
