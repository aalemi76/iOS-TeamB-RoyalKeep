//
//  NotesViewController+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
extension NotesViewController: SideMenuDelegate {
    func logoutButtonTouchUpInside() {
        closeSideMenu()
        let alert = UIAlertController(title: "خروج از برنامه!", message: "در صورت خروج همه ی اطلاعات شما حذف خواهد شد.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "خروج", style: .destructive) { _ in
            do {
                try FileManager.default.removeItem(at: User.userArchiveURL)
                User.resetUser()
                ReminderCollection.removeAllNotes()
                ReminderCollection.removeAllChecklists()
                self.coordinator?.toLoginView()
            } catch {
                print(error)
            }
        }
        alert.addAction(doneAction)
        let cancelAction = UIAlertAction(title: "انصراف", style: .default, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}
extension NotesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result: Int
        inSearchMode ? (result = searchResult.count) : (result = notesModels.count)
        return result
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.notesCellIdentifier, for: indexPath) as! NoteTableViewCell
        let note: Note
        inSearchMode ? (note = searchResult[indexPath.row]) : (note = notesModels[indexPath.row])
        cell.checkBox.isHidden = true
        cell.titleLabel.text = note.title
        cell.descriptionText.text = note.noteDescription
        if let labelId = note.labelId, let label = labelsDictionary[labelId] {
            let hex = String(label.Color, radix: 16)
            cell.label.backgroundColor = UIColor.init(hex: hex)
        } else {
            cell.label.backgroundColor = GlobalSettings.shared().lightGray
        }
        if let reminderDate = note.reminderDate {
            cell.hasReminder = true
        } else {
            cell.hasReminder = false
        }
        cell.inEditingMode = inEditingMode
        cell.isCellSelected = selectedItems.contains(indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let note: Note
        let cell = tableView.cellForRow(at: indexPath) as! NoteTableViewCell
        inSearchMode ? (note = searchResult[indexPath.row]) : (note = notesModels[indexPath.row])
        if inSearchMode {
        searchController.dismiss(animated: true) {
            self.toNoteDetails(note: note, indexPath: indexPath)}
            return
        } else if inEditingMode {
            if cell.isCellSelected {
                selectedItems = selectedItems.filter { $0 != indexPath.row }
                print(selectedItems)
                isAllSelected = false
            } else {
                selectedItems.append(indexPath.row)
                selectedItems.count == notesModels.count ?  isAllSelected = true : ()
            }
            cell.isCellSelected = !cell.isCellSelected
        } else {
            toNoteDetails(note: note, indexPath: indexPath)
        }
    }
    func toNoteDetails(note: Note, indexPath: IndexPath) {
        let index = mainNotes.firstIndex(of: note)
        let label = labelsDictionary[note.labelId ?? ""]
        let labels = Array(labelsDictionary.values)
        coordinator?.toNoteDetailsView(self, noteId: note.noteId!, indexPath: indexPath, noteLabel: label, labels: labels, mainIndex: index!)
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        guard !inEditingMode && !inSearchMode else {return nil}
        let note = notesModels[indexPath.row]
        let archive = UIAction(title: "آرشیو", image: UIImage(systemName: "archivebox.fill")) { [weak self] _ in
            self?.archive(note: note)
        }
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [archive])
        }
    }
    func archive(note: Note) {
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.notesSetCategory, method: .post, parameters: ["ItemIDs": [note.noteId!], "Category": 2], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
extension NotesViewController: AddNewNoteViewControllerDelegate {
    func addNewNote(_ controller: AddNewNoteViewController, didAddItem item: Note?, at indexPath: IndexPath?) {
        request()
    }
}
extension NotesViewController: NoteDetailsViewControllerDelegate {
    func backIconTouchUpInisde(isEdited: Bool) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        if isEdited {
            navigationController.menuIcon.isHidden = false
            editView.isHidden = false
            inSearchMode = false
            searchController.searchBar.text = ""
            request()
        }
    }
}
extension NotesViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.menuIcon.isHidden = true
        editView.isHidden = true
        inSearchMode = true
        return
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.menuIcon.isHidden = false
        editView.isHidden = false
        inSearchMode = false
        return
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search()
        return
    }
}
