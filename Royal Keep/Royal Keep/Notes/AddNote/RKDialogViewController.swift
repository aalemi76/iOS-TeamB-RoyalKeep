//
//  AlertViewController.swift
//  Password Manager
//
//  Created by ArmanG on 3/25/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

class RKDialogViewController: UIViewController {

    typealias completion = (Bool) -> Void
    var completionHandler: completion?
    
    @IBOutlet weak var topAnchorContainer: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblAlertTitle: UILabel!
    @IBOutlet weak var lblAlertMessage: UILabel!
    
    var alertTitle: String = "" 
    
    var alertMessage: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAlertView()
        lblAlertTitle.text = alertTitle
        lblAlertMessage.text = alertMessage
    }
    
    override func viewDidAppear(_ animated: Bool) {
      
        UIView.animate(withDuration: 0.5) {
            self.containerView.alpha = 1.0
            self.topAnchorContainer.constant += 120
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func tapRecognizer(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    private func setupAlertView() {
        view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        let gradient = CAGradientLayer()
        gradient.frame = self.containerView.bounds
        gradient.colors = [ #colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1).cgColor, #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1).cgColor]
        gradient.cornerRadius = 40
        self.containerView.layer.insertSublayer(gradient, at: 0)
        
        self.containerView.layer.cornerRadius = 40
        self.containerView.layer.masksToBounds = true
        
    }

    @IBAction func btnCancel(_ sender: RKGraidentButton) {
        guard let completionBlock = completionHandler else { return }
        completionBlock(false)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.alpha = 0.0
            self.topAnchorContainer.constant -= 120
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: nil)
        })
        
    }
    @IBAction func btnOk(_ sender: RKGraidentButton) {
        guard let completionBlock = completionHandler else { return }
        completionBlock(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.containerView.alpha = 0.0
            self.topAnchorContainer.constant += 180
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
