//
//  LabelIndicator.swift
//  LabelIndicator
//
//  Created by ArmanG on 4/8/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

class RKLabelIndicator: UIView {

    var color: Int = Int()
    var title: String = ""
    
    private var lblTilte = UILabel()
    private var colorIndicatorView = UIView()
    
    init() {
        super.init(frame: CGRect.zero)
    }
    init(color: Int, title: String) {
        super.init(frame: CGRect.zero)
        self.frame.size = CGSize(width: 150, height: 50)
        self.color = color
        self.title = title
        configureLayout()
        setup()
    }
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        configureLayout()
//        setup()
//    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
//        configureLayout()
//        setup()
    }
    
    func setup() {
        
        
        // CONFIGURE lblTitle
        lblTilte.font = GlobalSettings.shared().systemFont(size: 17)
        lblTilte.text = title
        lblTilte.textColor = GlobalSettings.shared().mainColor
        lblTilte.textAlignment = .right
        
        // CONFIGURE colorIndicatorView
        let hex = String(color, radix: 16)
        colorIndicatorView.backgroundColor = UIColor.init(hex: hex)
        colorIndicatorView.layer.cornerRadius = 10
        colorIndicatorView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        colorIndicatorView.layer.borderWidth = 1.5
        
        
    }
    
    func configureLayout() {
        
        self.addSubview(lblTilte)
        self.addSubview(colorIndicatorView)
        
        lblTilte.translatesAutoresizingMaskIntoConstraints = false
        colorIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        
        var allConstraint = [NSLayoutConstraint]()

        let clrInctrViewVC = NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[view(20)]-15-|", options: .init(rawValue: 0), metrics: nil, views: ["view": colorIndicatorView])
        let lblTtleVC = NSLayoutConstraint.constraints(withVisualFormat: "V:|-halfViewHeight-[lbl]", options: .init(rawValue: 0), metrics: ["halfViewHeight": 15], views: ["lbl": lblTilte])
        //let viewlblHC = NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[view(20)]-20-[lbl]", options: .init(rawValue: 0), metrics: nil, views: ["view":colorIndicatorView, "lbl": lblTilte])
        let viewlblHC = NSLayoutConstraint.constraints(withVisualFormat: "H:[lbl]-7-[view(20)]-20-|", options: .init(rawValue: 0), metrics: nil, views: ["view":colorIndicatorView, "lbl": lblTilte])
        allConstraint += clrInctrViewVC
        allConstraint += lblTtleVC
        allConstraint += viewlblHC
        
        NSLayoutConstraint.activate(allConstraint)
    }
    func setLabel(color: Int, title: String) {
        self.color = color
        self.title = title
        setup()
    }

}
