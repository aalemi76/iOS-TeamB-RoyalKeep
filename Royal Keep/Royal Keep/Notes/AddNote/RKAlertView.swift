//
//  AlertView.swift
//  PiiiiiiiiiiiickerView
//
//  Created by ArmanG on 4/10/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

enum Alert {
    case ok
    case error
}

class RKAlertView: UIView {

    
    
    var alert: Alert = .ok
    var message = ""
    
    private var lblAlertMessage = UILabel()
    private var imgAlert = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    init(alert: Alert, message: String) {
        super.init(frame: CGRect.zero)
        
        self.alert = alert
        self.message = message
        
        setup()
        createAlert()
    }
    
    func setup() {
        let screenWidth  = UIScreen.main.bounds.width * (90/100)
        let screenHeight = UIScreen.main.bounds.height * (15/100)
        
        self.bounds.size.width = screenWidth
            self.bounds.size.height = screenHeight
        self.layer.cornerRadius = self.bounds.height / 2
        
        self.addSubview(lblAlertMessage)
        self.addSubview(imgAlert)
        
        lblAlertMessage.font = UIFont(name: "IRANSansMobile", size: 17.0)
        lblAlertMessage.textAlignment = .right
        lblAlertMessage.textColor = .white
    }
    
    func createAlert() {
        switch alert {
        case .ok:
            self.backgroundColor = .blue
            self.imgAlert.image = UIImage(named: "ok")
            lblAlertMessage.text = message
        case .error:
            self.backgroundColor = .red
            self.imgAlert.image = UIImage(named: "error")
            lblAlertMessage.text = message
        }
        
        configureLayout()
    }

    func configureLayout() {
        
                
        lblAlertMessage.translatesAutoresizingMaskIntoConstraints = false
        imgAlert.translatesAutoresizingMaskIntoConstraints = false
        
        var allConstraint = [NSLayoutConstraint]()

//        let clrInctrViewVC = NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[view(32)]", options: .init(rawValue: 0), metrics: nil, views: ["view": imgAlert])
        imgAlert.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        //let lblTtleVC = NSLayoutConstraint.constraints(withVisualFormat: "V:|-40-[lbl]", options: .init(rawValue: 0), metrics: nil, views: ["lbl": lblAlertMessage])
        lblAlertMessage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        let viewlblHC = NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lbl]-20-[view(32)]-30-|", options: .init(rawValue: 0), metrics: nil, views: ["view":imgAlert, "lbl": lblAlertMessage])
//        allConstraint += clrInctrViewVC
        //allConstraint += lblTtleVC
        allConstraint += viewlblHC
        
        NSLayoutConstraint.activate(allConstraint)
    }
}
