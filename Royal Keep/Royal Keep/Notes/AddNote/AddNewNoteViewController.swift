//
//  AddNewNoteViewController.swift
//  Royal Keep
//
//  Created by Catalina on 6/29/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol AddNewNoteViewControllerDelegate: class {
    func addNewNote(_ controller: AddNewNoteViewController, didAddItem item: Note?, at indexPath: IndexPath?)
}
class AddNewNoteViewController: UIViewController, Storyboarded {
    enum ActionSate {
        case addNote
        case cancel
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    weak var delegate: AddNewNoteViewControllerDelegate?
    weak var coordinator: NoteCoordinator?
    var titleView: TitleView!
    var checkmarkIcon: UIImageView!
    var xmarkIcon: UIImageView!
    var itemToEdit: Note?
    var indexPath: IndexPath?
    var labels = [Label]()
    var noteLabel: Label?
    var date = Date()
    var newNote: Note!
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let persianDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .short
        formatter.dateFormat = "dd - MMMM - yyyy"
        return formatter
    }()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var btnLabel: RKButtonLabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var textView_Description: UITextView!
    @IBOutlet weak var textView_Titlte: UITextView!
//MARK:- Action Methods
    @objc func checkmarkIconTouchUpInside() {
        if checkActions(action: .addNote) {
            self.showAlertView(alert: .ok, message: "یادداشت شما با موفقیت ثبت شد")
            modifyNote()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                if var itemToedit = self.itemToEdit {
                    itemToedit.title = self.textView_Titlte.text
                    itemToedit.noteDescription = self.textView_Description.text
                }
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            showAlertView(alert: .error, message: "عنوان نمی تواند خالی باشد")
        }
    }
    @objc func xmarkIconTouchUpInside() {
        if  checkActions(action: .cancel) {
            let alert = UIAlertController(title: "اخطار", message: "همه ی نوشته های خود را از دست میدهید", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "خیر", style: .cancel ) {
                action in
                return
            }
            
            let okAction = UIAlertAction(title: "بلی", style: .destructive ) {
                action in
                self.dismiss(animated: true, completion: nil)
            }
            
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true)
            
        } else {
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func textFieldEditingChanged(_ sender: Any) {
        if let item = itemToEdit {
            let currentText = item.title
            if textView_Titlte.text == currentText {
                self.isEditing = false
            } else {
                self.isEditing = true
            }
        }
    }
    @objc func viewDidTap(tapGestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 50, right: 0)
        }
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func didTapLabelBtn() {
        let vc = CustomPickerViewController()
        vc.labels = labels
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        if let label = noteLabel {
            vc.selectedItem = label
            for (i, item) in labels.enumerated() {
                if item.Color == label.Color {
                    vc.selectedIndex = i
                    break
                }
            }
        }
        present(vc, animated: true, completion: nil)
        vc.cancelCompletion = { picker in
            picker.dismiss(animated: true, completion: nil)
        }
        vc.removeCompletion = { picker in
            picker.dismiss(animated: true) {
                self.noteLabel = nil
                self.isEditing = true
                self.configureLabelButton()
            }
        }
        vc.doneCompletion = { picker, label in
            picker.dismiss(animated: true) {
                self.noteLabel = label
                self.isEditing = true
                self.configureLabelButton()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        textView_Description.delegate = self
        textView_Titlte.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewDidTap(tapGestureRecognizer:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        configureTitleView()
        configureScrollView()
        configureContentView()
        configureDateLabel()
        configureTitleTextView()
        configureTextView()
        configureLabelButton()
        configureItemDetail()
    }
    //MARK:- Helper Methods
    
    func checkActions(action: ActionSate) -> Bool {
        let txtField_strLenght = self.textView_Titlte.text!.trimmingCharacters(in: CharacterSet.whitespaces).count
        let txtView_strLenght = self.textView_Description.text!.trimmingCharacters(in: CharacterSet.whitespaces).count
        
        switch action {
        case .addNote:
            
                
            if textView_Titlte.text!.isEmpty {
                return false
            } else if txtField_strLenght == 0 {
                return false
            } else {
                return true
            }
            
        case .cancel:
            
            if itemToEdit != nil {
                if isEditing == true {
                    return true
                } else {
                    return false
                }
            } else {
                if !textView_Titlte.text!.isEmpty {
                    if txtField_strLenght == 0 {
                        return false
                    }
                    return true
                } else if !textView_Description.text.isEmpty {
                    
                     if txtView_strLenght == 0 {
                         return false
                     }
                     return true
                } else if (btnLabel.labelTitle.text != "بدون برچسب"){
                    return true
                } else {
                    return false
                }

            }
            
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    }
    
    func configureTitleView() {
        if let _ = itemToEdit {
            let title = "ویرایش یادداشت"
            titleView = TitleView(title: title)
        } else {
            titleView = TitleView(title: "افزودن یادداشت جدید")
        }
        titleView.addCheckmarkIcon()
        titleView.addXmarkIcon()
        checkmarkIcon = titleView.checkmarkIcon
        let checkTap = UITapGestureRecognizer(target: self, action: #selector(checkmarkIconTouchUpInside))
        checkmarkIcon.addGestureRecognizer(checkTap)
        xmarkIcon = titleView.xmarkIcon
        let xTap = UITapGestureRecognizer(target: self, action: #selector(xmarkIconTouchUpInside))
        xmarkIcon.addGestureRecognizer(xTap)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleView)
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: view.topAnchor),
            titleView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            titleView.heightAnchor.constraint(equalToConstant: 90)])
    }
    func configureScrollView() {
        scrollView.layoutIfNeeded()
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    func configureContentView() {
        contentView.sizeToFit()
        contentView.backgroundColor = GlobalSettings.shared().lightGray
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor)])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: view.bounds.height)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
    }
    func configureItemDetail() {
        guard let item = itemToEdit else {return}
        textView_Titlte.text = item.title
        textView_Description.text = item.noteDescription
        for label in labels {
            if label.LabelID == item.labelId {
                let hex = String(label.Color, radix: 16)
                btnLabel.colorView.backgroundColor = UIColor.init(hex: hex)
                btnLabel.setTitle(label.Title, for: .normal)
            }
        }
    }
    func configureDateLabel() {
        lblDate.font = GlobalSettings.shared().mediumSystemFont(size: 20)
        lblDate.textColor = GlobalSettings.shared().darkGray
        lblDate.textAlignment = .center
        if let item = itemToEdit {
            date = dateFormatter.date(from: item.creationDate ?? "")!
            lblDate.text = persianDateFormatter.string(from: date)
        } else {
            lblDate.text = persianDateFormatter.string(from: date)
        }
    }
    func configureTitleTextView() {
        textView_Titlte.sizeToFit()
        textView_Titlte.isScrollEnabled = false
        textView_Titlte.backgroundColor = .clear
        textView_Titlte.isUserInteractionEnabled = true
        textView_Titlte.font = GlobalSettings.shared().boldSystemFont(size: 18)
        textView_Titlte.textColor = .black
        textView_Titlte.textAlignment = .right
        makeRound(textView_Titlte)
    }
    func configureLabelButton() {
        btnLabel.addTarget(self, action: #selector(didTapLabelBtn), for: .touchUpInside)
        guard let label = noteLabel else {
            btnLabel.colorView.backgroundColor = UIColor.white
            btnLabel.setTitle("بدون برچسب", for: .normal)
            return
        }
        let hex = String(label.Color, radix: 16)
        btnLabel.colorView.backgroundColor = UIColor.init(hex: hex)
        btnLabel.setTitle(label.Title, for: .normal)
    }
    func configureTextView() {
        textView_Description.sizeToFit()
        textView_Description.layoutIfNeeded()
        textView_Description.isScrollEnabled = false
        textView_Description.backgroundColor = .clear
        textView_Description.isUserInteractionEnabled = true
        textView_Description.translatesAutoresizingMaskIntoConstraints = false
        textView_Description.font = GlobalSettings.shared().boldSystemFont(size: 15)
        textView_Description.textColor = GlobalSettings.shared().darkGray
        textView_Description.textAlignment = .right
        makeRound(textView_Description)
        textView_Description.bottomAnchor.constraint(greaterThanOrEqualTo: contentView.bottomAnchor, constant: -20).isActive = true
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
    func cofigureActivityIndicator(activityView: UIView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityView.widthAnchor.constraint(equalToConstant: 80),
            activityView.heightAnchor.constraint(equalToConstant: 80)])
    }
    
    func prepareDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "farsi_IR")
        dateFormatter.dateFormat = "yyyy/MM/dd __ HH:mm"
        return dateFormatter.string(from: date)
    }
    
    func modifyNote() {
        var activityView: RKActivityIndicatorView? = RKActivityIndicatorView()
        cofigureActivityIndicator(activityView: activityView!)
        activityView?.showSpinner()
        let title = textView_Titlte.text!
        let description = textView_Description.text ?? ""
        let creationDate = dateFormatter.string(from: date)
        var url: String
        var method: HTTPMethod
        var headers: HTTPHeaders
        var parameters: [String: String]
        if let item = itemToEdit {
            url = RKAPI.updateNotesURL
            method = .put
            headers = RKAPI.headers
            parameters = ["NoteID": item.noteId!, "Title": title, "Description": description, "CreationDate": creationDate, "LabelID": noteLabel?.LabelID ?? ""]
        } else {
            url = RKAPI.createNotesURL
            method = .post
            headers = RKAPI.username
            parameters = ["Title": title, "Description": description, "CreationDate": creationDate, "LabelID": noteLabel?.LabelID ?? ""]
        }
        AF.request(url, method: method, parameters: parameters, encoder: JSONParameterEncoder.prettyPrinted, headers: headers, interceptor: nil).responseJSON(completionHandler: {
            response in
            switch response.result {
            case .success:
                activityView?.showCheckmark()
                let jsonObject = JSON(response.data ?? Data())
                if let title = jsonObject["Title"].string, let noteId = jsonObject["NoteID"].string, let creationDate = jsonObject["CreationDate"].string {
                    let description = jsonObject["Description"].string
                    let labelId = jsonObject["LabelID"].string
                    let category = jsonObject["Category"].int
                    self.newNote = Note(title: title, description: description, creationDate: creationDate, labelId: labelId, noteId: noteId, category: category)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        activityView?.dismiss()
                        activityView?.removeFromSuperview()
                        activityView = nil
                        self.showAlertView(alert: .ok, message: "یادداشت شما با موفقیت ثبت شد")
                        self.delegate?.addNewNote(self, didAddItem: self.newNote, at: self.indexPath)
                    }
                }
            case let .failure(error):
                print(error)
            }
        })
    }
    
    func showAlertView(alert: Alert, message: String) {
        var timer: Timer!
        let screenWidth  = UIScreen.main.bounds.width * (90/100)
        let screenHeight = UIScreen.main.bounds.height * (15/100)
        let xPosition    = (UIScreen.main.bounds.width - screenWidth) / 2
        
        
        switch alert {
        case .ok:
            let alertView = RKAlertView(alert: alert, message: message)
            alertView.frame.origin = CGPoint(x: xPosition, y: -screenHeight)
            
            self.view.addSubview(alertView)
            
            UIView.animate(withDuration: 0.8){
                alertView.frame.origin.y += screenHeight + 40
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false, block: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    alertView.frame.origin.y -= screenHeight + 40 }
                    , completion: { _ in alertView.removeFromSuperview() })
            })
            
        case .error:
            let alertView = RKAlertView(alert: alert, message: message)
            alertView.frame.origin = CGPoint(x: xPosition, y: -screenHeight)
            
            self.view.addSubview(alertView)
            
            UIView.animate(withDuration: 0.8){
                alertView.frame.origin.y += screenHeight + 40
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false, block: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    alertView.frame.origin.y -= screenHeight + 40 }
                    , completion: { _ in alertView.removeFromSuperview() })
            })
        }
        
    }
}


extension AddNewNoteViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if let item = itemToEdit {
            let currentText = item.description
            if textView.text == currentText {
                self.isEditing = false
            } else {
                self.isEditing = true
            }
        }
    }
}
