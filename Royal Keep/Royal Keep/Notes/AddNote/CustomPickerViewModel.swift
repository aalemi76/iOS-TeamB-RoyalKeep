//
//  CustomPickerViewModel.swift
//  Royal Keep
//
//  Created by Catalina on 7/1/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class CustomPickerViewModel {
    var content: UIView!
    var picker: UIView!
    var pickerView: UIPickerView!
    let pickerViewHeight: CGFloat = 216
    var done: UIButton!
    var remove: UIButton!
    var cancel: UIButton!
    let buttonHeight: CGFloat = 60
    init() {
        configureContent()
        configureCancelButton()
        configurePicker()
    }
    private func configureContent(){
        content = UIView(frame: .zero)
        content.backgroundColor = .none
        content.translatesAutoresizingMaskIntoConstraints = false
    }
    private func configureCancelButton(){
        cancel = UIButton(type: .system)
        cancel.backgroundColor = GlobalSettings.shared().lightGray
        cancel.setTitle("انصراف", for: .normal)
        cancel.titleLabel?.textAlignment = .center
        cancel.titleLabel?.font = GlobalSettings.shared().boldSystemFont(size: 20)
        cancel.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        cancel.translatesAutoresizingMaskIntoConstraints = false
        content.addSubview(cancel)
        NSLayoutConstraint.activate([
            cancel.bottomAnchor.constraint(equalTo: content.bottomAnchor, constant: -30),
            cancel.leadingAnchor.constraint(equalTo: content.leadingAnchor, constant: 20),
            cancel.trailingAnchor.constraint(equalTo: content.trailingAnchor, constant: -20),
            cancel.heightAnchor.constraint(equalToConstant: buttonHeight)])
        makeRounded(item: cancel, radius: 15)
    }
    private func configurePicker(){
        picker = UIView(frame: .zero)
        picker.translatesAutoresizingMaskIntoConstraints = false
        content.addSubview(picker)
        NSLayoutConstraint.activate([
            picker.bottomAnchor.constraint(equalTo: cancel.topAnchor, constant: -8),
            picker.leadingAnchor.constraint(equalTo: content.leadingAnchor, constant: 20),
            picker.trailingAnchor.constraint(equalTo: content.trailingAnchor, constant: -20),
            picker.heightAnchor.constraint(equalToConstant: pickerViewHeight + buttonHeight * 2)])
        makeRounded(item: picker)
        configureDoneButton()
        configureRemoveButton()
        configurePickerView()
    }
    private func configureDoneButton(){
        done = UIButton(type: .system)
        done.backgroundColor = GlobalSettings.shared().lightGray
        done.setTitle("انتخاب", for: .normal)
        done.titleLabel?.textAlignment = .center
        done.titleLabel?.font = GlobalSettings.shared().boldSystemFont(size: 20)
        done.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        done.translatesAutoresizingMaskIntoConstraints = false
        picker.addSubview(done)
        NSLayoutConstraint.activate([
            done.bottomAnchor.constraint(equalTo: picker.bottomAnchor),
            done.leadingAnchor.constraint(equalTo: picker.leadingAnchor),
            done.trailingAnchor.constraint(equalTo: picker.trailingAnchor),
            done.heightAnchor.constraint(equalToConstant: buttonHeight)])
        makeRounded(item: done, radius: 0)
    }
    private func configureRemoveButton() {
        remove = UIButton(type: .system)
        remove.backgroundColor = GlobalSettings.shared().lightGray
        remove.setTitle("حذف", for: .normal)
        remove.titleLabel?.textAlignment = .center
        remove.titleLabel?.font = GlobalSettings.shared().boldSystemFont(size: 20)
        remove.setTitleColor(.systemRed, for: .normal)
        remove.translatesAutoresizingMaskIntoConstraints = false
        picker.addSubview(remove)
        NSLayoutConstraint.activate([
            remove.bottomAnchor.constraint(equalTo: done.topAnchor),
            remove.leadingAnchor.constraint(equalTo: picker.leadingAnchor),
            remove.trailingAnchor.constraint(equalTo: picker.trailingAnchor),
            remove.heightAnchor.constraint(equalToConstant: buttonHeight)])
        let topBorder = UIView()
        topBorder.backgroundColor = GlobalSettings.shared().darkGray
        topBorder.translatesAutoresizingMaskIntoConstraints = false
        remove.addSubview(topBorder)
        NSLayoutConstraint.activate([
            topBorder.topAnchor.constraint(equalTo: remove.topAnchor),
            topBorder.leadingAnchor.constraint(equalTo: remove.leadingAnchor),
            topBorder.trailingAnchor.constraint(equalTo: remove.trailingAnchor),
            topBorder.heightAnchor.constraint(equalToConstant: 0.5)])
    }
    private func configurePickerView(){
        pickerView = UIPickerView(frame: .zero)
        pickerView.backgroundColor = GlobalSettings.shared().lightGray
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        picker.addSubview(pickerView)
        NSLayoutConstraint.activate([
            pickerView.bottomAnchor.constraint(equalTo: remove.topAnchor),
            pickerView.leadingAnchor.constraint(equalTo: picker.leadingAnchor),
            pickerView.trailingAnchor.constraint(equalTo: picker.trailingAnchor),
            pickerView.heightAnchor.constraint(equalToConstant: pickerViewHeight)])
    }
    private func makeRounded(item: UIView, radius: CGFloat = 20){
        item.layer.cornerRadius = radius
        item.layer.borderColor = UIColor.darkGray.cgColor
        item.layer.borderWidth = 0.5
        item.layer.masksToBounds = true
    }
}
