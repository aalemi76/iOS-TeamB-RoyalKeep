//
//  CustomPickerViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/1/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit

class CustomPickerViewController: UIViewController {
    var content: UIView!
    var picker: UIView!
    var pickerView: UIPickerView!
    var remove: UIButton!
    var done: UIButton!
    var cancel: UIButton!
    var viewModel = CustomPickerViewModel()
    var labels = [Label]()
    var selectedIndex: Int?
    var selectedItem: Label?
    var removeCompletion: ((CustomPickerViewController)->Void)?
    var doneCompletion: ((CustomPickerViewController, Label)->Void)?
    var cancelCompletion: ((CustomPickerViewController)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapOnView(tapGestureRecognizer:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        configureBackground()
        configureContent()
        configureCancelButton()
        configureDoneButton()
        configureRemoveButton()
        configurePickerView()
    }
    @objc func didTapRemoveButton(sender: UIButton) {
        removeCompletion?(self)
    }
    @objc func didTapDoneButton(sender: UIButton) {
        selectedIndex = pickerView.selectedRow(inComponent: 0)
        selectedItem = labels[selectedIndex!]
        doneCompletion?(self, selectedItem!)
    }
    @objc func didTapCancelButton(sender: UIButton) {
        cancelCompletion?(self)
    }
    @objc func didTapOnView(tapGestureRecognizer: UITapGestureRecognizer){
        let point = tapGestureRecognizer.location(in: self.view)
        if !picker.frame.contains(point) && !cancel.frame.contains(point) {
            dismiss(animated: true, completion: nil)
        }
    }
    func configureBackground(){
        let background = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        background.backgroundColor = .lightGray
        background.alpha = 0.6
        background.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(background)
        view.sendSubviewToBack(background)
    }
    func configureContent(){
        content = viewModel.content
        picker = viewModel.picker
        view.addSubview(content)
        NSLayoutConstraint.activate([
            content.topAnchor.constraint(equalTo: view.topAnchor),
            content.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            content.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            content.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func configureCancelButton(){
        cancel = viewModel.cancel
        cancel.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
    }
    func configureDoneButton(){
        done = viewModel.done
        if labels.count == 0 {
            done.isEnabled = false
        }
        done.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
    }
    func configureRemoveButton() {
        remove = viewModel.remove
        if selectedItem != nil {
            remove.isEnabled = true
            remove.setTitleColor(.systemRed, for: .normal)
        } else {
            remove.isEnabled = false
            remove.setTitleColor(GlobalSettings.shared().darkGray, for: .normal)
        }
        remove.addTarget(self, action: #selector(didTapRemoveButton(sender:)), for: .touchUpInside)
    }
    func configurePickerView(){
        pickerView = viewModel.pickerView
        pickerView.dataSource = self
        pickerView.delegate = self
        if let index = selectedIndex {
            pickerView.selectRow(index, inComponent: 0, animated: true)
        }
    }
}
extension CustomPickerViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 66
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return labels.count
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if labels.count > 0 {
            let label = labels[row]
            let title = label.Title
            let color = label.Color
            let indicatorView = RKLabelIndicator(color: color, title: title)
            return indicatorView
        } else {
            return UIView()
        }
    }
}
