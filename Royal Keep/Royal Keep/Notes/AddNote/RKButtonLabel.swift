//
//  LabelButton.swift
//  LabelButton
//
//  Created by ArmanG on 4/9/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

class RKButtonLabel: UIButton {
    
    let colorView = UIView()
    let labelTitle = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
            setup()
        }
      
    override func setTitle(_ title: String?, for state: UIControl.State) {
        labelTitle.text = title
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let halfHeightClrView = colorView.bounds.height / 2
        colorView.layer.cornerRadius = halfHeightClrView
        colorView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        colorView.layer.borderWidth = 1.5
    }
        func setup() {
            self.setTitle("", for: .normal)
            colorView.backgroundColor = .white
            addSubview(colorView)
            addSubview(labelTitle)
            
            labelTitle.font = GlobalSettings.shared().boldSystemFont(size: 17)
            labelTitle.textColor = #colorLiteral(red: 0.4253651798, green: 0.3433555944, blue: 0.7693718076, alpha: 1)
            labelTitle.text = "برچسب"
            configureLayout()
            
        }
    
    func configureLayout() {
        colorView.translatesAutoresizingMaskIntoConstraints = false
        labelTitle.translatesAutoresizingMaskIntoConstraints = false
        
        colorView.topAnchor.constraint(equalTo: self.topAnchor, constant: 3).isActive = true
        colorView.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        colorView.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        colorView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        labelTitle.trailingAnchor.constraint(equalTo: colorView.trailingAnchor, constant: -30).isActive = true
        labelTitle.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    }
}
