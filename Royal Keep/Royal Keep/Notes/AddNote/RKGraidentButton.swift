//
//  ProjectGraidentButton.swift
//  ProjectGraidentButton
//
//  Created by ArmanG on 2/27/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

@IBDesignable class RKGraidentButton: UIButton {

    @IBInspectable private var firstColor: UIColor = .clear {
        didSet {
            configuration()
        }
    }
    @IBInspectable private var secondColor: UIColor = .clear {
        didSet {
            configuration()
        }
    }
    @IBInspectable private var borderwidth: CGFloat = 0.0 {
        didSet {
            configuration()
        }
    }
    @IBInspectable private var borderColor: UIColor = .clear {
        didSet {
            configuration()
        }
    }
    @IBInspectable private var cornerRadius: CGFloat = CGFloat() {
        didSet {
            configuration()            
        }
    }
    @IBInspectable private var startLocation: CGPoint = CGPoint() {
        didSet {
            configuration()
        }
    }
    @IBInspectable private var endLocation: CGPoint = CGPoint() {
        didSet {
            configuration()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configuration()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configuration()
    }
    
//    override var isHighlighted: Bool {
//        didSet {
//            if isHighlighted {
//                self.frame.origin.y += 5
//            } else {
//                self.frame.origin.y -= 5
//            }
//        }
//    }
    
  private func configuration() {
    
        self.layer.borderColor  = borderColor.cgColor
        self.layer.borderWidth  = borderwidth
        self.layer.cornerRadius = cornerRadius
        createGradient()
        self.layer.masksToBounds = true
    }
  private  func createGradient() {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.endPoint   = CGPoint(x: 0.5, y: 1.0)
        gradient.cornerRadius = self.layer.cornerRadius
        self.layer.insertSublayer(gradient, at: 0)
    
        
    }
    
    override func willMove(toSuperview newSuperview: UIView!) {
        self.createGradient()
    }

}
