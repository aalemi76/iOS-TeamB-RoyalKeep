//
//  User.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class User: NSObject, NSCoding {
    var firstName: String
    var lastName: String
    var username: String
    static var userArchiveURL: URL = {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0].appendingPathComponent("user.archive")
    }()
    static var globalUsername: String = ""
    static var globalFirstName: String = ""
    static var globalLastName: String = ""
    init(firstName: String, lastName: String, username: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.username = username
    }
    func encode(with coder: NSCoder) {
        coder.encode(firstName, forKey: "firstName")
        coder.encode(lastName, forKey: "lastName")
        coder.encode(username, forKey: "username")
    }
    required init?(coder: NSCoder) {
        firstName = coder.decodeObject(forKey: "firstName") as! String
        lastName = coder.decodeObject(forKey: "lastName") as! String
        username = coder.decodeObject(forKey: "username") as! String
        super.init()
    }
    func saveUser() throws {
        let codedData = try NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: false)
        try codedData.write(to: User.userArchiveURL)
    }
    func loadUser() throws -> User? {
        let codedData = try Data(contentsOf: User.userArchiveURL)
        let user = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? User
        return user
    }
    func configureGlobals() {
        User.globalUsername = username
        User.globalFirstName = firstName
        User.globalLastName = lastName
    }
    static func resetUser() {
        User.globalUsername = ""
        User.globalFirstName = ""
        User.globalLastName = ""
    }
}
