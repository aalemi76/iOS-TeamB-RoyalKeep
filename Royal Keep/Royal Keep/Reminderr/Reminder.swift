//
//  Reminder.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/28/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import Foundation

struct Reminder {
    var id : String
    var title : String
    var date : Date
    var type :ReminderType
    var labelId :String?
    
    init(id:String , title:String , date:Date , type:ReminderType , labelId:String?) {
        self.id = id
        self.title = title
        self.date = date
        self.type = type
        self.labelId = labelId
    }
    
    enum ReminderType {
        case checklist
        case note
    }
}
