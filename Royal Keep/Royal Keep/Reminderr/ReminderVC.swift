//
//  ReminderVC.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/28/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol CalendarDelegate {
    func didSelectDate(date:Date)
}

class ReminderVC : SharedViewController , Storyboarded  {
    
    //MARK: - Properties
    weak var coordinator :ReminderCoordinator?
    
    fileprivate var reminderNotes : [Note] {
        var notes = [Note]()
        for (_,note) in ReminderCollection.notes {
            notes.append(note)
        }
        return notes
    }
    
    fileprivate var remindeCheckLists :[Checklist] {
        var checkLists = [Checklist]()
        for (_,checkList) in ReminderCollection.checklists {
            checkLists.append(checkList)
        }
        return checkLists
    }
    
    fileprivate var reminders : [Reminder] {
        var reminders = [Reminder]()
        for note in reminderNotes {
            let reminder = Reminder(id: note.noteId ?? NONE , title: note.title ?? NONE, date: note.reminderDate!,type:.note , labelId:note.labelId)
            reminders.append(reminder)
        }
        for checkList in remindeCheckLists {
            let reminder = Reminder(id: checkList.CheckListID!, title: checkList.Title!, date: checkList.reminderDate!,type:.checklist, labelId: checkList.LabelID)
            reminders.append(reminder)
        }
        return reminders
    }
    var labelsDictionary = [String:Label]()
    var labels = [Label]()
    var tableView = UITableView ()
    var searchField = UISearchBar()
    var btnFetch = UIButton(type: .system)
    var btnCalendar = UIButton()
    var remindersToShow = [Reminder]()
    var reminderForSearch = [Reminder]()
    
    var emptyLabel = UILabel()
    
    //MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ReminderCollection.loadChecklists()
        ReminderCollection.loadNotes()
        getLabels()
        setView()
        configureSideMenu()
        setViewComponents(forViews: [tableView, emptyLabel, searchField, btnCalendar, btnFetch])
        sideMenuVC.delegate = self
        searchField.delegate = self
        remindersToShow = reminders.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
        reminderForSearch = reminders.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
        tableView.rowHeight = 88
        tableView.separatorColor = GlobalSettings.shared().mainColor
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "ReminderTableViewCell", bundle: nil), forCellReuseIdentifier: "ReminderCell")
        tableView.allowsSelection = false
        tableView.reloadData()
        self.navigationItem.title = "یادآور ها"
        hideKeyboardWhenTappedAround()
        checkReminderIsEmpty()
        
    }
    
    //MARK: - Handlers
    @objc func openCalendar() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "CalendarVC") as! CalendarVC
        vc.reminders = reminders
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    func checkReminderIsEmpty() {
        if remindersToShow.isEmpty {
            showNoMatchFound()
        }else{
            hideNoMatchFound()
        }
    }
    
    @objc func fetchReminders() {
       guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.menuIcon.isHidden = false
        searchField.text = ""
        searchField.resignFirstResponder()
        remindersToShow = reminders.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
        reminderForSearch = reminders.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
        if remindersToShow.isEmpty {
            showNoMatchFound()
        }else{
            hideNoMatchFound()
        }
        tableView.reloadData()
        self.navigationItem.title = "یادآور ها"
    }


    @objc func searchFieldDidChange() {
        if (searchField.text?.isEmpty == true) {
            remindersToShow = reminders.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
        } else {
            remindersToShow.removeAll()
            for reminder in reminders {
                if (reminder.title.uppercased().contains(searchField.text!.uppercased())) {
                    remindersToShow.append(reminder)
                  remindersToShow = remindersToShow.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
                }else{
                }
            }
        }
        tableView.reloadData()
    }
    
    
}

//MARK: - table view delegation and data source

extension ReminderVC :UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return remindersToShow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderCell") as! ReminderTableViewCell
        let reminder = remindersToShow[indexPath.row]
        
        var image :UIImage {
            switch reminder.type {
            case .checklist : return UIImage(systemName: "text.badge.checkmark")!
            case .note :      return UIImage(systemName: "doc.text")!
            }
        }
        
        if let labelId = reminder.labelId, let label = labelsDictionary[labelId] {
            let hex = String(label.Color, radix: 16)
            cell.imgView.tintColor = UIColor.init(hex: hex)
        } else {
            cell.imgView.tintColor = GlobalSettings.shared().darkGray
        }
        cell.imgView?.image = image
        cell.lblTitle.text = reminder.title
        cell.lblDate.text = reminder.date.stringDate()
        cell.lblTime.text = reminder.date.stringTime()
        
        return cell
    }
    
}


extension ReminderVC {
    func setView() {
        self.view.addSubview(emptyLabel)
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        emptyLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            .isActive = true
        emptyLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            .isActive = true
        emptyLabel.topAnchor.constraint(equalTo: view.topAnchor)
            .isActive = true
        emptyLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            .isActive = true
        emptyLabel.text = "موردی برای نمایش یافت نشد"
        emptyLabel.textAlignment = .center
        self.emptyLabel.font = GlobalSettings.shared().boldSystemFont(size: 15)
        self.emptyLabel.textColor = GlobalSettings.shared().darkGray
        emptyLabel.isHidden = true
        
        self.view.addSubview(btnFetch)
        btnFetch.translatesAutoresizingMaskIntoConstraints = false
        btnFetch.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 10)
            .isActive = true
        btnFetch.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor,constant: 10)
            .isActive = true
        btnFetch.widthAnchor.constraint(equalToConstant: 40)
            .isActive = true
        btnFetch.heightAnchor.constraint(equalToConstant: 40)
            .isActive = true
        btnFetch.tintColor = GlobalSettings.shared().mainColor
        btnFetch.setBackgroundImage(UIImage(systemName: "gobackward"), for: .normal)
        btnFetch.addTarget(self, action: #selector(fetchReminders), for: .touchUpInside)
        
        
        self.view.addSubview(searchField)
        searchField.translatesAutoresizingMaskIntoConstraints = false
        searchField.leadingAnchor.constraint(equalTo: btnFetch.trailingAnchor)
            .isActive = true
        searchField.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: 10)
            .isActive = true
        searchField.topAnchor.constraint(equalTo: btnFetch.topAnchor)
            .isActive = true
        searchField.heightAnchor.constraint(equalToConstant: 40)
            .isActive = true
        
        
        
        
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            .isActive = true
        tableView.topAnchor.constraint(equalTo: searchField.bottomAnchor,constant: 10)
            .isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor)
            .isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            .isActive = true
        
        self.view.addSubview(btnCalendar)
        btnCalendar.translatesAutoresizingMaskIntoConstraints = false
        btnCalendar.addTarget(self, action: #selector(openCalendar), for: .touchUpInside)
        btnCalendar.tintColor = GlobalSettings.shared().mainColor
        btnCalendar.setBackgroundImage(UIImage(systemName: "calendar.circle.fill"), for: .normal)
        NSLayoutConstraint.activate([
            btnCalendar.widthAnchor.constraint(equalToConstant: 50),
            btnCalendar.heightAnchor.constraint(equalToConstant: 50),
            btnCalendar.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            btnCalendar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)])
        
    }
    
    
    
}



//MARK: - Side Menu


extension ReminderVC :SideMenuDelegate {
    func logoutButtonTouchUpInside() {
        let alert = UIAlertController(title: "خروج از برنامه!", message: "در صورت خروج همه ی اطلاعات شما حذف خواهد شد.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "خروج", style: .destructive) { _ in
            do {
                try FileManager.default.removeItem(at: User.userArchiveURL)
                User.resetUser()
                ReminderCollection.removeAllNotes()
                ReminderCollection.removeAllChecklists()
                self.sideMenuCoordinator?.toLoginView()
            } catch {
                print(error)
            }
        }
        alert.addAction(doneAction)
        let cancelAction = UIAlertAction(title: "انصراف", style: .default, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}


extension ReminderVC  {
    func showNoMatchFound() {
        tableView.isHidden = true
        emptyLabel.isHidden = false
    }
    func hideNoMatchFound() {
        tableView.isHidden = false
        emptyLabel.isHidden = true
    }
    
    func setDatesForCalendar() {
        for reminder in reminders {
            GlobalSettings.datesForCalendar.removeAll()
            GlobalSettings.datesForCalendar.append(reminder.date)
        }
    }
}

extension Date { /*:method for getting date in perian on Date Class */
    func stringDate() -> String {
        
        let formatter = DateFormatter()
       // formatter.dateFormat = "dd/MM/YYYY"
        // formatter.calendar = Calendar(identifier: .gregorian)
      //  let dateInGrogrian = formatter.string(from: self)
        formatter.calendar = Calendar(identifier: .persian)
        formatter.dateFormat = "YYYY/MM/dd"
        formatter.locale = Locale(identifier: "fa_IR")
        return formatter.string(from: self)
    }
    
    func stringTime() -> String {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateFormat = "HH:MM"
        return formatter.string(from: self)
    }
}

//MARK: - calendar delegate

extension ReminderVC :CalendarDelegate {
    func didSelectDate(date: Date) {
        var dateForTitle : String {
            return date.stringDate()
        }
        self.navigationItem.title =  " یادآورها در تاریخ" + " :" + " \(dateForTitle)"
        remindersToShow.removeAll()
        for reminder in reminders  {
            if reminder.date.stringDate() == date.stringDate() {
                remindersToShow.append(reminder)
                remindersToShow = remindersToShow.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
            }
        }
        reminderForSearch = remindersToShow
        if remindersToShow.isEmpty {
            showNoMatchFound()
        } else {
            hideNoMatchFound()
            tableView.reloadData()
        }
    }
}
//MARK: - search bar delegate



extension ReminderVC : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText.isEmpty == true) {
            remindersToShow = reminderForSearch.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
        } else {
            remindersToShow.removeAll()
            for reminder in reminderForSearch {
                if (reminder.title.uppercased().contains(searchText.uppercased())) {
                    remindersToShow.append(reminder)
                    remindersToShow = remindersToShow.sorted(by: {String($0.date.stringDate()) < String($1.date.stringDate())})
                }else{
                    
                }
            }
        }
        if remindersToShow.isEmpty {
            showNoMatchFound()
        }else {
            hideNoMatchFound()
            tableView.reloadData()
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.menuIcon.isHidden = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text!.isEmpty {
            guard let navigationController = navigationController as? RKNavigationController else {return}
            navigationController.menuIcon.isHidden = false

        }
    }
}


extension ReminderVC {
    
    fileprivate func getLabels() {
        let url = RKAPI.labelsURL
        AF.request(url, method: .get, headers: RKAPI.username).responseString { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                let n = jsonObject.count
                for i in 0..<n {
                    if let title = jsonObject[i]["Title"].string, let color = jsonObject[i]["Color"].int, let labelId = jsonObject[i]["LabelID"].string {
                        let label = Label(Title: title, Color: color, LabelID: labelId)
                        self.labelsDictionary[label.LabelID] = label
                    }
                }
                self.labels = Array(self.labelsDictionary.values)
                self.tableView.reloadData()
            case let .failure(error):
                print(error)
                
            }
        }
    }
}


