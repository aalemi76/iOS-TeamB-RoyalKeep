//
//  CalendarVC.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/30/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import UIKit
import FSCalendar_Persian


class CalendarVC :UIViewController, FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance {
    //MARK: - Properties
    let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: 320, height: 300))
    var reminders = [Reminder]()
    var delegate : CalendarDelegate?
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy - MM - dd"
        return formatter
    }()
    
    
    //MARK: - ViewCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBackground()
        calendar.dataSource = self
        calendar.delegate = self
        view.addSubview(calendar)
        calendar.center = view.center
        setCalendar()
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        calendar.center = view.center
    }
    

    func configureBackground(){
        let background = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        background.backgroundColor = GlobalSettings.shared().lightGray
        background.alpha = 0.4
        background.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(background)
        view.sendSubviewToBack(background)
    }
    //MARK: - Handlers
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        var dates :[String] {
            var dates = [String]()
            for reminder in reminders {
                let stringDate = dateFormatter.string(from: reminder.date)
                dates.append(stringDate)
            }
            return dates
        }
        if dates.contains(dateFormatter.string(from: date)) {
            return GlobalSettings.shared().mainColor
        }else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        var dates :[String] {
            var dates = [String]()
            for reminder in reminders {
                let stringDate = dateFormatter.string(from: reminder.date)
                dates.append(stringDate)
            }
            return dates
        }
        if dates.contains(dateFormatter.string(from: date)) {
            return .white
        }else{
            return nil
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print(date.stringDate())
        delegate?.didSelectDate(date: date)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { /*:touch outside of calendar dissmiss view */
        let touch = touches.first
        if touch?.view != calendar.contentView {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
}
extension CalendarVC {
    fileprivate func setCalendar() {
        self.calendar.locale = NSLocale.init(localeIdentifier: "fa-IR") as Locale
        self.calendar.calendarIdentifier = NSCalendar.Identifier.persian.rawValue
        self.calendar.firstWeekday = 7
        self.calendar.appearance.caseOptions = FSCalendarCaseOptions.weekdayUsesSingleUpperCase
        self.calendar.appearance.headerTitleColor = GlobalSettings.shared().lightGray
        self.calendar.appearance.headerTitleFont = GlobalSettings.shared().systemFont()
        self.calendar.appearance.weekdayFont = GlobalSettings.shared().systemFont()
        self.calendar.calendarHeaderView.backgroundColor = GlobalSettings.shared().mainColor
        self.calendar.backgroundColor = GlobalSettings.shared().lightGray
        self.calendar.appearance.headerMinimumDissolvedAlpha = 0.6
        self.calendar.layer.masksToBounds = true
        self.calendar.layer.cornerRadius = 5
        self.calendar.layer.borderWidth = 2
        self.calendar.layer.borderColor = GlobalSettings.shared().lightGray.cgColor
        self.calendar.appearance.todayColor = .none
    }
}
