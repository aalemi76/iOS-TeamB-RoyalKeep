//
//  RKAPI.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire

let NONE = "NONE"

struct RKAPI {
    static var baseURL: String {
        get {
            return "http://royalkeep-v3.eu-4.evennode.com/api/main"
        }
    }
    static var contentType: HTTPHeaders {
        get {
            return HTTPHeaders(["Content-Type": "application/json"])
        }
    }
    static var username: HTTPHeaders {
        get {
            return HTTPHeaders(["username": User.globalUsername])
        }
    }
    static var headers: HTTPHeaders {
        get {
            return HTTPHeaders(["username": User.globalUsername, "Content-Type": "application/json"])
        }
    }
    static var loginURL: String {
        get {
            return baseURL + "/login"
        }
    }
    static var labelsURL: String {
        get {
            return baseURL + "/labels"
        }
    }
    static var createLabelsURL: String {
        get {
            return baseURL + "/labels/create"
        }
    }
    static var updateLabelsURL: String {
        get {
            return baseURL + "/labels/update"
        }
    }
    static var deleteLabelsURL: String {
        get {
            return baseURL + "/labels/delete"
        }
    }
    static var notesURL: String {
        get {
            return baseURL + "/notes"
        }
    }
    static var createNotesURL: String {
        get {
            return baseURL + "/notes/create"
        }
    }
    static var updateNotesURL: String {
        get {
            return baseURL + "/notes/update"
        }
    }
    static var deleteNotesURL: String {
        get {
            return baseURL + "/notes/delete"
        }
    }
    static var noteDetailsURL: String {
        get {
            return baseURL + "/notes/note"
        }
    }
    static var notesSetCategory: String {
        get {
            return baseURL + "/notes/setCategory"
        }
    }
    static var checklistsURL: String {
        get {
            return baseURL + "/checkLists"
        }
    }
    static var createChecklistsURL: String {
        get {
            return baseURL + "/checklists/create"
        }
    }
    static var updateChecklistsURL: String {
        get {
            return baseURL + "/checklists/update"
        }
    }
    static var deletechecklistsURL: String {
        get {
            return baseURL + "/checklists/delete"
        }
    }
    static var checklistDetailsURL: String {
        get {
            return baseURL + "/checklists/checklist"
        }
    }
    static var checklistsSetCategory: String {
        get {
            return baseURL + "/checkLists/setCategory"
        }
    }
}


