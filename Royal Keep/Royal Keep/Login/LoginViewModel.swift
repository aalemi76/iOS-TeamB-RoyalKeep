//
//  LoginViewModel.swift
//  Royal Keep
//
//  Created by Catalina on 6/24/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class LoginViewModel {
    let contentView = UIView()
    let userLabel = UILabel()
    let passLabel = UILabel()
    let userTextField = UITextField()
    let passTextField = UITextField()
    let loginButton = UIButton(type: .system)
    let padding: CGFloat = 20
    init() {
        addContentView()
    }
    private func addContentView() {
        contentView.alpha = 0
        makeRound(view: contentView, cornerRadius: 20)
        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(blurredEffectView)
        NSLayoutConstraint.activate([
            blurredEffectView.topAnchor.constraint(equalTo: contentView.topAnchor),
            blurredEffectView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            blurredEffectView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            blurredEffectView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)])
        configure(label: userLabel, text: "نام کاربری :")
        addConstraint(label: userLabel, distance: -60)
        configure(label: passLabel, text: "رمز عبور :")
        addConstraint(label: passLabel, distance: 0)
        configure(textField: userTextField)
        addConstraint(textField: userTextField, distance: -60)
        configure(textField: passTextField)
        addConstraint(textField: passTextField, distance: 0)
        addLoginButton()
    }
    private func configure(label: UILabel, text: String) {
        label.text = text
        label.alpha = 0
        label.font = GlobalSettings.shared().boldSystemFont(size: 15)
        label.textColor = GlobalSettings.shared().lightGray
        label.textAlignment = .right
    }
    private func addConstraint(label: UILabel, distance: CGFloat) {
        label.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: distance),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -padding)])
    }
    private func configure(textField: UITextField) {
        textField.alpha = 0
        textField.autocapitalizationType = .none
        textField.borderStyle = .roundedRect
        textField.backgroundColor = GlobalSettings.shared().lightGray
        textField.textAlignment = .left
        textField.font = GlobalSettings.shared().boldSystemFont(size: 15)
    }
    private func addConstraint(textField: UITextField, distance: CGFloat) {
        textField.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(textField)
        NSLayoutConstraint.activate([
            textField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: distance),
            textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: padding),
            textField.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -130)])
    }
    private func addLoginButton() {
        loginButton.alpha = 0
        loginButton.isEnabled = false
        loginButton.setTitle("ورود", for: .normal)
        loginButton.setTitleColor(GlobalSettings.shared().darkGray, for: .normal)
        loginButton.backgroundColor = GlobalSettings.shared().lightGreen
        loginButton.titleLabel?.font = GlobalSettings.shared().boldSystemFont(size: 18)
        makeRound(view: loginButton, cornerRadius: 10)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(loginButton)
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: passTextField.bottomAnchor, constant: 30),
            loginButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            loginButton.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -70),
            loginButton.heightAnchor.constraint(equalToConstant: 50)])
    }
    private func makeRound(view: UIView, cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }
}
