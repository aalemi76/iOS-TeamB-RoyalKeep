//
//  LoginErrorView.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
struct LoginError: Codable {
    var error: Int
    var message: String
}
class LoginErrorView: UIView {
    let message = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        makeRound(view: self, cornerRadius: 10)
        backgroundColor = .systemRed
        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(blurredEffectView)
        NSLayoutConstraint.activate([
            blurredEffectView.topAnchor.constraint(equalTo: topAnchor),
            blurredEffectView.leadingAnchor.constraint(equalTo: leadingAnchor),
            blurredEffectView.trailingAnchor.constraint(equalTo: trailingAnchor),
            blurredEffectView.bottomAnchor.constraint(equalTo: bottomAnchor)])
        message.font = GlobalSettings.shared().boldSystemFont(size: 15)
        message.textColor = GlobalSettings.shared().lightGray
        message.textAlignment = .center
        message.translatesAutoresizingMaskIntoConstraints = false
        addSubview(message)
        NSLayoutConstraint.activate([
            message.centerYAnchor.constraint(equalTo: centerYAnchor),
            message.centerXAnchor.constraint(equalTo: centerXAnchor)])
    }
    private func makeRound(view: UIView, cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }
}
