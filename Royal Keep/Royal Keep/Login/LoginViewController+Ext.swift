//
//  LoginViewController+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case userTextField:
            passTextField.becomeFirstResponder()
            return true
        case passTextField:
            passTextField.resignFirstResponder()
            return true
        default:
            return true
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard userTextField.text?.count != 0 && passTextField.text?.count != 0 else {
            configueLoginButton(isEnabled: false)
            return}
        configueLoginButton(isEnabled: true)
        return
    }
}
