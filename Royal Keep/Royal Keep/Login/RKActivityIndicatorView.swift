//
//  ActivityIndicatorView.swift
//  Royal Keep
//
//  Created by Catalina on 6/27/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class RKActivityIndicatorView: UIView {
    let spinner = UIActivityIndicatorView(style: .large)
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configureView() {
        makeRound(view: self, cornerRadius: 10)
        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(blurredEffectView)
        NSLayoutConstraint.activate([
            blurredEffectView.topAnchor.constraint(equalTo: topAnchor),
            blurredEffectView.leadingAnchor.constraint(equalTo: leadingAnchor),
            blurredEffectView.trailingAnchor.constraint(equalTo: trailingAnchor),
            blurredEffectView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
    func showSpinner() {
        spinner.color = .white
        spinner.startAnimating()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        addSubview(spinner)
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor),
            spinner.widthAnchor.constraint(equalTo: widthAnchor, constant: -15),
            spinner.heightAnchor.constraint(equalTo: heightAnchor, constant: -15)])
    }
    func showCheckmark() {
        spinner.isHidden = true
        let checkmark = UIImageView(image: UIImage(systemName: "checkmark"))
        checkmark.tintColor = GlobalSettings.shared().lightGray
        checkmark.alpha = 0
        checkmark.translatesAutoresizingMaskIntoConstraints = false
        addSubview(checkmark)
        NSLayoutConstraint.activate([
            checkmark.centerXAnchor.constraint(equalTo: centerXAnchor),
            checkmark.centerYAnchor.constraint(equalTo: centerYAnchor),
            checkmark.widthAnchor.constraint(equalTo: widthAnchor, constant: -20),
            checkmark.heightAnchor.constraint(equalTo: heightAnchor, constant: -20)])
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn, animations: {
            checkmark.alpha = 1
        }, completion: nil)
    }
    func showXmark() {
        spinner.isHidden = true
        let xmark = UIImageView(image: UIImage(systemName: "xmark"))
        xmark.tintColor = GlobalSettings.shared().lightGray
        xmark.alpha = 0
        xmark.translatesAutoresizingMaskIntoConstraints = false
        addSubview(xmark)
        NSLayoutConstraint.activate([
            xmark.centerXAnchor.constraint(equalTo: centerXAnchor),
            xmark.centerYAnchor.constraint(equalTo: centerYAnchor),
            xmark.widthAnchor.constraint(equalTo: widthAnchor, constant: -20),
            xmark.heightAnchor.constraint(equalTo: heightAnchor, constant: -20)])
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseIn, animations: {
            xmark.alpha = 1
        }, completion: nil)
    }
    func dismiss() {
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseOut, animations: {
            self.alpha = 0
        }, completion: nil)
    }
    private func makeRound(view: UIView, cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }
}
