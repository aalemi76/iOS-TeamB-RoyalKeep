//
//  ActivityIndicatorView.swift
//  Royal Keep
//
//  Created by Catalina on 6/27/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class RKActivityIndicatorView: UIView {
    let spinner = UIActivityIndicatorView(style: .large)
    let checkmark = UIImageView(image: UIImage(systemName: "Checkmark"))
}
