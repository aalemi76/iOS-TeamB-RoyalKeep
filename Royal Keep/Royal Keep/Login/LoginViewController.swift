//
//  ViewController.swift
//  Royal Keep
//
//  Created by Erfan Iranpour on 6/15/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class LoginViewController: UIViewController, Storyboarded {
    //MARK:- Variables
    weak var coordinator: MainCoordinator?
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let model = LoginViewModel()
    let scrollView = UIScrollView()
    var contentView: UIView!
    var backgroundImage = UIImageView(image: UIImage(named: "Night"))
    var userLabel: UILabel!
    var passLabel: UILabel!
    var userTextField: UITextField!
    var passTextField: UITextField!
    var loginButton: UIButton!
    //MARK:- Actions
    @objc func viewDidTap(tapGestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 100, right: 0)
        }
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func loginButtonDidTap(sender: UIButton) {
        var activityView: RKActivityIndicatorView? = RKActivityIndicatorView()
        cofigureActivityIndicator(activityView: activityView!)
        activityView?.showSpinner()
        let url = RKAPI.loginURL
        let username = userTextField.text!
        let password = passTextField.text!
        AF.request(url, method: .post, parameters: ["Username": "\(username)", "Password": "\(password)"], encoder: JSONParameterEncoder.prettyPrinted, headers: RKAPI.contentType).responseString { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                if let errorCode = jsonObject["error"].int, let message = jsonObject["message"].string {
                    activityView?.showXmark()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        activityView?.dismiss()
                        activityView?.removeFromSuperview()
                        activityView = nil
                    }
                    let error = LoginError(error: errorCode, message: message)
                    self.showErrorAlert(message: error.message)
                }
                if let firstName = jsonObject["FirstName"].string, let lastName = jsonObject["LastName"].string {
                    activityView?.showCheckmark()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        activityView?.dismiss()
                        activityView?.removeFromSuperview()
                        activityView = nil
                    }
                    let user = User(firstName: firstName, lastName: lastName, username: username)
                    user.configureGlobals()
                    do {
                        try user.saveUser()
                    } catch {
                        print(error)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let noteCoordinator = NoteCoordinator(navigationController: self.navigationController!)
                        noteCoordinator.start()
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        do {
            if let user = try loadUser() {
                user.configureGlobals()
                let noteCoordinator = NoteCoordinator(navigationController: self.navigationController!)
                noteCoordinator.start()
            }
        } catch {
            print(error)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        addScrollView()
        addBackgroundImage()
        addContentView()
        animate()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewDidTap(tapGestureRecognizer:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isPortrait {
            scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height)
        } else {
            let height = view.bounds.height / 2 + 200
            scrollView.contentSize = CGSize(width: view.bounds.width, height: height)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    //MARK:- View Layout
    func loadUser() throws -> User? {
        let codedData = try Data(contentsOf: User.userArchiveURL)
        let user = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? User
        return user
    }
    func addBackgroundImage() {
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.alpha = 0
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(backgroundImage)
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func addScrollView() {
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        if UIDevice.current.orientation.isPortrait {
            scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height)
        } else {
            let height = view.bounds.height / 2 + 200
            scrollView.contentSize = CGSize(width: view.bounds.width, height: height)
        }
    }
    func addContentView() {
        contentView = model.contentView
        userLabel = model.userLabel
        userTextField = model.userTextField
        userTextField.delegate = self
        passLabel = model.passLabel
        passTextField = model.passTextField
        passTextField.isSecureTextEntry = true
        passTextField.delegate = self
        loginButton = model.loginButton
        loginButton.addTarget(self, action: #selector(loginButtonDidTap(sender:)), for: .touchUpInside)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor, constant: -30),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            contentView.heightAnchor.constraint(equalToConstant: 250)])
    }
    func configueLoginButton(isEnabled: Bool) {
        loginButton.isEnabled = isEnabled
        if isEnabled {
            loginButton.setTitleColor(GlobalSettings.shared().lightGray, for: .normal)
            loginButton.backgroundColor = GlobalSettings.shared().darkGreen
        } else {
            loginButton.setTitleColor(GlobalSettings.shared().darkGray, for: .normal)
            loginButton.backgroundColor = GlobalSettings.shared().lightGreen
        }
    }
    func showErrorAlert(message: String) {
        var text = ""
        switch message {
        case "Invalid username.":
            text = "نام کاربری اشتباه است."
        case "Invalid password.":
            text = "رمز عبور اشتباه است."
        default:
            text = message
        }
        var alert: LoginErrorView? = LoginErrorView()
        alert?.message.text = text
        alert?.translatesAutoresizingMaskIntoConstraints = false
        alert?.center.y = 0
        view.addSubview(alert!)
        NSLayoutConstraint.activate([
            alert!.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            alert!.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            alert!.heightAnchor.constraint(equalToConstant: 50)])
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            alert?.center.y += 80
            alert?.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 1, delay: 3, options: .curveEaseInOut, animations: {
            alert?.center.y -= 100
            alert?.alpha = 0
        }) { _ in
            alert?.removeFromSuperview()
            alert = nil
        }
    }
    func cofigureActivityIndicator(activityView: UIView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityView.widthAnchor.constraint(equalToConstant: 80),
            activityView.heightAnchor.constraint(equalToConstant: 80)])
    }
    //MARK:- Animation
    func animate() {
        UIView.animate(withDuration: 1, delay: 0, options: [], animations: {
            self.backgroundImage.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 2, delay: 1, options: [], animations: {
            self.contentView.alpha = 1
            self.contentView.center.y -= 30
            self.userLabel.alpha = 1
            self.userLabel.center.y -= 30
            self.userTextField.alpha = 1
            self.userTextField.center.y -= 30
            self.passLabel.alpha = 1
            self.passLabel.center.y -= 30
            self.passTextField.alpha = 1
            self.passTextField.center.y -= 30
            self.loginButton.alpha = 1
            self.loginButton.center.y -= 30
        }, completion: nil)
    }
}

