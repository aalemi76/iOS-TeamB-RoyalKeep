//
//  SceneDelegate.swift
//  Royal Keep
//
//  Created by Erfan Iranpour on 6/15/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import UserNotifications
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var coordinator: MainCoordinator?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let navController = RKNavigationController()
        ReminderCollection.loadNotes()
        ReminderCollection.loadChecklists()
        if let response = connectionOptions.notificationResponse {
            handleNotificationAction(response: response, navigationController: navController)
            let user = try? loadUser()
            user?.configureGlobals()
        } else {
            coordinator = MainCoordinator(navigationController: navController)
            coordinator?.start()
        }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { granted, error in
            if granted {
                UNUserNotificationCenter.current().delegate = self
                print("User gave permission")
            } else {
                print("Permission denied")
            }
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }

    func handleNotificationAction(response: UNNotificationResponse, navigationController: RKNavigationController) {
        let category = response.notification.request.content.categoryIdentifier
        let info = response.notification.request.content.userInfo
        guard let indexPath = info["indexPath"] as? Int else {return}
        if category == "Note" {
            let note = ReminderCollection.notes[indexPath]
            if note?.category == 2 {
                let archiveCoordinator = ArchiveCoordinator(navigationController: navigationController)
                archiveCoordinator.toNoteFromNotification(indexPath: indexPath)
            } else {
                let noteCoordinator = NoteCoordinator(navigationController: navigationController)
                noteCoordinator.startFromNotification(indexPath: indexPath)
            }
        } else if category == "Checklist" {
            let checklist = ReminderCollection.checklists[indexPath]
            if checklist?.Category == 2 {
                let archiveCoordinator = ArchiveCoordinator(navigationController: navigationController)
                archiveCoordinator.toChecklistFromNotification(index: indexPath)
            } else{
                let checklistCoordinator = ChecklistCoordinator(navigationController: navigationController)
                checklistCoordinator.startFromNotification(index: indexPath)
            }
        }
    }
    func loadUser() throws -> User? {
        let codedData = try Data(contentsOf: User.userArchiveURL)
        let user = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? User
        return user
    }

}

extension SceneDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        guard let rootViewController = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.window?.rootViewController as? RKNavigationController else {return}
        ReminderCollection.loadNotes()
        ReminderCollection.loadChecklists()
        handleNotificationAction(response: response, navigationController: rootViewController)
        completionHandler()
    }
}
