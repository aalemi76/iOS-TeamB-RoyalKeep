//
//  AppDelegate.swift
//  Royal Keep
//
//  Created by Erfan Iranpour on 6/15/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var coordinator: MainCoordinator?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 13, *) {
            return true
        }
        let navController = RKNavigationController()
        coordinator = MainCoordinator(navigationController: navController)
        coordinator?.start()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        ReminderCollection.loadNotes()
        ReminderCollection.loadChecklists()
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound]) { granted, error in
            if granted {
                print("User gave permission")
                UNUserNotificationCenter.current().delegate = self
            } else {
                print("Permission denied")
            }
        }
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let navController = RKNavigationController()
        let category = response.notification.request.content.categoryIdentifier
        let info = response.notification.request.content.userInfo
        if category == "Note" {
            let noteCoordinator = NoteCoordinator(navigationController: navController)
            noteCoordinator.start()
        } else if category == "Checklist" {
            let checklistCoordinator = ChecklistCoordinator(navigationController: navController)
            checklistCoordinator.start()
        }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
        ReminderCollection.loadNotes()
        ReminderCollection.loadChecklists()
    }
}
