//
//  Error.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/7/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import Foundation
enum Error:Swift.Error {
    case notes
    case notesIsEmpty
    case checkListIsEmpty
    case checklistTitle
    case checklistDate
}
