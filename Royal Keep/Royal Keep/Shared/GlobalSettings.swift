//
//  GlobalSetting.swift
//  Royal Keep
//
//  Created by Catalina on 6/24/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class GlobalSettings {
    let mainColor = UIColor(displayP3Red: 108/255, green: 60/255, blue: 196/255, alpha: 1)
    let darkGreen = UIColor(displayP3Red: 0/255, green: 229/255, blue: 89/255, alpha: 0.8)
    let darkTitleGreen = UIColor(displayP3Red: 2/255, green: 180/255, blue: 66/255, alpha: 1)
    let lightGreen = UIColor(displayP3Red: 170/255, green: 255/255, blue: 154/255, alpha: 1)
    let lightGray = UIColor(displayP3Red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
    let darkGray = UIColor(displayP3Red: 145/255, green: 145/255, blue: 145/255, alpha: 1)
    let lightGrayInt = 16119285
    private static var sharedGlobalSettings: GlobalSettings = {
      let globalSettings = GlobalSettings()
      return globalSettings
    }()
    static var datesForCalendar = [Date]()
    class func shared() -> GlobalSettings {
      return sharedGlobalSettings
    }
    
    private func calculateFontSize() -> CGFloat {
      let fontSize : CGFloat = 13;
      var newFontSize : CGFloat = UIScreen.main.bounds.size.height * CGFloat(fontSize/568)
      if (UIScreen.main.bounds.size.height < 500) {
        newFontSize = UIScreen.main.bounds.size.height * CGFloat(CGFloat(fontSize) / 480.0);
      }
      let maxFontSize : CGFloat = 18;
      if (newFontSize>maxFontSize) {
        newFontSize = maxFontSize;
      }
      return newFontSize;
    }
    func systemFont() -> UIFont {
      return UIFont(name: "IRANSansMobile", size:self.calculateFontSize())!
    }
    func lightSystemFont() -> UIFont {
      return UIFont(name: "IRANSansMobile-Light", size:self.calculateFontSize())!
    }
    func mediumSystemFont() -> UIFont {
      return UIFont(name: "IRANSansMobile-Medium", size:self.calculateFontSize())!
    }
    func ultraLightSystemFont() -> UIFont {
      return UIFont(name: "IRANSansMobile-UltraLight", size:self.calculateFontSize())!
    }
    func boldSystemFont() -> UIFont {
      return UIFont.init(name: "IRANSansMobile-Bold", size:self.calculateFontSize()+2)!
    }
    func systemFont(size:CGFloat) -> UIFont {
      return UIFont.init(name: "IRANSansMobile", size:size)!
    }
    func lightSystemFont(size: CGFloat) -> UIFont {
      return UIFont(name: "IRANSansMobile-Light", size:size)!
    }
    func mediumSystemFont(size: CGFloat) -> UIFont {
      return UIFont(name: "IRANSansMobile-Medium", size:size)!
    }
    func ultraLightSystemFont(size: CGFloat) -> UIFont {
      return UIFont(name: "IRANSansMobile-UltraLight", size:size)!
    }
    func boldSystemFont(size:CGFloat) -> UIFont {
      return UIFont.init(name: "IRANSansMobile-Bold", size:size+2)!
    }
}
