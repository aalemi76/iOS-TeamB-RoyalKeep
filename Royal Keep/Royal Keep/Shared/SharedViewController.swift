//
//  SharedViewController.swift
//  Royal Keep
//
//  Created by Catalina on 6/30/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SharedViewController: UIViewController {
    var sideMenuCoordinator: SideMenuCoordinator?
    let sideMenu = UIView()
    let sideMenuVC = SideMenuViewController()
    var isSideMenuOpened: Bool = false
    var viewComponents = [UIView]()
    var width: CGFloat = 0
    @objc func openSideMenu() {
        guard isSideMenuOpened == false else {return}
        isSideMenuOpened = true
        sideMenuCoordinator = SideMenuCoordinator(navigationController: navigationController!)
        sideMenuVC.coordinator = sideMenuCoordinator
        UIView.animate(withDuration: 0.35, delay: 0.1, options: .curveLinear, animations: {
            guard var navigationbarFrame = self.navigationController?.navigationBar.frame else { return }
            navigationbarFrame.origin.x -= self.width
            self.moveVCComponent(forViews: self.viewComponents, distance: self.width, isReturned: false)
            self.navigationController?.navigationBar.frame = navigationbarFrame
            var sidebarFrame = self.sideMenu.frame
            sidebarFrame.origin.x -= self.width
            self.sideMenu.frame = sidebarFrame
        }, completion: { finished in
        })
    }
    @objc func closeSideMenu() {
        guard isSideMenuOpened == true else {return}
        isSideMenuOpened = false
        sideMenuCoordinator = nil
        UIView.animate(withDuration: 0.35, delay: 0.1, options: .curveLinear, animations: {
            guard var navigationbarFrame = self.navigationController?.navigationBar.frame else { return }
            var viewFrame = self.sideMenu.frame
            navigationbarFrame.origin.x += self.width
            self.moveVCComponent(forViews: self.viewComponents, distance: self.width, isReturned: true)
            self.navigationController?.navigationBar.frame = navigationbarFrame
            viewFrame.origin.x += self.width
            self.sideMenu.frame = viewFrame
        }, completion: { finished in
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        width = min(view.bounds.width, view.bounds.height) * 0.75
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.customNavDelegate = self
        navigationItem.setHidesBackButton(true, animated: false)
    }
    func setViewComponents(forViews views: [UIView]) {
        for i in 0..<views.count {
            viewComponents.append(views[i])
        }
    }
    func configureSideMenu() {
        addChild(sideMenuVC)
        sideMenu.addSubview(sideMenuVC.view)
        sideMenuVC.view.frame = sideMenu.bounds
        sideMenuVC.didMove(toParent: self)
        sideMenu.isUserInteractionEnabled = true
        view.addSubview(sideMenu)
        sideMenu.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
          sideMenu.topAnchor.constraint(equalTo: view.topAnchor),
          sideMenu.bottomAnchor.constraint(equalTo: view.bottomAnchor),
          sideMenu.leadingAnchor.constraint(equalTo: view.trailingAnchor),
          sideMenu.widthAnchor.constraint(equalToConstant: width)
        ])
        configureSwipeLeft()
        configureSwipeRight()
    }
    func configureSwipeLeft() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(openSideMenu))
       swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
       view.addGestureRecognizer(swipeLeft)
     }
    func configureSwipeRight() {
       let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(closeSideMenu))
      swipeRight.direction = UISwipeGestureRecognizer.Direction.right
      view.addGestureRecognizer(swipeRight)
    }
    func moveVCComponent(forViews views: [UIView], distance: CGFloat, isReturned: Bool) {
        if isReturned {
            for view in views {
                view.isUserInteractionEnabled = true
                view.frame.origin.x += distance
            }
        } else {
            for view in views {
                view.isUserInteractionEnabled = false
                view.frame.origin.x -= distance
            }
        }
    }
}
extension SharedViewController: RKNavigationControllerDelegate {
    func menuIconTouchUpInside() {
        if !isSideMenuOpened {
            isSideMenuOpened = true
            sideMenuCoordinator = SideMenuCoordinator(navigationController: navigationController!)
            sideMenuVC.coordinator = sideMenuCoordinator
            UIView.animate(withDuration: 0.35, delay: 0.1, options: .curveLinear, animations: {
                guard var navigationbarFrame = self.navigationController?.navigationBar.frame else { return }
                navigationbarFrame.origin.x -= self.width
                self.moveVCComponent(forViews: self.viewComponents, distance: self.width, isReturned: false)
                self.navigationController?.navigationBar.frame = navigationbarFrame
                var sidebarFrame = self.sideMenu.frame
                sidebarFrame.origin.x -= self.width
                self.sideMenu.frame = sidebarFrame
            }, completion: { finished in
            })
        } else {
            isSideMenuOpened = false
            sideMenuCoordinator = nil
            UIView.animate(withDuration: 0.35, delay: 0.1, options: .curveLinear, animations: {
                guard var navigationbarFrame = self.navigationController?.navigationBar.frame else { return }
                var viewFrame = self.sideMenu.frame
                navigationbarFrame.origin.x += self.width
                self.moveVCComponent(forViews: self.viewComponents, distance: self.width, isReturned: true)
                self.navigationController?.navigationBar.frame = navigationbarFrame
                viewFrame.origin.x += self.width
                self.sideMenu.frame = viewFrame
            }, completion: { finished in
            })
        }
    }
}
