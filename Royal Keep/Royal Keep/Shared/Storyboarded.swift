//
//  Storyboarded.swift
//  Royal Keep
//
//  Created by Catalina on 6/29/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
protocol Storyboarded {
    static func instantiate() -> Self
}
extension Storyboarded where Self: UIViewController {
    static func instantiate() -> Self {
        let fullName = NSStringFromClass(self)
        let className = fullName.components(separatedBy: ".")[1]
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}
