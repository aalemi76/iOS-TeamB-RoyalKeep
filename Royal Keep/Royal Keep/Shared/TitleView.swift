//
//  TitleView.swift
//  Royal Keep
//
//  Created by Catalina on 6/30/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class TitleView: UIView {
    let xmarkIcon = UIImageView(image: UIImage(systemName: "xmark"))
    let checkmarkIcon = UIImageView(image: UIImage(systemName: "checkmark"))
    let editIcon = UIImageView(image: UIImage(systemName: "square.and.pencil"))
    let backIcon = UIImageView(image: UIImage(systemName: "arrow.left"))
    let title = UILabel()
    let text: String
    let menuIconWidth: CGFloat = 35
    let itemHeight: CGFloat = 35
    init(title: String) {
        text = title
        super.init(frame: .zero)
        configureView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configureView() {
        backgroundColor = GlobalSettings.shared().mainColor
        title.text = text
        title.font = GlobalSettings.shared().boldSystemFont(size: 16)
        title.textColor = GlobalSettings.shared().lightGray
        title.textAlignment = .center
        title.translatesAutoresizingMaskIntoConstraints = false
        addSubview(title)
        NSLayoutConstraint.activate([
            title.centerXAnchor.constraint(equalTo: centerXAnchor),
            title.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)])
    }
    func addXmarkIcon() {
        xmarkIcon.translatesAutoresizingMaskIntoConstraints = false
        addSubview(xmarkIcon)
        NSLayoutConstraint.activate([
            xmarkIcon.centerYAnchor.constraint(equalTo: title.centerYAnchor),
            xmarkIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            xmarkIcon.widthAnchor.constraint(equalToConstant: 30),
            xmarkIcon.heightAnchor.constraint(equalToConstant: 30)])
        xmarkIcon.contentMode = .scaleAspectFit
        xmarkIcon.tintColor = GlobalSettings.shared().lightGray
        xmarkIcon.isUserInteractionEnabled = true
    }
    func addCheckmarkIcon() {
        checkmarkIcon.translatesAutoresizingMaskIntoConstraints = false
        addSubview(checkmarkIcon)
        NSLayoutConstraint.activate([
            checkmarkIcon.centerYAnchor.constraint(equalTo: title.centerYAnchor),
            checkmarkIcon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            checkmarkIcon.widthAnchor.constraint(equalToConstant: 30),
            checkmarkIcon.heightAnchor.constraint(equalToConstant: 30)])
        checkmarkIcon.contentMode = .scaleAspectFit
        checkmarkIcon.tintColor = GlobalSettings.shared().lightGray
        checkmarkIcon.isUserInteractionEnabled = true
    }
    func addEditIcon() {
        editIcon.isHidden = false
        editIcon.contentMode = .scaleAspectFit
        editIcon.tintColor = GlobalSettings.shared().lightGray
        editIcon.isUserInteractionEnabled = true
        editIcon.translatesAutoresizingMaskIntoConstraints = false
        addSubview(editIcon)
        NSLayoutConstraint.activate([
            editIcon.centerYAnchor.constraint(equalTo: title.centerYAnchor),
            editIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            editIcon.widthAnchor.constraint(equalToConstant: menuIconWidth),
            editIcon.heightAnchor.constraint(equalToConstant: itemHeight)])
    }
    func addBackIcon() {
        backIcon.isHidden = false
        backIcon.contentMode = .scaleAspectFit
        backIcon.tintColor = GlobalSettings.shared().lightGray
        backIcon.isUserInteractionEnabled = true
        backIcon.translatesAutoresizingMaskIntoConstraints = false
        addSubview(backIcon)
        NSLayoutConstraint.activate([
            backIcon.centerYAnchor.constraint(equalTo: title.centerYAnchor),
            backIcon.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            backIcon.widthAnchor.constraint(equalToConstant: menuIconWidth),
            backIcon.heightAnchor.constraint(equalToConstant: itemHeight)])
    }
}
