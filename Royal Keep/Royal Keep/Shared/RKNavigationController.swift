//
//  RKNavigationController.swift
//  Royal Keep
//
//  Created by Catalina on 6/25/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
protocol RKNavigationControllerDelegate: class {
    func menuIconTouchUpInside()
}
class RKNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    var menuIcon = UIImageView(image: UIImage(systemName: "line.horizontal.3"))
    let editIcon = UIImageView(image: UIImage(systemName: "square.and.pencil"))
    let backIcon = UIImageView(image: UIImage(systemName: "arrow.left"))
    let label = UILabel()
    let padding: CGFloat = 20
    let menuIconWidth: CGFloat = 35
    let labelWidth: CGFloat = 110
    let itemHeight: CGFloat = 35
    weak var customNavDelegate : RKNavigationControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    @objc func menuIconDidTap(tapGestureRecognizer: UITapGestureRecognizer) {
        customNavDelegate?.menuIconTouchUpInside()
    }
    func configureView() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: GlobalSettings.shared().lightGray, NSAttributedString.Key.font: GlobalSettings.shared().boldSystemFont(size: 18)]
        navigationItem.setHidesBackButton(true, animated: false)
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = GlobalSettings.shared().mainColor
    }
    func addMenuIcon() {
        menuIcon.isHidden = false
        menuIcon.contentMode = .scaleAspectFit
        menuIcon.tintColor = GlobalSettings.shared().lightGray
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(menuIconDidTap(tapGestureRecognizer:)))
        menuIcon.isUserInteractionEnabled = true
        menuIcon.addGestureRecognizer(tapGestureRecognizer)
        menuIcon.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.addSubview(menuIcon)
        NSLayoutConstraint.activate([
            menuIcon.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
            menuIcon.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -15),
            menuIcon.widthAnchor.constraint(equalToConstant: menuIconWidth),
            menuIcon.heightAnchor.constraint(equalToConstant: itemHeight)])
    }
    func addLabel(with title: String) {
        label.textColor = .white
        navigationBar.addSubview(label)
        label.text = title
        label.textAlignment = .left
        label.font = GlobalSettings.shared().boldSystemFont()
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
            label.leadingAnchor.constraint(equalTo: menuIcon.trailingAnchor, constant: padding),
            label.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -padding),
            label.heightAnchor.constraint(equalToConstant: itemHeight)
        ])
    }
    func addEditIcon() {
        editIcon.isHidden = false
        editIcon.contentMode = .scaleAspectFit
        editIcon.tintColor = GlobalSettings.shared().lightGray
        editIcon.isUserInteractionEnabled = true
        editIcon.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.addSubview(editIcon)
        NSLayoutConstraint.activate([
            editIcon.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
            editIcon.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -15),
            editIcon.widthAnchor.constraint(equalToConstant: menuIconWidth),
            editIcon.heightAnchor.constraint(equalToConstant: itemHeight)])
    }
    func addBackIcon() {
        backIcon.isHidden = false
        backIcon.contentMode = .scaleAspectFit
        backIcon.tintColor = GlobalSettings.shared().lightGray
        backIcon.isUserInteractionEnabled = true
        backIcon.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.addSubview(backIcon)
        NSLayoutConstraint.activate([
            backIcon.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
            backIcon.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor, constant: 15),
            backIcon.widthAnchor.constraint(equalToConstant: menuIconWidth),
            backIcon.heightAnchor.constraint(equalToConstant: itemHeight)])
    }
}
