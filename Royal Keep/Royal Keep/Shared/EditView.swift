//
//  EditView.swift
//  Royal Keep
//
//  Created by Catalina on 7/18/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class EditView: UIView {
    let editButton = UIButton(type: .system)
    let deleteButton = UIButton(type: .system)
    let selectButton = UIButton(type: .system)
    let archiveButton = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBorder()
        backgroundColor = GlobalSettings.shared().lightGray
        configure(button: editButton, image: UIImage(systemName: "square.and.pencil"), title: "ویرایش")
        configure(button: deleteButton, image: UIImage(systemName: "trash"), title: "حذف")
        configure(button: selectButton, image: UIImage(systemName: "checkmark.square"), title: "انتخاب همه")
        NSLayoutConstraint.activate([
            editButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            selectButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            deleteButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)])
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configure(button: UIButton, image: UIImage?, title: String?) {
        button.tintColor = GlobalSettings.shared().mainColor
        button.titleLabel?.font = GlobalSettings.shared().lightSystemFont(size: 15)
        button.titleLabel?.textAlignment = .right
        button.setTitle(title, for: .normal)
        button.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        button.setImage(image, for: .normal)
        button.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        button.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        button.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -5, bottom: -7, right: 0.0)
    }
    func addArchiveButton() {
        configure(button: archiveButton, image: UIImage(systemName: "archivebox.fill"), title: "آرشیو")
        archiveButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
        deleteButton.removeFromSuperview()
    }
    func addBorder() {
        let topBorder = UIView()
        topBorder.backgroundColor = GlobalSettings.shared().darkGray
        topBorder.translatesAutoresizingMaskIntoConstraints = false
        addSubview(topBorder)
        let bottomBorder = UIView()
        bottomBorder.backgroundColor = GlobalSettings.shared().darkGray
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomBorder)
        NSLayoutConstraint.activate([
            topBorder.topAnchor.constraint(equalTo: topAnchor),
            topBorder.leadingAnchor.constraint(equalTo: leadingAnchor),
            topBorder.trailingAnchor.constraint(equalTo: trailingAnchor),
            topBorder.heightAnchor.constraint(equalToConstant: 1)])
        NSLayoutConstraint.activate([
            bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor),
            bottomBorder.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomBorder.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomBorder.heightAnchor.constraint(equalToConstant: 1)])
    }
}
