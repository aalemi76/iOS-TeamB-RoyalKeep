//
//  SideMenuSegue.swift
//  Royal Keep
//
//  Created by Catalina on 6/29/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SideMenuSegue: UIStoryboardSegue {
    override func perform() {
        let transition = CATransition()
        transition.duration = 1
        transition.type = .moveIn
        transition.subtype = .fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: .easeIn)
        self.destination.modalPresentationStyle = .overFullScreen
        
        self.source.view.window?.layer.add(transition, forKey: kCATransition)
        self.source.present(self.destination, animated: false, completion: nil)
    }
}
