//
//  CustomDatePickerViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/12/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class CustomDatePickerViewController: UIViewController {
    var content: UIView!
    var pickerView: UIDatePicker!
    var done: UIButton!
    var remove: UIButton!
    var date: Date?
    var cancel: UIButton!
    var viewModel = CustomDatePickerViewModel()
    var removeCompletion: ((CustomDatePickerViewController)->Void)?
    var doneCompletion: ((CustomDatePickerViewController, Date)->Void)?
    var cancelCompletion: ((CustomDatePickerViewController)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapOnView(tapGestureRecognizer:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        configureBackground()
        configureContent()
        configureCancelButton()
        configureDoneButton()
        configureRemoveButton()
        configurePickerView()
    }
    @objc func didTapRemoveButton(sender: UIButton) {
        removeCompletion?(self)
    }
    @objc func didTapDoneButton(sender: UIButton) {
        date = pickerView.date
        doneCompletion?(self, date!)
    }
    @objc func didTapCancelButton(sender: UIButton) {
        cancelCompletion?(self)
    }
    @objc func didTapOnView(tapGestureRecognizer: UITapGestureRecognizer){
        let point = tapGestureRecognizer.location(in: self.view)
        if !pickerView.frame.contains(point) && !cancel.frame.contains(point) {
            dismiss(animated: true, completion: nil)
        }
    }
    func configureBackground(){
        let background = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
        background.backgroundColor = .lightGray
        background.alpha = 0.6
        background.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(background)
        view.sendSubviewToBack(background)
    }
    func configureContent(){
        content = viewModel.content
        view.addSubview(content)
        NSLayoutConstraint.activate([
            content.topAnchor.constraint(equalTo: view.topAnchor),
            content.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            content.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            content.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func configureCancelButton(){
        cancel = viewModel.cancel
        cancel.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
    }
    func configureDoneButton(){
        done = viewModel.done
        done.addTarget(self, action: #selector(didTapDoneButton), for: .touchUpInside)
    }
    func configureRemoveButton() {
        remove = viewModel.remove
        if date != nil {
            remove.isEnabled = true
            remove.setTitleColor(.systemRed, for: .normal)
        } else {
            remove.isEnabled = false
            remove.setTitleColor(GlobalSettings.shared().darkGray, for: .normal)
        }
        remove.addTarget(self, action: #selector(didTapRemoveButton(sender:)), for: .touchUpInside)
    }
    func configurePickerView(){
        pickerView = viewModel.pickerView
        pickerView.minimumDate = Date()
        if let date = date {
            pickerView.setDate(date, animated: true)
        }
    }
}

