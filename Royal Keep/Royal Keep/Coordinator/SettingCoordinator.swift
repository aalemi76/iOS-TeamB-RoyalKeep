//
//  SettingCoordinator.swift
//  Royal Keep
//
//  Created by Catalina on 7/10/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SettingCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let vc = SettingsViewController.instantiate()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toLabelsView() {
        let vc = LabelsViewController.instantiate()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
}
