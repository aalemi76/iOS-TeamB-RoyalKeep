//
//  MainCoordinator.swift
//  Royal Keep
//
//  Created by Catalina on 6/29/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let vc = LoginViewController.instantiate()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
}
