//
//  NoteCoordinator.swift
//  Royal Keep
//
//  Created by Catalina on 7/3/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class NoteCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let vc = NotesViewController.instantiate()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func startFromNotification(indexPath: Int?) {
        let vc = NotesViewController.instantiate()
        vc.coordinator = self
        vc.mainIndex = indexPath
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toNoteDetailsView(_ viewController: UIViewController, noteId: String, indexPath: IndexPath, noteLabel: Label?, labels: [Label], mainIndex: Int) {
        let vc = NoteDetailsViewController.instantiate()
        vc.delegate = viewController.self as? NoteDetailsViewControllerDelegate
        vc.noteId = noteId
        vc.indexPath = indexPath
        vc.mainIndex = mainIndex
        vc.noteLabel = noteLabel
        vc.labels = labels
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toLoginView() {
        let coordinator = MainCoordinator(navigationController: navigationController)
        coordinator.start()
    }
}
