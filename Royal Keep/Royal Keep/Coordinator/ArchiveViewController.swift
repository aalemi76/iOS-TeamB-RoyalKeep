//
//  ArchiveViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/20/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class ArchiveViewController: SharedViewController {
    var coordinator: ArchiveCoordinator?
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    //MARK:- Actions
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSideMenu()
        sideMenuVC.delegate = self
        setViewComponents(forViews: [collectionView])
        configureCollectionView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "آرشیو"
        navigationController.menuIcon.isHidden = false
        navigationController.addMenuIcon()
        navigationController.backIcon.isHidden = true
    }
    func configureCollectionView() {
        let img = UIImageView(image: UIImage(named: "AfterNoon"))
        img.contentMode = .scaleAspectFill
        collectionView.backgroundView = img
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        collectionView.register(ArchiveCollectionViewCell.self, forCellWithReuseIdentifier: ArchiveCollectionViewCell.reuseID)
    }
}
//MARK:- Extensions
extension ArchiveViewController: SideMenuDelegate {
    func logoutButtonTouchUpInside() {
        let alert = UIAlertController(title: "خروج از برنامه!", message: "در صورت خروج همه ی اطلاعات شما حذف خواهد شد.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "خروج", style: .destructive) { _ in
            do {
                try FileManager.default.removeItem(at: User.userArchiveURL)
                User.resetUser()
                self.sideMenuCoordinator?.toLoginView()
            } catch {
                print(error)
            }
        }
        alert.addAction(doneAction)
        let cancelAction = UIAlertAction(title: "انصراف", style: .default, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}
extension ArchiveViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.bounds.width - 30)
        return CGSize(width: width, height: 120)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArchiveCollectionViewCell.reuseID, for: indexPath) as! ArchiveCollectionViewCell
        switch indexPath.row {
        case 0:
            cell.img.image = UIImage(systemName: "folder.fill")
            cell.titleLabel.text = "یادداشت های آرشیو شده"
            return cell
        case 1:
            cell.img.image = UIImage(systemName: "checkmark.square.fill")
            cell.titleLabel.text = "چک لیست های آرشیو شده"
            return cell
        default:
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            coordinator?.toNoteView()
        case 1:
            coordinator?.toChecklistView()
        default:
            return
        }
    }
}

