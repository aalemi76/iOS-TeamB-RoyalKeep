//
//  ReminderCoordinator.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/28/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import UIKit

class ReminderCoordinator : Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
       print("reminder vc called")
        let vc = ReminderVC.instantiate()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
}
