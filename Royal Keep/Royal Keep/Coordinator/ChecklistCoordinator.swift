//
//  ChecklistCoordinator.swift
//  Royal Keep
//
//  Created by Catalina on 7/4/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class ChecklistCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let vc = ChecklistsViewController.instantiate()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func startFromNotification(index: Int?) {
        let vc = ChecklistsViewController.instantiate()
        vc.coordinator = self
        vc.mainIndex = index
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toChecklistDetails(_ viewController: UIViewController, checklistID: String, indexPath: IndexPath, checklistLabel: Label?, labels: [Label], mainIndex: Int) {
        let vc = ChecklistDetailsViewController.instantiate()
        vc.delegate = viewController.self as? ChecklistDetailsViewControllerDelegate
        vc.checklistID = checklistID
        vc.indexPath = indexPath
        vc.mainIndex = mainIndex
        vc.checkListLabel = checklistLabel
        vc.labels = labels
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toLoginView() {
        let coordinator = MainCoordinator(navigationController: navigationController)
        coordinator.start()
    }
}

