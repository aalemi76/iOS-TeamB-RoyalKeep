//
//  Coordinator.swift
//  Royal Keep
//
//  Created by Catalina on 6/29/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
}
