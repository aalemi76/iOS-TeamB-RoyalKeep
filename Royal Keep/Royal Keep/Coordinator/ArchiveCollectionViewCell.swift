//
//  ArchiveCollectionViewCell.swift
//  Royal Keep
//
//  Created by Catalina on 7/21/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class ArchiveCollectionViewCell: UICollectionViewCell {
    static let reuseID = "ArchiveCollectionViewCell"
    var img: UIImageView = {
        let temp = UIImageView()
        temp.contentMode = .scaleAspectFit
        return temp
    }()
    let titleLabel: UILabel = {
        let temp = UILabel()
        temp.font = GlobalSettings.shared().boldSystemFont(size: 20)
        temp.textColor = GlobalSettings.shared().darkGray
        temp.textAlignment = .center
        return temp
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        img.tintColor = GlobalSettings.shared().mainColor
        img.translatesAutoresizingMaskIntoConstraints = false
        addSubview(img)
        NSLayoutConstraint.activate([
            img.centerYAnchor.constraint(equalTo: centerYAnchor),
            img.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
            img.widthAnchor.constraint(equalToConstant: 50),
            img.heightAnchor.constraint(equalToConstant: 50)])
        backgroundColor = GlobalSettings.shared().lightGray
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: img.centerYAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: img.leadingAnchor, constant: -10)])
        makeRounded(self, cornerRadius: 10)
    }
    private func makeRounded(_ view: UIView,cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
}
