//
//  SideMenuCoordinator.swift
//  Royal Keep
//
//  Created by Catalina on 7/4/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SideMenuCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        return
    }
    func toLoginView() {
        let coordinator = MainCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    func toNoteView() {
        let coordinator = NoteCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    func toCheckListsView() {
        let coordinator = ChecklistCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    func toArchiveView() {
        let coordinator = ArchiveCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    func toSettingsView() {
        let coordinator = SettingCoordinator(navigationController: navigationController)
        coordinator.start()
    }
    func toRemindersView() {
        let coordinator = ReminderCoordinator(navigationController: navigationController)
        coordinator.start()
    }
}
