//
//  ArchiveCoordinator.swift
//  Royal Keep
//
//  Created by Catalina on 7/20/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class ArchiveCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let vc = ArchiveViewController()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toNoteView() {
        let vc = ArchiveNotesVC()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toNoteFromNotification(indexPath: Int?) {
        let vc = ArchiveNotesVC()
        vc.coordinator = self
        vc.mainIndex = indexPath
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toNoteDetailsView(_ viewController: UIViewController, noteId: String, indexPath: IndexPath, noteLabel: Label?, labels: [Label], mainIndex: Int) {
        let vc = ArchiveNoteDetailsVC()
        vc.delegate = viewController.self as? ArchiveNoteDetailsVCDelegate
        vc.noteId = noteId
        vc.indexPath = indexPath
        vc.mainIndex = mainIndex
        vc.noteLabel = noteLabel
        vc.labels = labels
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toChecklistView() {
        let vc = ArchiveChecklistsVC()
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toChecklistFromNotification(index: Int?) {
        let vc = ArchiveChecklistsVC()
        vc.coordinator = self
        vc.mainIndex = index
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
    func toChecklistDetails(_ viewController: UIViewController, checklistID: String, indexPath: IndexPath, checklistLabel: Label?, labels: [Label], mainIndex: Int) {
        let vc = ArchiveChecklistDetailsVC()
        vc.delegate = viewController.self as? ArchiveChecklistDetailsVCDelegate
        vc.checklistID = checklistID
        vc.indexPath = indexPath
        vc.mainIndex = mainIndex
        vc.checkListLabel = checklistLabel
        vc.labels = labels
        vc.coordinator = self
        vc.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController.pushViewController(vc, animated: false)
    }
}
