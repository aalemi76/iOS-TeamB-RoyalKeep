//
//  SideMenuCell.swift
//  Royal Keep
//
//  Created by Catalina on 6/28/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SideMenuCell: UITableViewCell {
    static var reuseId = "SideMenuCell"
    let icon = UIImageView()
    let label = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        backgroundColor = GlobalSettings.shared().lightGray
        label.font = GlobalSettings.shared().boldSystemFont()
        label.textColor = GlobalSettings.shared().darkGray
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        icon.tintColor = GlobalSettings.shared().darkGray
        icon.translatesAutoresizingMaskIntoConstraints = false
        addSubview(icon)
        NSLayoutConstraint.activate([
            icon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            icon.centerYAnchor.constraint(equalTo: centerYAnchor),
            icon.widthAnchor.constraint(equalToConstant: 30),
            icon.heightAnchor.constraint(equalToConstant: 30),
            label.trailingAnchor.constraint(equalTo: icon.leadingAnchor, constant: -15),
            label.centerYAnchor.constraint(equalTo: centerYAnchor)])
    }
}
