//
//  SideMenuViewController.swift
//  Royal Keep
//
//  Created by Catalina on 6/28/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
protocol SideMenuDelegate: class {
    func logoutButtonTouchUpInside()
}
class SideMenuViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    weak var coordinator: SideMenuCoordinator?
    let model = SideMenuViewModel()
    let scrollView = UIScrollView()
    var contentView: UIView!
    var avatar: UIImageView!
    var name: UILabel!
    var tableView: UITableView!
    var logoutButton: UIButton!
    var width: CGFloat = 0
    var height: CGFloat = 0
    let tableViewTitles = ["یادداشت ها", "چک لیست ها", "يادآور", "آرشیو", "تنظیمات"]
    let tableViewImages = [UIImage(systemName: "folder.fill"), UIImage(systemName: "checkmark.square.fill"), UIImage(systemName: "calendar"), UIImage(systemName: "archivebox.fill"), UIImage(named: "Settings")]
    let rowHeight: CGFloat = 55
    weak var delegate: SideMenuDelegate?
    @objc func didTapLogoutButton(sender: UIButton) {
        delegate?.logoutButtonTouchUpInside()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isPortrait {
            scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height)
        } else {
            scrollView.contentSize = CGSize(width: view.bounds.height, height: 550)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        width = min(view.bounds.width, view.bounds.height)
        height = max(view.bounds.width, view.bounds.height)
        addScrollView()
        addContentView()
        addConponents()
    }
    private func addScrollView() {
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        if UIDevice.current.orientation.isPortrait {
            scrollView.contentSize = CGSize(width: width, height: height)
        } else {
            scrollView.contentSize = CGSize(width: view.bounds.height, height: 550)
        }
    }
    private func addContentView() {
        contentView = model.contentView
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    private func addConponents() {
        avatar = model.avatar
        name = model.name
        name.text = "\(User.globalFirstName) \(User.globalLastName)"
        tableView = model.tableView
        tableView.delegate = self
        tableView.dataSource = self
        logoutButton = model.logoutButton
        logoutButton.addTarget(self, action: #selector(didTapLogoutButton(sender:)), for: .touchUpInside)
    }
}
