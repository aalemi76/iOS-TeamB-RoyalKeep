//
//  SideMenuView+Extension.swift
//  Royal Keep
//
//  Created by Catalina on 6/28/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewTitles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.reuseId, for: indexPath) as! SideMenuCell
        cell.icon.image = tableViewImages[indexPath.row]
        cell.label.text = tableViewTitles[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            coordinator?.toNoteView()
        case 1:
            coordinator?.toCheckListsView()
        case 2:
            coordinator?.toRemindersView()
        case 3:
            coordinator?.toArchiveView()
        case 4:
            coordinator?.toSettingsView()
        default:
            return
        }
    }
}

