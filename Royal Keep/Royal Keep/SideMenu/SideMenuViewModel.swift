//
//  SideMenuViewModel.swift
//  Royal Keep
//
//  Created by Catalina on 6/28/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SideMenuViewModel {
    let contentView = UIView()
    let topView = UIView()
    let avatar = UIImageView()
    let avatarHeight: CGFloat = 70
    let name = UILabel()
    let tableView = UITableView(frame: .zero, style: .plain)
    let logoutButton = UIButton(type: .system)
    init() {
        addContentView()
    }
    private func addContentView() {
        contentView.backgroundColor = .systemGray5
        addTopView()
        addTableView()
        addLogoutButton()
    }
    private func addTopView() {
        contentView.backgroundColor = GlobalSettings.shared().lightGray
        topView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(topView)
        NSLayoutConstraint.activate([
            topView.topAnchor.constraint(equalTo: contentView.topAnchor),
            topView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            topView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            topView.heightAnchor.constraint(equalToConstant: 200)])
        let bg = UIImageView(image: UIImage(named: "Night"))
        bg.contentMode = .scaleAspectFill
        bg.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(bg)
        NSLayoutConstraint.activate([
            bg.topAnchor.constraint(equalTo: topView.topAnchor),
            bg.leadingAnchor.constraint(equalTo: topView.leadingAnchor),
            bg.trailingAnchor.constraint(equalTo: topView.trailingAnchor),
            bg.bottomAnchor.constraint(equalTo: topView.bottomAnchor)])
        makeRounded(view: avatar, cornerRadius: avatarHeight/2)
        avatar.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(avatar)
        NSLayoutConstraint.activate([
            avatar.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
            avatar.centerYAnchor.constraint(equalTo: topView.centerYAnchor),
            avatar.widthAnchor.constraint(equalToConstant: avatarHeight),
            avatar.heightAnchor.constraint(equalToConstant: avatarHeight)])
        name.font = GlobalSettings.shared().boldSystemFont(size: 17)
        name.textColor = GlobalSettings.shared().lightGray
        name.textAlignment = .right
        name.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(name)
        NSLayoutConstraint.activate([
            name.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
            name.topAnchor.constraint(equalTo: avatar.bottomAnchor, constant: 10)])
    }
    private func addTableView() {
        tableView.isScrollEnabled = false
        tableView.backgroundColor = GlobalSettings.shared().lightGray
        tableView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            tableView.heightAnchor.constraint(equalToConstant: 275)])
        tableView.register(SideMenuCell.self, forCellReuseIdentifier: SideMenuCell.reuseId)
    }
    private func addLogoutButton() {
        logoutButton.setTitle("خروج", for: .normal)
        logoutButton.setTitleColor(GlobalSettings.shared().lightGray, for: .normal)
        logoutButton.titleLabel?.font = GlobalSettings.shared().boldSystemFont(size: 17)
        logoutButton.backgroundColor = GlobalSettings.shared().mainColor
        makeRounded(view: logoutButton, cornerRadius: 5)
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(logoutButton)
        NSLayoutConstraint.activate([
            logoutButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            logoutButton.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 30),
            logoutButton.heightAnchor.constraint(equalToConstant: 40),
            logoutButton.widthAnchor.constraint(equalToConstant: 120)])
    }
    private func makeRounded(view: UIView, cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().lightGray.cgColor
        view.layer.borderWidth = 1
    }
}
