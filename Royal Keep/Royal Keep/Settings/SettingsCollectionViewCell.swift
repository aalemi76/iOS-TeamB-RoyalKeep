//
//  SettingsCollectionViewCell.swift
//  Royal Keep
//
//  Created by Catalina on 7/11/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SettingsCollectionViewCell: UICollectionViewCell {
    static let reuseID = "SettingsCollectionViewCell"
    var img: UIImageView = {
        let temp = UIImageView()
        temp.contentMode = .scaleAspectFit
        return temp
    }()
    let titleLabel: UILabel = {
        let temp = UILabel()
        temp.font = GlobalSettings.shared().boldSystemFont(size: 15)
        temp.textColor = GlobalSettings.shared().darkGray
        temp.textAlignment = .center
        return temp
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        backgroundColor = GlobalSettings.shared().lightGray
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)])
        img.translatesAutoresizingMaskIntoConstraints = false
        addSubview(img)
        NSLayoutConstraint.activate([
            img.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            img.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            img.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5),
            img.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -5)])
        makeRounded(self, cornerRadius: 10)
    }
    private func makeRounded(_ view: UIView,cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
}
