//
//  ReminderCollection.swift
//  Royal Keep
//
//  Created by Catalina on 7/12/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import UserNotifications
struct ReminderCollection {
    static var remindedNotesURL: URL = {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0].appendingPathComponent("remindedNotes.archive")
    }()
    static var remindedChecklistsURL: URL = {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0].appendingPathComponent("remindedChecklist.archive")
    }()
    static var notes = [Int: Note]()
    static var checklists = [Int: Checklist]()
    static func saveNotes() {
        do {
            let codedData = try NSKeyedArchiver.archivedData(withRootObject: ReminderCollection.notes, requiringSecureCoding: false)
            try codedData.write(to: ReminderCollection.remindedNotesURL)
        } catch {
            print("Error saving notes: \(error.localizedDescription)")
        }
    }
    static func saveChecklists() {
        do {
            let codedData = try NSKeyedArchiver.archivedData(withRootObject: ReminderCollection.checklists, requiringSecureCoding: false)
            try codedData.write(to: ReminderCollection.remindedChecklistsURL)
        } catch {
            print("Error saving notes: \(error.localizedDescription)")
        }
    }
    @discardableResult static func loadNotes() -> [Int: Note]? {
        do {
            let codedData = try Data(contentsOf: ReminderCollection.remindedNotesURL)
            guard let dictionary = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? [Int: Note] else { return nil }
            notes = dictionary
            print(dictionary)
            return dictionary
        } catch {
            print("Error loading notes: \(error.localizedDescription)")
            return nil
        }
    }
    @discardableResult static func loadChecklists() -> [Int: Checklist]? {
        do {
            let codedData = try Data(contentsOf: ReminderCollection.remindedChecklistsURL)
            guard let dictionary = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? [Int: Checklist] else { return nil }
            checklists = dictionary
            print(dictionary)
            return dictionary
        } catch {
            print("Error loading notes: \(error.localizedDescription)")
            return nil
        }
    }
    static func getNote(indexPath: Int) -> Note? {
        if let note = notes[indexPath] {
            return note
        } else {
            return nil
        }
    }
    static func getChecklist(indexPath: Int) -> Checklist? {
        if let checklist = checklists[indexPath] {
            return checklist
        } else {
            return nil
        }
    }
    static func setReminder(noteList: [Note]) {
        for (key, value) in notes {
            noteList[key].reminderDate = value.reminderDate
        }
    }
    static func setReminder(checklists: [Checklist]) {
        for (key, value) in self.checklists {
            checklists[key].reminderDate = value.reminderDate
        }
    }
    static func updateIndex(noteList: [Note]) {
        notes.removeAll()
        for (i, note) in noteList.enumerated() {
            notes.updateValue(note, forKey: i)
        }
        saveNotes()
    }
    static func update(noteList: [Note]) {
        notes.removeAll()
        for (i, note) in noteList.enumerated() {
            if note.reminderDate != nil {
                notes.updateValue(note, forKey: i)
            }
        }
        saveNotes()
    }
    static func update(checklists: [Checklist]) {
        self.checklists.removeAll()
        for (i, checklist) in checklists.enumerated() {
            if checklist.reminderDate != nil {
                self.checklists.updateValue(checklist, forKey: i)
            }
        }
        saveChecklists()
    }
    static func updateIndex(checklists: [Checklist]) {
        self.checklists.removeAll()
        for (i, checklist) in checklists.enumerated() {
            self.checklists.updateValue(checklist, forKey: i)
        }
        saveChecklists()
    }
    static func scheduleNotification(note: Note, indexPath: Int) {
        guard note.reminderDate! > Date() else {return}
        removeNotification(note: note, indexPath: indexPath)
        let content = UNMutableNotificationContent()
        content.categoryIdentifier = "Note"
        content.userInfo = ["indexPath": indexPath]
        content.title = note.title!
        content.body = note.noteDescription ?? ""
        content.sound = .default
        let calender = Calendar(identifier: .gregorian)
        let components = calender.dateComponents([.year, .month, .day, .hour, .minute], from: note.reminderDate!)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: note.noteId!, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { error in
            print("Notification Error: \(error?.localizedDescription)")
        }
        notes.updateValue(note, forKey: indexPath)
    }
    static func scheduleNotification(checklist: Checklist, indexPath: Int) {
        guard checklist.reminderDate! > Date() else {return}
        removeNotification(checklist: checklist, indexPath: indexPath)
        let content = UNMutableNotificationContent()
        content.categoryIdentifier = "Checklist"
        content.userInfo = ["indexPath": indexPath]
        content.title = checklist.Title!
        content.body = checklist.Description ?? ""
        content.sound = .default
        let calender = Calendar(identifier: .gregorian)
        let components = calender.dateComponents([.year, .month, .day, .hour, .minute], from: checklist.reminderDate!)
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: checklist.CheckListID!, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { error in
            print("Notification Error: \(error?.localizedDescription)")
        }
        checklists.updateValue(checklist, forKey: indexPath)
    }
    static func removeNotification(note: Note, indexPath: Int) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [note.noteId!])
        notes.removeValue(forKey: indexPath)
    }
    static func removeNotification(checklist: Checklist, indexPath: Int) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [checklist.CheckListID!])
        checklists.removeValue(forKey: indexPath)
    }
    static func removeAllNotes() {
        for note in notes.values {
            let center = UNUserNotificationCenter.current()
            center.removePendingNotificationRequests(withIdentifiers: [note.noteId!])
        }
        notes.removeAll()
        do {
            try FileManager.default.removeItem(at: ReminderCollection.remindedNotesURL)
        } catch {
            print(error)
        }
    }
    static func removeAllChecklists() {
        for checklist in checklists.values {
            let center = UNUserNotificationCenter.current()
            center.removePendingNotificationRequests(withIdentifiers: [checklist.CheckListID!])
        }
        checklists.removeAll()
        do {
            try FileManager.default.removeItem(at: ReminderCollection.remindedChecklistsURL)
        } catch {
            print(error)
        }
    }
}
