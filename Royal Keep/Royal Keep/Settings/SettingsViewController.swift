//
//  SettingsViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/11/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class SettingsViewController: SharedViewController, Storyboarded {
    //MARK:- Variables
    var coordinator: SettingCoordinator?
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    //MARK:- Actions
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureSideMenu()
        sideMenuVC.delegate = self
        setViewComponents(forViews: [collectionView])
        configureNavigationBar()
        configureCollectionView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "تنظیمات"
        navigationController.menuIcon.isHidden = false
        navigationController.addMenuIcon()
        navigationController.backIcon.isHidden = true
    }
    func configureCollectionView() {
        let img = UIImageView(image: UIImage(named: "AfterNoon"))
        img.contentMode = .scaleAspectFill
        collectionView.backgroundView = img
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        collectionView.register(SettingsCollectionViewCell.self, forCellWithReuseIdentifier: SettingsCollectionViewCell.reuseID)
    }
}
//MARK:- Extensions
extension SettingsViewController: SideMenuDelegate {
    func logoutButtonTouchUpInside() {
        let alert = UIAlertController(title: "خروج از برنامه!", message: "در صورت خروج همه ی اطلاعات شما حذف خواهد شد.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "خروج", style: .destructive) { _ in
            do {
                try FileManager.default.removeItem(at: User.userArchiveURL)
                User.resetUser()
                self.sideMenuCoordinator?.toLoginView()
            } catch {
                print(error)
            }
        }
        alert.addAction(doneAction)
        let cancelAction = UIAlertAction(title: "انصراف", style: .default, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}
extension SettingsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (min(view.bounds.width, view.bounds.height) - 60 - 30) / 2
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingsCollectionViewCell.reuseID, for: indexPath) as! SettingsCollectionViewCell
        switch indexPath.row {
        case 0:
            cell.img.image = UIImage(named: "share")
            cell.titleLabel.text = "اشتراک گذاری"
            return cell
        case 1:
            cell.img.image = UIImage(named: "label")
            cell.titleLabel.text = "برچسب ها"
            return cell
        case 2:
            cell.img.image = UIImage(named: "diamond")
            cell.titleLabel.text = "نسخه نرم افزار"
            return cell
        default:
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            return
        case 1:
            coordinator?.toLabelsView()
        case 2:
            return
        default:
            return
        }
    }
}
