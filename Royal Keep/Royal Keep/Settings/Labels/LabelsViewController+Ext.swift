//
//  LabelsViewController+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 7/10/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
extension LabelsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 25, left: 20, bottom: 25, right: 20)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = min(view.bounds.width, view.bounds.height) - 40
        return CGSize(width: width, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return labels.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let label = labels[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LabelCollectionViewCell.reuseID, for: indexPath) as! LabelCollectionViewCell
        cell.titleLabel.text = label.Title
        let hex = String(label.Color, radix: 16)
        cell.topView.backgroundColor = UIColor.init(hex: hex)
        cell.bottomView.backgroundColor = UIColor.init(hex: hex)
        cell.inEditingMode = inEditingMode
        cell.isCellSelected = selectedItems.contains(indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let label = labels[indexPath.row]
        let cell = collectionView.cellForItem(at: indexPath) as! LabelCollectionViewCell
        if inEditingMode {
            if cell.isCellSelected {
                selectedItems = selectedItems.filter { $0 != indexPath.row }
                print(selectedItems)
                isAllSelected = false
            } else {
                selectedItems.append(indexPath.row)
                selectedItems.count == labels.count ?  isAllSelected = true : ()
            }
            cell.isCellSelected = !cell.isCellSelected
        } else {
            sendData(label: label, selectedColor: label.Color, indexPath: indexPath)
        }
    }
    func sendData(label: Label, selectedColor: Int, indexPath: IndexPath) {
        let addLabelVC = AddLabelViewController()
        addLabelVC.itemToEdit = label
        addLabelVC.labels = self.labels
        addLabelVC.delegate = self
        addLabelVC.selectedColor = label.Color
        addLabelVC.indexPath = indexPath
        addLabelVC.modalPresentationStyle = .overFullScreen
        addLabelVC.modalTransitionStyle = .crossDissolve
        present(addLabelVC, animated: true, completion: nil)
    }
}
 
extension LabelsViewController: AddLabelViewControllerDelegate {
    func addNewLabel(_ controller: AddLabelViewController, didAddItem item: Label?, at indexPath: IndexPath?) {
//        if let indexPath = indexPath {
//            labels.remove(at: indexPath.item)
//            labels.insert(item!, at: indexPath.item)
//        } else {
//            self.labels.insert(item!, at: 0)
//            isLabelAvailable = true
//        }
//        collectionView.reloadData()
        getLabels()
    }
}

