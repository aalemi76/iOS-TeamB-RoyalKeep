//
//  LabelsViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/9/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class LabelsViewController: UIViewController, Storyboarded {
//MARK:- Variables
    var coordinator: SettingCoordinator?
    var backIcon: UIImageView!
    let editView = EditView()
    var editButton, selectButton, deleteButton: UIButton!
    let addButton = UIButton(type: .system)
    var labelsDictionary = [String: Label]()
    var labels = [Label]()
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    var checklists = [Checklist]()
    var notes      = [Note]()
    var noteLabels = Set<String>()
    var checklsitLabels  = Set<String>()
   // var selectedLabelsID = Set<String>()
    
    let noLabel: UILabel = {
        let label = UILabel()
        label.text = "موردی برای نمایش یافت نشد"
        label.font = GlobalSettings.shared().boldSystemFont(size: 15)
        label.textColor = GlobalSettings.shared().darkGray
        label.textAlignment = .center
        return label
    }()
    var isLabelAvailable: Bool = false {
        willSet {
            collectionView.isHidden = !newValue
            noLabel.isHidden = newValue
        }
    }
    var isAllSelected: Bool = false {
        willSet {
            if newValue {
                selectButton.setTitle("غیر فعال کردن همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                selectButton.setTitle("انتخاب همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            }
        }
    }
    var inEditingMode: Bool = false {
        willSet {
            addButton.isHidden = newValue
            deleteButton.isHidden = !newValue
            selectButton.isHidden = !newValue
            newValue ? editButton.setTitle("انصراف", for: .normal) : editButton.setTitle("ویرایش", for: .normal)
        }
    }
    var selectedItems = [Int]()
//MARK:- Actions
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    @objc func addButtonDidTap(sender: UIButton) {
        let vc = AddLabelViewController.instantiate()
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.labels = self.labels
        present(vc, animated: true, completion: nil)
    }
    
    @objc func didTapEditButton(sender: UIButton) {
        print("Hey didTapEditButton")
        selectedItems.removeAll()
        isAllSelected = false
        inEditingMode = !inEditingMode
        collectionView.reloadData()
    }
    @objc func didTapSelectButton(sender: UIButton) {
        print("Hey didTapSelectButton")
        print(selectedItems)
        if selectedItems.count < labels.count {
            selectedItems = Array(0..<labels.count)
            isAllSelected = true
            collectionView.reloadData()
        } else {
            selectedItems.removeAll()
            isAllSelected = false
            collectionView.reloadData()
        }
    }
    @objc func didTapDeleteButton(sender: UIButton) {
        print("Main label")
        print(labels)
        print("Main label end")
        let rseult = prepareRemovableLabel()
        let alertController = UIAlertController(title: "حذف برچسب", message: "", preferredStyle: .alert)
        if selectedItems.isEmpty {
            alertController.message = "لطفا ابتدا برچسبی را انتخاب نمایید"
            let action = UIAlertAction(title: "فهمیدم", style: .default, handler: { action in alertController.dismiss(animated: true, completion: nil)})
            alertController.addAction(action)
        } else if rseult.1.count == 0 {
            alertController.message = "این برچسب ها در جایی دیگر استفاده شده اند و قابل حذف نیستند ."
            let action = UIAlertAction(title: "فهمیدم", style: .cancel, handler: { action in alertController.dismiss(animated: true, completion: nil)})
            alertController.addAction(action)
        } else if rseult.1.count == selectedItems.count {
            for label in rseult.removable {
                self.deleteLabels(labelID: label) {
                print(label)
                    self.selectedItems.removeAll()
                    self.inEditingMode = false
                    self.isAllSelected = false
                    self.getLabels()
                }
            }
            alertController.message = "همه برچسب ها با موفقیت حذف شدند ."
            let action = UIAlertAction(title: "فهمیدم", style: .cancel, handler: { action in alertController.dismiss(animated: true, completion: nil)})
            alertController.addAction(action)
        } else {
            for label in rseult.removable {
                self.deleteLabels(labelID: label) {
                print(label)
                    self.selectedItems.removeAll()
                    self.inEditingMode = false
                    self.isAllSelected = false
                    self.getLabels()
                }
            }
            alertController.message = "برخی برچسب ها به دلیل استفاده قبلی نتوانستند حذف شوند."
            let action = UIAlertAction(title: "فهمیدم", style: .cancel, handler: { action in alertController.dismiss(animated: true, completion: nil)})
            alertController.addAction(action)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
//MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNoLabelMessage()
        configureEditView()
        configureCollectionView()
        configureButton()
        getLabels()
        getCheckList()
        getNotes()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
    }
//MARK:- Web API
    func getLabels() {
        var activityView: RKActivityIndicatorView? = RKActivityIndicatorView()
        cofigureActivityIndicator(activityView: activityView!)
        activityView?.showSpinner()
        let url = RKAPI.labelsURL
        AF.request(url, method: .get, headers: RKAPI.username).responseString { response in
            switch response.result {
            case .success:
                activityView?.showCheckmark()
                let jsonObject = JSON(response.data ?? Data())
                let n = jsonObject.count
                n != 0 ? (self.isLabelAvailable = true) : (self.isLabelAvailable = false)
                for i in 0..<n {
                    if let title = jsonObject[i]["Title"].string, let color = jsonObject[i]["Color"].int, let labelId = jsonObject[i]["LabelID"].string {
                        let label = Label(Title: title, Color: color, LabelID: labelId)
                        self.labelsDictionary[label.LabelID] = label
                    }
                }
                self.labels = Array(self.labelsDictionary.values)
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.collectionView.reloadData()
                    activityView?.dismiss()
                    activityView?.removeFromSuperview()
                    activityView = nil
                }
            case let .failure(error):
                print(error)
                self.isLabelAvailable = false
                activityView?.showXmark()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    activityView?.dismiss()
                    activityView?.removeFromSuperview()
                    activityView = nil
                }
            }
        }
       
    }
    func cofigureActivityIndicator(activityView: UIView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityView.widthAnchor.constraint(equalToConstant: 80),
            activityView.heightAnchor.constraint(equalToConstant: 80)])
    }
//MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: false)
         navigationItem.title = "برچسب ها"
        navigationController.menuIcon.isHidden = true
        navigationController.addBackIcon()
        backIcon = navigationController.backIcon
        let backTap = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon(tapGestureRecognizer:)))
        backIcon.addGestureRecognizer(backTap)
        navigationController.backIcon.isHidden = false
    }
    func configureEditView() {
        editView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(editView)
        NSLayoutConstraint.activate([
            editView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            editView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            editView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            editView.heightAnchor.constraint(equalToConstant: 40)])
        editButton = editView.editButton
        editButton.addTarget(self, action: #selector(didTapEditButton(sender:)), for: .touchUpInside)
        selectButton = editView.selectButton
        selectButton.isHidden = true
        selectButton.addTarget(self, action: #selector(didTapSelectButton(sender:)), for: .touchUpInside)
        deleteButton = editView.deleteButton
        deleteButton.isHidden = true
        deleteButton.addTarget(self, action: #selector(didTapDeleteButton(sender:)), for: .touchUpInside)
    }
    func configureButton() {
        addButton.setBackgroundImage(UIImage(systemName: "plus.circle.fill"), for: .normal)
        addButton.tintColor = GlobalSettings.shared().mainColor
        addButton.addTarget(self, action: #selector(addButtonDidTap(sender:)), for: .touchUpInside)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(addButton)
        NSLayoutConstraint.activate([
            addButton.widthAnchor.constraint(equalToConstant: 50),
            addButton.heightAnchor.constraint(equalToConstant: 50),
            addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)])
    }
    func configureCollectionView() {
        let img = UIImageView(image: UIImage(named: "AfterNoon"))
        img.contentMode = .scaleAspectFill
        img.alpha = 0.8
        collectionView.backgroundView = img
        collectionView.backgroundColor = GlobalSettings.shared().lightGray
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: editView.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        collectionView.register(LabelCollectionViewCell.self, forCellWithReuseIdentifier: LabelCollectionViewCell.reuseID)
    }
    func configureNoLabelMessage() {
        noLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(noLabel)
        NSLayoutConstraint.activate([
            noLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            noLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)])
    }
//MARK:- Helper Method
    
    func deleteLabels(labelID: String, hanlder: @escaping ()->Void) {
        let url = RKAPI.deleteLabelsURL
        AF.request(url, method: .delete, parameters: ["LabelID": labelID], encoding: URLEncoding(destination: .queryString), headers: RKAPI.username).responseString{ response in
            switch response.result {
            case .success:
                debugPrint(response)
                self.removeLabelInDataSource(labelID: labelID)
                let jsonObject = JSON(response.data ?? Data())
                if let _ = jsonObject["error"].int {
                    let alert = UIAlertController(title: "خطا در برقراری ارتباط با سرور!", message: "مشکلی در برقراری ارتباط با سرور بوجود آمده است. لطفا پس از اطمینان از اتصال با اینرنت دوباره تلاش کنید.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "متوجه شدم", style: .default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    hanlder()
                }
            case .failure:
                let alert = UIAlertController(title: "خطا در برقراری ارتباط با سرور!", message: "مشکلی در برقراری ارتباط با سرور بوجود آمده است. لطفا پس از اطمینان از اتصال با اینرنت دوباره تلاش کنید.", preferredStyle: .alert)
                let action = UIAlertAction(title: "متوجه شدم", style: .default, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    func removeLabelInDataSource(labelID: String) {
        for (index, label) in labels.enumerated() {
            if label.LabelID == labelID {
                print("THIS Item or index will removed \(index) -- \(label)")
                labels.remove(at: index)
            }
        }
    }
    func getCheckList() {
        AF.request(checklistManager.url , headers: HTTPHeaders(["username": User.globalUsername])).responseJSON { (data) in
            do {
                self.checklists = try checklistManager.setFinalArrayOfModels(data: data.data!)
                self.checklsitLabels = self.filter(checklists: self.checklists)
                print("==============")
                print(self.checklsitLabels)
            }catch Error.checkListIsEmpty {
                print("ERROR: In getChecklist")
            }
            catch {
                print(error)
            }
        }
    }
    func getNotes() {
        AF.request(notesManager.url , headers: HTTPHeaders(["username": User.globalUsername])).responseJSON { (data) in
            do {
                self.notes = try notesManager.setFinalArrayOfModels(data: data.data!)
                self.noteLabels =  self.filter(notes: self.notes)
            }catch Error.notesIsEmpty {
                print("Error in get not")
            }
            catch {
                print(error)
            }
        }
    }
    func filter(checklists: [Checklist]) -> Set<String> {
        var result = [String]()
        for checkList in checklists {
            if let labelID = checkList.LabelID {
                result.append(labelID)
            }
        }
        let newResult = Set(result)
        return newResult
    }
    
    func filter(notes: [Note]) -> Set<String> {
        var result = [String]()
        for note in notes {
            if let labelID = note.labelId {
                result.append(labelID)
            }
        }
        let newResult = Set(result)
        return newResult
    }

    func getSelectedLabelID() -> Set<String> {
        var selctedLabelsID = [String]()
        for index in selectedItems {
            selctedLabelsID.append(labels[index].LabelID)
        }
        return Set(selctedLabelsID)
    }
    
    func prepareRemovableLabel() -> (selectedInUse: Set<String>, removable: Set<String>) {
        let allUsedLabel = noteLabels.union(checklsitLabels)
        let selectedInUseLabel = getSelectedLabelID().intersection(allUsedLabel)
        let removableLabel = getSelectedLabelID().subtracting(selectedInUseLabel)

        return (selectedInUseLabel, removableLabel)
    }
}


/**
 
 
 
 
 func prepareRemovableLabel() -> (Set<String>, String) {
     let allUsedLabel = noteLabels.union(checklsitLabels)
     let selectedInUseLabel = getSelectedLabelID().intersection(allUsedLabel)
     let removableLabel = getSelectedLabelID().subtracting(selectedInUseLabel)
     if !selectedInUseLabel.isEmpty {
         return (removableLabel, "برخی برچسب ها به دلیل استفاده قبلی قابل حذف نبودند")
     } else if selectedInUseLabel.isEmpty {
         return (removableLabel, "لیبل ها با موفقیت حدف شدند ")
     } else {
         return (removableLabel, "برجسب های انتخابی قابل حدف نیستند")
     }
 }
 
 
 
 @objc func didTapDeleteButton(sender: UIButton) {
     
     print("Main labels is ")
     print(labels)
     print("Main label endsق")
     let alertController = UIAlertController(title: "حذف برچسب", message: "آیا میخواهبد این برجسب ها را حدف کیند ", preferredStyle: .alert)
     let actionOk = UIAlertAction(title: "بلی", style: .destructive, handler: { alert in
         let removableLabel = self.prepareRemovableLabel()
         if !removableLabel.0.isEmpty {
             for label in removableLabel.0 {
                 self.deleteLabels(labelID: label) {
                     self.selectedItems.removeAll()
                     self.inEditingMode = false
                     self.isAllSelected = false
                     self.getLabels()
                 }
             }
         } else {
             let alertController = UIAlertController(title: "حذف برچسب", message: "ارایه قابل حذفی وجود ندارد", preferredStyle: .alert)
             let approveAction = UIAlertAction(title: "فهمیدم", style: .default, handler: { alert in
                 alertController.dismiss(animated: true, completion: nil)
             })
             alertController.addAction(approveAction)
             self.present(alertController, animated: true, completion: nil)
         }
     })
     let cancelAction = UIAlertAction(title: "لغو", style: .default, handler: nil)
     alertController.addAction(actionOk)
     alertController.addAction(cancelAction)
     present(alertController, animated: true, completion: nil)
     
 }
 
 
 print("++++++++++++")
 print("allUsedLabel: \(allUsedLabel)")
 
 print("++++++++++++")
 print("selectedInUseLabel: \(selectedInUseLabel)")
 
 print("++++++++++++")
 print("removableLabel: \(removableLabel)")
 */


/**@objc func didTapDeleteButton(sender: UIButton) {
    
   
    print(selectedItems)
    print("========== Labels: ")
    print(labels)
    print("========== Used Label In Checklist: \(checklsitLabels.count)")
    print(checklsitLabels)
    print("==========  Used Label In Notes:\(noteLabels.count) ")
    print(noteLabels)
    print("========== Selected Label: ")
    print(getSelectedLabelID())
    print("#########################################################")
   print(prepareRemovableLabel())
}
  */

