//
//  RKButtonColor.swift
//  Popup And CollectionView
//
//  Created by ArmanG on 4/21/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

class RKButtonColor: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        let radius: CGFloat = self.bounds.height / 2

        self.layer.cornerRadius = radius
    }
    
    func configure() {
        self.addTarget(self, action: #selector(btnColorTapped), for: .touchUpInside)
        self.isSelected = false
        self.setImage(UIImage(named: "selectColor_icn"), for: .selected)
        self.setImage(nil, for: .normal)
        self.setImage(UIImage(named:"forbidden_icn"), for: .disabled)
        self.layer.borderColor = UIColor.white.cgColor
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected == false {
                self.layer.borderWidth = 0.0
            } else {
                self.layer.borderWidth = 3.0
            }
        }
        
    }
    @objc func btnColorTapped(_ sender: UIButton) {
        if isSelected {
            isSelected = !isSelected
        } else {
            isSelected = !isSelected
        }
       
    }

}
