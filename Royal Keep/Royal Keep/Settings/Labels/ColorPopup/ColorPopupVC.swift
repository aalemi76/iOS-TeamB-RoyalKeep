//
//  SecondViewController.swift
//  Half Popup ViewController
//
//  Created by ArmanG on 8/17/1398 AP.
//  Copyright © 1398 Pars Digital. All rights reserved.
//

import UIKit
protocol ColorPickerDelegate: class {
    func colorPicker(color number: Int, bgColor: UIColor)
}
 class ColorPopupVC: UIViewController {
    
    let colorNumbres = [6031105,    8071424,    16761600,   2506255,    1115451,    3933979,
                        8589568,     5905408,   16755457,   3692314,    1706578,    9440319,
                        13041721,   11353600,   16631552,   5143079,    2886007,    5773381,
                        14820352,   14307584,   16116736,   6724916,    3611284,    8003998,
                        16734003,   16738816,   14350246,    7781184,    5055154,    9972412]

    
    var selectedColors = [Int]()
    var currentColor   = Int()
    weak var delegate: ColorPickerDelegate?
    
    var collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    lazy var backdropView: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        return bdView
    }()
   // var height = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
   // var menuHeight = CGFloat()
    let menuHeight = (UIScreen.main.bounds.height / 2) - 40
    var isPresenting = false
    let cellID = "cell"
    
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .custom
        transitioningDelegate = self
      //  menuHeight = height / 2 - 40
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        view.addSubview(backdropView)

     
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        backdropView.addGestureRecognizer(tapGesture)
        configureCollectionView()
    }
    
    func configureCollectionView() {
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundView = blurredEffectView
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)

        collectionView.register(ColorCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        
        collectionView.heightAnchor.constraint(equalToConstant: menuHeight).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 20).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.layoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        collectionView.layer.cornerRadius = collectionView.bounds.height * (1/10)
    }

//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        dismiss(animated: true, completion: nil)
//    }

// MARK: TARGET ACTION
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
   
}






// MARK: Extension

extension ColorPopupVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! ColorCollectionViewCell
        let hexString = String(colorNumbres[indexPath.item], radix: 16)
            cell.button.backgroundColor = UIColor(hex: hexString)
            cell.button.isUserInteractionEnabled = false
        return cell
    }
    override func viewDidAppear(_ animated: Bool) {
        if !selectedColors.isEmpty {
//
//            print("***********************************")
//
//            print(currentColor)
//
//
//            print("-----------------------------------")
            for color in selectedColors {
                if colorNumbres.contains(color) {
                    let indexOFThatColor = colorNumbres.firstIndex(of: color)
                    let cell = collectionView.cellForItem(at: IndexPath(item: indexOFThatColor!, section: 0)) as? ColorCollectionViewCell
                    cell?.button.isEnabled = false
                    cell?.isUserInteractionEnabled = false
                }
            }
            
            if currentColor != 0 {
                
                    if colorNumbres.contains(currentColor) {
                        let indexOFThatColor = colorNumbres.firstIndex(of: currentColor)
                        let cell = collectionView.cellForItem(at: IndexPath(item: indexOFThatColor!, section: 0)) as? ColorCollectionViewCell
                        cell?.button.isSelected = true
                        cell?.isUserInteractionEnabled = true
                    }
                }
            
        } else {
            print("Error: selcted array is empty")
        }
        }
    
    /**
     if colorNumbres.contains(selectedColors[index]) {
                        let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0)) as! ColorCollectionViewCell
                    }
     
     if !selectedColors.isEmpty {
         if colorNumbres[indexPath.item] == selectedColors[slctIndex] {
             cell.button.backgroundColor = UIColor(hex: hexString)
             cell.button.setImage(UIImage(named: "forbidden_icn"), for: .normal)
             cell.button.isUserInteractionEnabled = false
             slctIndex += 1
         } else {
             cell.button.backgroundColor = UIColor(hex: hexString)
             cell.button.isUserInteractionEnabled = false
         }
     } else {
         cell.button.backgroundColor = UIColor(hex: hexString)
         cell.button.isUserInteractionEnabled = false
     }
     */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 25, left: 20, bottom: 25, right: 20)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 45, height: 45)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cv = collectionView.cellForItem(at: indexPath) as? ColorCollectionViewCell
        delegate?.colorPicker(color: colorNumbres[indexPath.item], bgColor: (cv?.button.backgroundColor)!)
        self.dismiss(animated: true, completion: nil)
    }
   
}

extension ColorPopupVC: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        
        if isPresenting == true {
            containerView.addSubview(toVC.view)

            collectionView.frame.origin.y += menuHeight
            backdropView.alpha = 0
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.collectionView.frame.origin.y -= self.menuHeight
                self.backdropView.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.collectionView.frame.origin.y += self.menuHeight
                self.backdropView.alpha = 0
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}







    
/* let colorNumbres = [6031105,5905408,16736848,2506255,1115451,3933979,
8589568, 8071424, 16755457, 3692314, 1706578, 5574697,
11868672,11353600,16631552,5143079,2886007,6363260,
14820352,14307584,16116736,6724916,3611284,8003998,
16728085,16738816, 7304714,7781184,5055154, 9972412] */







