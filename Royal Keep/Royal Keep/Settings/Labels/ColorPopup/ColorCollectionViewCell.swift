//
//  ColorCellCollectionView.swift
//  Popup And CollectionView
//
//  Created by ArmanG on 4/21/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

class ColorCollectionViewCell: UICollectionViewCell {
    
    let button = RKButtonColor()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        NSLayoutConstraint.activate([
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            button.centerXAnchor.constraint(equalTo: centerXAnchor),
            button.widthAnchor.constraint(equalToConstant: 40),
            button.heightAnchor.constraint(equalToConstant: 40)])
        self.layoutSubviews()
        makeRounded(self, cornerRadius: 10)
    }
    private func makeRounded(_ view: UIView,cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = .none
        view.layer.borderWidth = 0
    }
    
    
}












//    override func awakeFromNib()
//    {
//        super.awakeFromNib()
//        button.translatesAutoresizingMaskIntoConstraints = false
//        self.contentView.addSubview(button)
//
//        button.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10.0).isActive = true
//        button.leadingAnchor.constraint(equalTo :self.contentView.leadingAnchor, constant: 10.0).isActive = true
//        button.trailingAnchor.constraint(equalTo :self.contentView.trailingAnchor, constant: -10.0).isActive = true
//        button.bottomAnchor.constraint(equalTo :self.contentView.bottomAnchor, constant: -10.0).isActive = true
//    }
