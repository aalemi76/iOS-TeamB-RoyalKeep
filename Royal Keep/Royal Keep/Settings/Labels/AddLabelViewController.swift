//
//  AddLabelViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/9/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol AddLabelViewControllerDelegate: class {
    func addNewLabel(_ controller: AddLabelViewController, didAddItem item: Label?, at indexPath: IndexPath?)
}
class AddLabelViewController: UIViewController, Storyboarded, UITextFieldDelegate {
    //MARK:- Variables
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    weak var delegate: AddLabelViewControllerDelegate?
    var titleView: TitleView!
    var checkmarkIcon: UIImageView!
    var xmarkIcon: UIImageView!
    let scrollView      = UIScrollView()
    let contentView     = UIView()
    let label_Title     = UILabel()
    let textField_label = RKLimitableTextField()
    let label_color     = UILabel()
    let imageView_Color = UIImageView()
    let button_Color    = UIButton()
    
    var labels: [Label]?
    var itemToEdit: Label?
    
    var currentColorsNumber: [Int]? = [Int]()
    var currentTitles      : [String]? = [String]()
    
    var isTilteTaken = Bool()
    var indexPath: IndexPath?
    var selectedColor = Int()
    var newLabel: Label!
    //  var colorsInt = [14350246, 16761600, 16734003, 13041721, 9440319, 5773381]
    var colors = [UIColor]()
    
    
    //MARK:- Actions
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 50, right: 0)
        }
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = scrollView.contentInset
    }
    @objc func checkmarkIconTouchUpInside() {
        if let itemToEdit = self.itemToEdit {
            updateLabel(itemToEdit: itemToEdit)
            self.dismiss(animated: true, completion: nil)
        } else {
            let checkResult = checkeAction(action: .addLabel)
            if checkResult != nil  {
                showAlertView(alert: .error, message: checkResult!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    
                }
                
            } else {
                showAlertView(alert: .ok, message: "برچسب با موفقیت ایچاد شد")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.dismiss(animated: true, completion: nil)
                }
                createNewLabel()
            }
            
            
        }
        
    }
    @objc func xmarkIconTouchUpInside() {
        let checkResult = checkeAction(action: .cancelOperation)
        if  checkResult != nil {
            let alert = UIAlertController(title: "اخطار", message: checkResult , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "خیر", style: .cancel ) {
                action in
                return
            }
            
            let okAction = UIAlertAction(title: "بلی", style: .destructive ) {
                action in
                self.dismiss(animated: true, completion: nil)
            }
            
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true)
            
        } else {
            
            dismiss(animated: true, completion: nil)
        }
    }
    @objc func textFieldEditingChanged(_ sender: RKLimitableTextField) {
        if itemToEdit != nil {
            let currentText = itemToEdit?.Title
            if sender.text?.trimmingCharacters(in: .whitespacesAndNewlines) != currentText {
                self.isEditing = true
            }
        }
        if let currentTitles = currentTitles {
            for title in currentTitles {
                if title == textField_label.text!.trimmingCharacters(in: .whitespacesAndNewlines) {
                    isTilteTaken = true
                    textField_label.mode = .error
                    return
                }
            }
            isTilteTaken = false
            textField_label.mode = .normal
        }
    }
    @objc func viewDidTap(tapGestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func btnAddColorTapped(_ sender: UIButton) {
        view.endEditing(true)
        let colorpckcrVC = ColorPopupVC()
        colorpckcrVC.delegate = self
        colorpckcrVC.modalPresentationStyle = .overFullScreen
        colorpckcrVC.modalTransitionStyle = .crossDissolve
        colorpckcrVC.selectedColors = currentColorsNumber!
        colorpckcrVC.currentColor = selectedColor
        present(colorpckcrVC, animated: true, completion: nil)
    }
    
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewDidTap(tapGestureRecognizer:)))
        view.addGestureRecognizer(tapGestureRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        configureTitleView()
        addScrollView()
        addContentView()
        configureTitleLabel()
        configureTextField()
        configure‌LabelColor()
        configureButtonColor()
        //  colors = createColorsArray(colorsInt: colorsInt)
        prepareNumbers_Tiltes()
        
        if let itemToEdit = self.itemToEdit {
            textField_label.text = itemToEdit.Title
            selectedColor = itemToEdit.Color
            let hexString = String(itemToEdit.Color, radix: 16)
            button_Color.backgroundColor = UIColor(hex: hexString)
            button_Color.setImage(UIImage(named: "edit_icn"), for: .normal)
        } else {
            print("No In Edit Mode OR Error")
        }
        
    }
    
    func deleteLabels() {
        let url = RKAPI.deleteLabelsURL
        AF.request(url, method: .delete, parameters: ["LabelID": "d14757a0-c731-11ea-8b73-a7bb1b4090b1"], encoding: URLEncoding(destination: .queryString), headers: RKAPI.username).responseString{ response in
            debugPrint(response)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidLayoutSubviews() {
        textField_label.layer.cornerRadius = textField_label.bounds.height * (1/5)
        button_Color.layer.cornerRadius = button_Color.bounds.height * (1/2)
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if let vc = presentedViewController as? ColorPopupVC {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    //MARK:- Web API
    func updateLabel(itemToEdit: Label) {
        guard let title = textField_label.text?.trimmingCharacters(in: .whitespacesAndNewlines) else  {return}
        let color = self.selectedColor
        let labelID = itemToEdit.LabelID
        var url: String
        var method: HTTPMethod
        var headers: HTTPHeaders
        let parameters: Parameters = ["Title": title, "Color": color, "LabelID": labelID]
        url = RKAPI.updateLabelsURL
        method = .put
        headers = RKAPI.headers
        AF.request(url, method: method, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                if let title = jsonObject["Title"].string, let labelID = jsonObject["LabelID"].string,
                    let color = jsonObject["Color"].int {
                    self.newLabel = Label(Title: title, Color: color, LabelID: labelID)
                    self.delegate?.addNewLabel(self, didAddItem: self.newLabel, at: self.indexPath)
                }
            case .failure(let error) :
                print(error.localizedDescription)
            }
        }
    }
    func createNewLabel() {
        let url = RKAPI.createLabelsURL
        guard let title = textField_label.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        AF.request(url, method: .post, parameters: ["Title": title, "Color": selectedColor], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).responseString { response in
            debugPrint(response)
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                if let title = jsonObject["Title"].string, let labelID = jsonObject["LabelID"].string,
                    let color = jsonObject["Color"].int {
                    self.newLabel = Label(Title: title, Color: color, LabelID: labelID)
                    self.delegate?.addNewLabel(self, didAddItem: self.newLabel, at: self.indexPath)
                }
            case .failure(let error) :
                print(error.localizedDescription)
            }
        }
    }
    //MARK:- View Layout
    func configureTitleView() {
        if let _ = itemToEdit {
            let title = "ویرایش برچسب"
            titleView = TitleView(title: title)
        } else {
            titleView = TitleView(title: "افزودن برچسب جدید")
        }
        titleView.addCheckmarkIcon()
        titleView.addXmarkIcon()
        checkmarkIcon = titleView.checkmarkIcon
        let checkTap = UITapGestureRecognizer(target: self, action: #selector(checkmarkIconTouchUpInside))
        checkmarkIcon.addGestureRecognizer(checkTap)
        xmarkIcon = titleView.xmarkIcon
        let xTap = UITapGestureRecognizer(target: self, action: #selector(xmarkIconTouchUpInside))
        xmarkIcon.addGestureRecognizer(xTap)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleView)
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: view.topAnchor),
            titleView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            titleView.heightAnchor.constraint(equalToConstant: 90)])
    }
    
    func addScrollView() {
        scrollView.layoutIfNeeded()
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    func addContentView() {
        contentView.sizeToFit()
        contentView.backgroundColor = GlobalSettings.shared().lightGray
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: view.bounds.height)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
    }
    func configureTitleLabel() {
        label_Title.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label_Title)
        label_Title.font = GlobalSettings.shared().boldSystemFont(size: 22)
        label_Title.textAlignment = .right
        label_Title.text = "عنوان"
        label_Title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 45).isActive = true
        label_Title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
    }
    func configureTextField() {
        textField_label.translatesAutoresizingMaskIntoConstraints = false
        textField_label.font = GlobalSettings.shared().mediumSystemFont()
        textField_label.textAlignment = .right
        textField_label.borderStyle = .none
        textField_label.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
        textField_label.delegate = self
        contentView.addSubview(textField_label)
        
        textField_label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 100).isActive = true
        textField_label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10).isActive = true
        textField_label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
        textField_label.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    func configure‌LabelColor() {
        label_color.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(label_color)
        label_color.font = GlobalSettings.shared().boldSystemFont(size: 22)
        label_color.textAlignment = .right
        label_color.text = "رنگ برچسب :"
        label_color.topAnchor.constraint(equalTo: textField_label.bottomAnchor, constant: 45).isActive = true
        label_color.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10).isActive = true
    }
    func configureButtonColor() {
        button_Color.translatesAutoresizingMaskIntoConstraints = false
        button_Color.setImage(UIImage(named: "addColor_icn"), for: .normal)
        button_Color.backgroundColor = #colorLiteral(red: 0.4253651798, green: 0.3433555944, blue: 0.7693718076, alpha: 1)
        contentView.addSubview(button_Color)
        button_Color.trailingAnchor.constraint(equalTo: label_color.leadingAnchor, constant: -10).isActive = true
        button_Color.widthAnchor.constraint(equalToConstant: 50).isActive = true
        button_Color.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button_Color.centerYAnchor.constraint(equalTo: label_color.centerYAnchor).isActive = true
        button_Color.addTarget(self, action: #selector(btnAddColorTapped), for: .touchUpInside)
    }
    //MARK:- Helper Methods
    func prepareNumbers_Tiltes() {
        if let labels = labels {
            for label in labels {
                currentColorsNumber?.append(label.Color)
                currentTitles?.append(label.Title)
            }
        } else {
            print("Error: Labels is empty")
        }
    }
    
    func createColorsArray(colorsInt: [Int]) -> [UIColor] {
        var colors = [UIColor]()
        for color in colorsInt {
            let hex = String(color, radix: 16)
            if let temp = UIColor.init(hex: hex) {
                colors.append(temp)
            }
        }
        return colors
    }
}

//MARK:- Control UserAction
extension AddLabelViewController {
    enum Action {
        case addLabel
        case cancelOperation
        case removeLabel
    }
    
    func checkeAction(action: Action) -> String? {
        let txtField_strLenght = self.textField_label.text!.trimmingCharacters(in: CharacterSet.whitespaces).count
        
        switch action {
        case .addLabel:
            if textField_label.text!.isEmpty {
                return "عنوان لیبل خالی است ."
            } else if txtField_strLenght == 0 {
                return "عنوان لیبل خالی است ."
            } else if button_Color.image(for: .normal) == UIImage(named: "addColor_icn") {
                return "لطفا رنگ برچسب را انتخاب کنید"
            } else if isTilteTaken == true {
                return "این عنوان قبلا انتخاب شده"
            } else {
                return nil
            }
            
        case .cancelOperation:
            if itemToEdit != nil {
                if isEditing == true {
                    return "اخطار اطلاعات خود را از دست میدهید"
                } else {
                    return nil
                }
            } else {
                if !textField_label.text!.isEmpty {
                    if txtField_strLenght == 0 {
                        return nil
                    }
                    return "اخطار اطلاعات خود را از دست میدهید"
                } else if button_Color.image(for: .normal) != UIImage(named: "addColor_icn") {
                    return "اخطار اطلاعات خود را از دست میدهید"
                }
                else {
                    return nil
                }
                
            }
        case .removeLabel:
            print("")
        }
        return String()
    }
    
    func showAlertView(alert: Alert, message: String) {
        var timer: Timer!
        let screenWidth  = UIScreen.main.bounds.width * (90/100)
        let screenHeight = UIScreen.main.bounds.height * (15/100)
        let xPosition    = (UIScreen.main.bounds.width - screenWidth) / 2
        
        
        switch alert {
        case .ok:
            let alertView = RKAlertView(alert: alert, message: message)
            alertView.frame.origin = CGPoint(x: xPosition, y: -screenHeight)
            
            self.view.addSubview(alertView)
            
            UIView.animate(withDuration: 0.8){
                alertView.frame.origin.y += screenHeight + 40
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false, block: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    alertView.frame.origin.y -= screenHeight + 40 }
                    , completion: { _ in alertView.removeFromSuperview() })
            })
            
        case .error:
            let alertView = RKAlertView(alert: alert, message: message)
            alertView.frame.origin = CGPoint(x: xPosition, y: -screenHeight)
            
            self.view.addSubview(alertView)
            
            UIView.animate(withDuration: 0.8){
                alertView.frame.origin.y += screenHeight + 40
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false, block: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    alertView.frame.origin.y -= screenHeight + 40 }
                    , completion: { _ in alertView.removeFromSuperview() })
            })
        }
        
    }
}


/** THIS IS TEMPORARY*/

extension AddLabelViewController: ColorPickerDelegate {
    func colorPicker(color number: Int, bgColor: UIColor) {
        print(number)
        print(bgColor)
        if itemToEdit != nil {
            self.isEditing = true
        }
        let hexString = String(number, radix: 16)
        button_Color.backgroundColor = UIColor(hex: hexString)
        button_Color.setImage(UIImage(named: "edit_icn"), for: .normal)
        self.selectedColor = number
        //  print(selectedColor)
    }
    
    
}
