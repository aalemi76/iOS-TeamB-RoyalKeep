//
//  Label.swift
//  PickerView
//
//  Created by ArmanG on 4/9/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import Foundation
struct Label: Codable {
    var Title: String
    var Color: Int
    var LabelID: String
}
