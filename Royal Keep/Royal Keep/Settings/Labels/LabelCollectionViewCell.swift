//
//  LabelCollectionViewCell.swift
//  Royal Keep
//
//  Created by Catalina on 7/10/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class LabelCollectionViewCell: UICollectionViewCell {
    static let reuseID = "LabelCollectionViewCell"
    let titleLabel: UILabel = {
        let temp = UILabel()
        temp.font = GlobalSettings.shared().boldSystemFont(size: 20)
        temp.textColor = .black
        temp.textAlignment = .right
        return temp
    }()
    var topView: UIView = UIView()
    let bottomView : UIView = UIView()
    var inEditingMode: Bool = false {
        willSet {
            selectImage.isHidden = !newValue
        }
    }
    var isCellSelected: Bool = false {
        willSet {
            newValue ? (selectImage.image = UIImage(systemName: "checkmark.circle")) :  (selectImage.image = UIImage(systemName: "circle"))
        }
    }
    let selectImage = UIImageView(image: UIImage(systemName: "circle"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    private func configure() {
        configureSelectImage()
        self.backgroundColor = .white
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        topView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        addSubview(topView)
        addSubview(bottomView)
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            topView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            topView.topAnchor.constraint(equalTo: self.topAnchor),
            topView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            topView.heightAnchor.constraint(equalToConstant: 20),
            bottomView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bottomView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bottomView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            bottomView.heightAnchor.constraint(equalToConstant: 20)])
        makeRounded(self, cornerRadius: 10)
    }
    private func configureSelectImage() {
        selectImage.isHidden = true
        selectImage.tintColor = GlobalSettings.shared().mainColor
        selectImage.translatesAutoresizingMaskIntoConstraints = false
        addSubview(selectImage)
        NSLayoutConstraint.activate([
            selectImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            selectImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30),
            selectImage.widthAnchor.constraint(equalToConstant: 40),
            selectImage.heightAnchor.constraint(equalToConstant: 40)])
    }
    private func makeRounded(_ view: UIView,cornerRadius: CGFloat) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = .none
        view.layer.borderWidth = 0
    }
}
