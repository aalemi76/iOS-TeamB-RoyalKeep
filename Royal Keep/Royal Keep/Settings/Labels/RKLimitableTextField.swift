//
//  RKLimitableTextField.swift
//  RKLimitableTextField
//
//  Created by ArmanG on 4/20/1399 AP.
//  Copyright © 1399 Pars Digital. All rights reserved.
//

import UIKit

class RKLimitableTextField: UITextField, UITextFieldDelegate {
    enum TextFieldMode {
        case normal
        case error
        case fadeError
    }
    
    let label_Alert = UILabel()
    var prevStatus = TextFieldMode.normal
    var mode: TextFieldMode = .normal {
        didSet {
            setTextFieldMode(mode: mode)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
       // set font and color here
        self.leftViewMode = .always
        self.layer.cornerRadius = self.bounds.height * (1/5)
        self.layer.borderWidth  = 2.0
        self.layer.borderColor = UIColor.black.cgColor
        self.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.borderStyle = .roundedRect
        configureLayout()
    }
    
 /**   func showError() {
        UIView.animate(withDuration: 2.0, animations: {
            self.layer.borderColor = UIColor.red.cgColor
            self.label_Alert.alpha = 1.0
             let color = CABasicAnimation(keyPath: "borderColor");
             color.fromValue = UIColor.black.cgColor;
             color.toValue = UIColor.red.cgColor;
            color.duration = 1.5;
            self.layer.add(color, forKey: "color and width");
        }) { vars in
            UIView.animate(withDuration: 2.0, animations: {
               self.layer.borderColor = UIColor.black.cgColor
               self.label_Alert.alpha = 0.0
               let color = CABasicAnimation(keyPath: "borderColor");
               color.fromValue = UIColor.red.cgColor;
               color.toValue = UIColor.black.cgColor;
               color.duration = 1.5;
               self.layer.add(color, forKey: "color and width");
            })
    
        }
        
       
    }  */
    
    
    func configureLayout() {
        label_Alert.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label_Alert)
        label_Alert.font = GlobalSettings.shared().lightSystemFont()
        label_Alert.textColor = .red
        label_Alert.alpha = 0.0
        label_Alert.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -3).isActive = true
        label_Alert.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 25).isActive = true
    }
    
    func setTextFieldMode(mode: TextFieldMode) {

        switch mode {
        case .normal:
            prevStatus = .normal
            UIView.animate(withDuration: 2.0, animations: {
                 self.leftView = nil
                 self.layer.borderColor = UIColor.black.cgColor
                 self.label_Alert.alpha = 0.0
                 let color = CABasicAnimation(keyPath: "borderColor");
//                 color.fromValue = UIColor.red.cgColor;
//                 color.toValue = UIColor.black.cgColor;
                 color.duration = 1.5;
                self.layer.add(color, forKey: "color and width");
            })
        case .fadeError:
            self.label_Alert.text = "عنوان نباید بیش از ۳۰ حرف باشد"
            if prevStatus == .normal {
                UIView.animate(withDuration: 2.0, animations: {
                        self.layer.borderColor = UIColor.red.cgColor
                        self.label_Alert.alpha = 1.0
                    self.layer.borderWidth = 3.0
                         let color = CABasicAnimation(keyPath: "borderColor");
                         color.fromValue = UIColor.black.cgColor;
                         color.toValue = UIColor.red.cgColor;
                        color.duration = 1.5;
                        self.layer.add(color, forKey: "color and width");
                    }) { vars in
                        UIView.animate(withDuration: 2.0, animations: {
                           self.layer.borderColor = UIColor.black.cgColor
                           self.label_Alert.alpha = 0.0
                           let color = CABasicAnimation(keyPath: "borderColor");
                           color.fromValue = UIColor.red.cgColor;
                           color.toValue = UIColor.black.cgColor;
                           color.duration = 1.5;
                           self.layer.add(color, forKey: "color and width");
                        })
                
                    }
            } else {
                UIView.animate(withDuration: 2.0, animations: {
                    self.label_Alert.text = "عنوان نباید بیش از ۳۰ حرف باشد"
                })
            }
            
        case .error :
            prevStatus = .error
            UIView.animate(withDuration: 2.0, animations: {
                self.leftView = UIImageView(image: UIImage(named: "warning_icn"))
                self.label_Alert.text = "این عنوان قبلا انتخاب شده "
                 self.label_Alert.alpha = 1.0
                self.layer.borderWidth = 3.0
                self.layer.borderColor = UIColor.red.cgColor
//                 let color = CABasicAnimation(keyPath: "borderColor");
//                 color.fromValue = UIColor.black.cgColor;
//                 color.toValue = UIColor.red.cgColor;
//                 color.duration = 1.5;
//                self.layer.add(color, forKey: "color and width");
            })
        }
        
    }
   @objc func textFieldDidChange(_ sender: UITextField) {
        if self.text!.count > 30 {
            self.deleteBackward()
            self.mode = .fadeError
        }
    }
       
    func setUp() {
        

       
      //  self.leftView = btnShowHide
        
        
            }
    
    override func leftViewRect(forBounds: CGRect) -> CGRect {
        var textrect = super.leftViewRect(forBounds: forBounds)
            textrect.origin.x += 10
        return textrect
    }
    

    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x - 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x - 10, y: bounds.origin.y, width: bounds.width, height: bounds.height)
    }
}
