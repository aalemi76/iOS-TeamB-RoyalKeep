//
//  ArchiveNotesVC.swift
//  Royal Keep
//
//  Created by Catalina on 7/20/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ArchiveNotesVC: UIViewController {
    //MARK:- Variables
    var coordinator: ArchiveCoordinator?
    var backIcon: UIImageView!
    let notesManager = NotesManager()
    var notesModels = [Note]()
    var mainNotes = [Note]()
    var mainIndex: Int?
    var labelsDictionary = [String: Label]()
    let emptyNoteLbl = UILabel()
    let tableView = UITableView(frame: .zero, style: .plain)
    let editView = EditView()
    var editButton, selectButton, archiveButton: UIButton!
    var isAllSelected: Bool = false {
        willSet {
            if newValue {
                selectButton.setTitle("غیر فعال کردن همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                selectButton.setTitle("انتخاب همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            }
        }
    }
    var inEditingMode: Bool = false {
        willSet {
            archiveButton.isHidden = !newValue
            selectButton.isHidden = !newValue
            newValue ? editButton.setTitle("انصراف", for: .normal) : editButton.setTitle("ویرایش", for: .normal)
        }
    }
    var selectedItems = [Int]()
    //MARK:- Actions
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        //navigationController?.popViewController(animated: true)
        coordinator?.start()
    }
    @objc func didTapEditButton(sender: UIButton) {
        selectedItems.removeAll()
        isAllSelected = false
        inEditingMode = !inEditingMode
        tableView.reloadData()
    }
    @objc func didTapSelectButton(sender: UIButton) {
           guard notesModels.count != 0 else {return}
           if selectedItems.count < notesModels.count {
               selectedItems = Array(0..<notesModels.count)
               isAllSelected = true
               tableView.reloadData()
           } else {
               selectedItems.removeAll()
               isAllSelected = false
               tableView.reloadData()
           }
       }
       @objc func didTapArchiveButton(sender: UIButton) {
           guard selectedItems.count != 0 else {return}
           let ids = getNoteID()
           let queue = DispatchQueue.global()
           queue.async {
               AF.request(RKAPI.notesSetCategory, method: .post, parameters: ["ItemIDs": ids, "Category": 0], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                   switch response.result {
                   case .success:
                       DispatchQueue.main.async {
                           self.presentAlert()
                       }
                   case .failure(let error):
                       print(error.localizedDescription)
                   }
               }
           }
       }
    func presentAlert() {
        let alert = UIAlertController(title: "حذف از آرشیو", message: "یادداشت ها با موفقیت از آرشیو حذف گردید.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            self.inEditingMode = false
            self.request()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GlobalSettings.shared().lightGray
        configureEditView()
        configureTableView()
        configureEmptyNoteLabel()
        getLabels()
        request()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: false)
         navigationItem.title = "آرشیو یادداشت ها"
        navigationController.menuIcon.isHidden = true
        navigationController.addBackIcon()
        backIcon = navigationController.backIcon
        let backTap = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon(tapGestureRecognizer:)))
        backIcon.addGestureRecognizer(backTap)
        navigationController.backIcon.isHidden = false
    }
    func handleNotification() {
        guard let mainIndex = mainIndex else {return}
        let note = mainNotes[mainIndex]
        let index = notesModels.firstIndex(of: note)
        let indexPath = IndexPath(row: index!, section: 0)
        self.mainIndex = nil
        let label = labelsDictionary[note.labelId ?? ""]
        let labels = Array(labelsDictionary.values)
        coordinator?.toNoteDetailsView(self, noteId: note.noteId!, indexPath: indexPath, noteLabel: label, labels: labels, mainIndex: mainIndex)
    }
    //MARK:- Web API
    func getLabels() {
        let url = RKAPI.labelsURL
        AF.request(url, method: .get, headers: RKAPI.username).responseString { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                let n = jsonObject.count
                for i in 0..<n {
                    if let title = jsonObject[i]["Title"].string, let color = jsonObject[i]["Color"].int, let labelId = jsonObject[i]["LabelID"].string {
                        let label = Label(Title: title, Color: color, LabelID: labelId)
                        self.labelsDictionary[label.LabelID] = label
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    func request() {
           AF.request(notesManager.url , headers: HTTPHeaders(["username": User.globalUsername])).responseJSON { (data) in
               do {
                self.mainNotes = try self.notesManager.setFinalArrayOfModels(data: data.data!)
                   self.mainNotes = self.mainNotes.sorted(by: {$0.creationDate! < $1.creationDate!})
                   ReminderCollection.setReminder(noteList: self.mainNotes)
                ReminderCollection.update(noteList: self.mainNotes)
                   self.notesModels = self.mainNotes.filter { (note: Note) -> Bool in
                       return note.category! == 2
                   }
                   DispatchQueue.main.async {
                       self.tableView.reloadData()
                       self.configureEmptyNoteMessage(isHidden: !self.notesModels.isEmpty)
                       self.handleNotification()
                   }
               }catch Error.notesIsEmpty {
                   self.configureEmptyNoteMessage(isHidden: false)
               }
               catch {
                   print(error)
               }
           }
       }
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: K.notesCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: editView.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func configureEmptyNoteLabel() {
        self.emptyNoteLbl.text = "موردی برای نمایش یافت نشد"
        self.emptyNoteLbl.font = GlobalSettings.shared().boldSystemFont(size: 15)
        self.emptyNoteLbl.textColor = GlobalSettings.shared().darkGray
        self.emptyNoteLbl.textAlignment = .center
        self.emptyNoteLbl.sizeToFit()
        emptyNoteLbl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(emptyNoteLbl)
        NSLayoutConstraint.activate([
            emptyNoteLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            emptyNoteLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
    }
    func configureEmptyNoteMessage(isHidden: Bool) {
        self.tableView.isHidden = !isHidden
        self.emptyNoteLbl.isHidden = isHidden
    }
    //MARK:- Edit View
    func configureEditView() {
        editView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(editView)
        NSLayoutConstraint.activate([
            editView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            editView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            editView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            editView.heightAnchor.constraint(equalToConstant: 40)])
        editButton = editView.editButton
        editButton.addTarget(self, action: #selector(didTapEditButton(sender:)), for: .touchUpInside)
        selectButton = editView.selectButton
        selectButton.isHidden = true
        selectButton.addTarget(self, action: #selector(didTapSelectButton(sender:)), for: .touchUpInside)
        editView.addArchiveButton()
        archiveButton = editView.archiveButton
        archiveButton.setImage(UIImage(systemName: "archivebox.fill"), for: .normal)
        archiveButton.setTitle("حذف از آرشیو", for: .normal)
        archiveButton.isHidden = true
        archiveButton.addTarget(self, action: #selector(didTapArchiveButton(sender:)), for: .touchUpInside)
    }
    func getNoteID() -> [String] {
        var ids = [String]()
        for i in selectedItems {
            ids.append(notesModels[i].noteId!)
        }
        return ids
    }
}
