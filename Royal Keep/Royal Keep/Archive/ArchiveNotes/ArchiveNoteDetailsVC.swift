//
//  ArchiveNoteDetailsVC.swift
//  Royal Keep
//
//  Created by Catalina on 7/21/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol ArchiveNoteDetailsVCDelegate: class {
    func backIconTouchUpInisde()
}
class ArchiveNoteDetailsVC: UIViewController {
    //MARK:- Variables
    var coordinator: ArchiveCoordinator?
    weak var delegate: ArchiveNoteDetailsVCDelegate?
    var backIcon: UIImageView!
    let detailView = AddDetailView(frame: .zero)
    var scrollView = UIScrollView()
    var contentView: UIView!
    var dateLabel = UILabel()
    let reminderLabel = UILabel()
    let reminderButton = UIButton(type: .system)
    var titleText = UITextView()
    var labelButton = RKButtonLabel()
    var descriptionText = UITextView()
    var note: Note!
    var noteId: String!
    var noteLabel: Label?
    var indexPath: IndexPath!
    var mainIndex: Int!
    var labels = [Label]()
    var date: Date!
    var dueDate: Date? {
        willSet {
            if let newDate = newValue {
                reminderButton.setTitle(persianLongDF.string(from: newDate), for: .normal)
            } else {
                reminderButton.setTitle("بدون یادآور", for: .normal)
            }
        }
    }
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let persianDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .short
        formatter.dateFormat = "dd - MMMM - yyyy"
        return formatter
    }()
    let persianLongDF: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .long
        formatter.dateFormat = "dd - MMMM - yyyy - HH:mm"
        return formatter
    }()
    let archiveButton = UIButton(type: .system)
    //MARK:- Actions
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.backIconTouchUpInisde()
        navigationController?.popViewController(animated: true)
    }
    @objc func didTapArchive(sender: UIButton) {
        let category: Int
        note.category == 2 ? (category = 0) : (category = 2)
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.notesSetCategory, method: .post, parameters: ["ItemIDs": [self.note.noteId!], "Category": category], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                        self.note.category = 0
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    func presentAlert() {
        let alert = UIAlertController(title: "حذف از آرشیو", message: "یادداشت با موفقیت از لیست آرشیو ها حذف گردید.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            guard let tap = self.backIcon.gestureRecognizers?[0] as? UITapGestureRecognizer else {return}
            self.didTapBackIcon(tapGestureRecognizer: tap)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GlobalSettings.shared().lightGray
        getNote()
        configureDetailView()
        configureScrollView()
        configureContentView()
        configureReminderComponent()
        configureLabelButton()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard note.reminderDate != nil else {return}
        ReminderCollection.notes.updateValue(note, forKey: mainIndex)
        ReminderCollection.saveNotes()
    }
    //MARK:- Web API
    func getNote() {
        guard let noteId = noteId else {return}
        if let note = ReminderCollection.getNote(indexPath: mainIndex) {
            self.dueDate = note.reminderDate
        }
        let url = RKAPI.noteDetailsURL
        AF.request(url, method: .get, parameters: ["NoteID": noteId], encoding: URLEncoding(destination: .queryString), headers: RKAPI.username).responseJSON { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                if let title = jsonObject["Title"].string, let noteId = jsonObject["NoteID"].string, let creationDate = jsonObject["CreationDate"].string {
                    let description = jsonObject["Description"].string
                    let labelId = jsonObject["LabelID"].string
                    let category = jsonObject["Category"].int
                    self.note = Note(title: title, description: description, creationDate: creationDate, labelId: labelId, noteId: noteId, category: category)
                    self.note.reminderDate = self.dueDate
                    DispatchQueue.main.async {
                        self.date = self.dateFormatter.date(from: self.note.creationDate!)
                        self.dateLabel.text = self.persianDateFormatter.string(from: self.date)
                        self.titleText.text = self.note.title
                        self.descriptionText.text = self.note.noteDescription
                        self.configureArchiveButton()
                        if self.descriptionText.text == "" {
                            self.descriptionText.text = "-"
                        }
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.title = "جزيیات یادداشت"
        navigationController.menuIcon.isHidden = true
        navigationController.addBackIcon()
        backIcon = navigationController.backIcon
        let backTap = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon(tapGestureRecognizer:)))
        backIcon.addGestureRecognizer(backTap)
        navigationController.backIcon.isHidden = false
    }
    func configureDetailView() {
        detailView.topDistance = 50
        contentView = detailView.contentView
        scrollView = detailView.scrollView
        dateLabel = detailView.dateLabel
        titleText = detailView.titleText
        titleText.isUserInteractionEnabled = false
        labelButton = detailView.labelButton
        labelButton.isUserInteractionEnabled = false
        descriptionText = detailView.descriptionText
        descriptionText.isUserInteractionEnabled = false
    }
    func configureScrollView() {
        scrollView.layoutIfNeeded()
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func configureContentView() {
        contentView.sizeToFit()
        contentView.backgroundColor = GlobalSettings.shared().lightGray
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualTo: view.heightAnchor)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
    }
    func configureArchiveButton() {
        archiveButton.tintColor = GlobalSettings.shared().mainColor
        if note.category == 2 {
            archiveButton.setBackgroundImage(UIImage(systemName: "archivebox.fill"), for: .normal)
        } else {
            archiveButton.setBackgroundImage(UIImage(systemName: "archivebox"), for: .normal)
        }
        archiveButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(archiveButton)
        NSLayoutConstraint.activate([
            archiveButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            archiveButton.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            archiveButton.widthAnchor.constraint(equalToConstant: 40),
            archiveButton.heightAnchor.constraint(equalToConstant: 40)])
        archiveButton.addTarget(self, action: #selector(didTapArchive(sender:)), for: .touchUpInside)
    }
    func configureReminderComponent() {
        reminderLabel.text = "تنظیمات یادآور:"
        reminderLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        reminderLabel.textColor = GlobalSettings.shared().darkGray
        reminderLabel.textAlignment = .right
        reminderLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reminderLabel)
        NSLayoutConstraint.activate([
            reminderLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10),
            reminderLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)])
        reminderButton.titleLabel?.font = GlobalSettings.shared().mediumSystemFont(size: 17)
        reminderButton.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        reminderButton.tintColor = GlobalSettings.shared().mainColor
        reminderButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reminderButton)
        if let dueDate = dueDate {
            reminderButton.setTitle(persianLongDF.string(from: dueDate), for: .normal)
        } else {
            reminderButton.setTitle("بدون یادآور", for: .normal)
        }
        NSLayoutConstraint.activate([
            reminderButton.trailingAnchor.constraint(equalTo: reminderLabel.leadingAnchor, constant: -10),
            reminderButton.centerYAnchor.constraint(equalTo: reminderLabel.centerYAnchor)])
    }
    func configureLabelButton() {
        guard let label = noteLabel else {
            labelButton.setTitle("بدون برچسب", for: .normal)
            labelButton.isHidden = true
            return
        }
        let hex = String(label.Color, radix: 16)
        labelButton.colorView.backgroundColor = UIColor.init(hex: hex)
        labelButton.setTitle(label.Title, for: .normal)
        labelButton.isHidden = false
    }
}
