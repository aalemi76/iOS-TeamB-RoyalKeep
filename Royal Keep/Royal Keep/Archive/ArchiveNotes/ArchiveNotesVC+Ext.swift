//
//  ArchiveNotesVC+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 7/21/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
extension ArchiveNotesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notesModels.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.notesCellIdentifier, for: indexPath) as! NoteTableViewCell
        let note = notesModels[indexPath.row]
        cell.checkBox.isHidden = true
        cell.titleLabel.text = note.title
        cell.descriptionText.text = note.noteDescription
        if let labelId = note.labelId, let label = labelsDictionary[labelId] {
            let hex = String(label.Color, radix: 16)
            cell.label.backgroundColor = UIColor.init(hex: hex)
        } else {
            cell.label.backgroundColor = GlobalSettings.shared().lightGray
        }
        if let reminderDate = note.reminderDate {
            cell.hasReminder = true
        } else {
            cell.hasReminder = false
        }
        cell.inEditingMode = inEditingMode
        cell.isCellSelected = selectedItems.contains(indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let note = notesModels[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! NoteTableViewCell
        if inEditingMode {
            if cell.isCellSelected {
                selectedItems = selectedItems.filter { $0 != indexPath.row }
                print(selectedItems)
                isAllSelected = false
            } else {
                selectedItems.append(indexPath.row)
                selectedItems.count == notesModels.count ?  isAllSelected = true : ()
            }
            cell.isCellSelected = !cell.isCellSelected
        } else {
            toNoteDetails(note: note, indexPath: indexPath)
        }
    }
    func toNoteDetails(note: Note, indexPath: IndexPath) {
        let index = mainNotes.firstIndex(of: note)
        let label = labelsDictionary[note.labelId ?? ""]
        let labels = Array(labelsDictionary.values)
        coordinator?.toNoteDetailsView(self, noteId: note.noteId!, indexPath: indexPath, noteLabel: label, labels: labels, mainIndex: index!)
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        guard !inEditingMode else {return nil}
        let note = notesModels[indexPath.row]
        let archive = UIAction(title: "حذف از آرشیو", image: UIImage(systemName: "archivebox")) { [weak self] _ in
            self?.archive(note: note)
        }
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [archive])
        }
    }
    func archive(note: Note) {
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.notesSetCategory, method: .post, parameters: ["ItemIDs": [note.noteId!], "Category": 0], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
extension ArchiveNotesVC: ArchiveNoteDetailsVCDelegate {
    func backIconTouchUpInisde() {
        request()
    }
}
