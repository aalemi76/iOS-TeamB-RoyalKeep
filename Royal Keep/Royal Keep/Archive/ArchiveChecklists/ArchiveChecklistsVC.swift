//
//  ArchiveChecklistsVC.swift
//  Royal Keep
//
//  Created by Catalina on 7/20/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ArchiveChecklistsVC: UIViewController {
    //MARK:- Variables
    var coordinator: ArchiveCoordinator?
    var backIcon: UIImageView!
    let checklistManager = CheckListManager()
    var checklists = [Checklist]()
    var mainChecklists = [Checklist]()
    var mainIndex: Int?
    var labelsDictionary = [String: Label]()
    let emptyChecklistLbl = UILabel()
    let tableView = UITableView(frame: .zero, style: .plain)
    let editView = EditView()
    var editButton, selectButton, archiveButton: UIButton!
    var isAllSelected: Bool = false {
        willSet {
            if newValue {
                selectButton.setTitle("غیر فعال کردن همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                selectButton.setTitle("انتخاب همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            }
        }
    }
    var inEditingMode: Bool = false {
        willSet {
            archiveButton.isHidden = !newValue
            selectButton.isHidden = !newValue
            newValue ? editButton.setTitle("انصراف", for: .normal) : editButton.setTitle("ویرایش", for: .normal)
        }
    }
    var selectedItems = [Int]()
    //MARK:- Actions
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        //navigationController?.popViewController(animated: true)
        coordinator?.start()
    }
    @objc func didTapEditButton(sender: UIButton) {
        selectedItems.removeAll()
        isAllSelected = false
        inEditingMode = !inEditingMode
        tableView.reloadData()
    }
    @objc func didTapSelectButton(sender: UIButton) {
        guard checklists.count != 0 else {return}
        if selectedItems.count < checklists.count {
            selectedItems = Array(0..<checklists.count)
            isAllSelected = true
            tableView.reloadData()
        } else {
            selectedItems.removeAll()
            isAllSelected = false
            tableView.reloadData()
        }
    }
    @objc func didTapArchiveButton(sender: UIButton) {
        guard selectedItems.count != 0 else {return}
        let ids = getChecklistID()
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.checklistsSetCategory, method: .post, parameters: ["ItemIDs": ids, "Category": 0], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    func presentAlert() {
        let alert = UIAlertController(title: "حذف از آرشیو", message: "چک لیست ها با موفقیت از آرشیو حذف گردید.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            self.inEditingMode = false
            self.request()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GlobalSettings.shared().lightGray
        configureEditView()
        configureTableView()
        configureEmptyChecklistLabel()
        getLabels()
        request()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: false)
         navigationItem.title = "آرشیو چک لیست ها"
        navigationController.menuIcon.isHidden = true
        navigationController.addBackIcon()
        backIcon = navigationController.backIcon
        let backTap = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon(tapGestureRecognizer:)))
        backIcon.addGestureRecognizer(backTap)
        navigationController.backIcon.isHidden = false
    }
    func handleNotification() {
        guard let mainIndex = mainIndex else {return}
        let checklist = mainChecklists[mainIndex]
        let index = checklists.firstIndex(of: checklist)
        let indexPath = IndexPath(row: index!, section: 0)
        self.mainIndex = nil
        let label = labelsDictionary[checklist.LabelID ?? ""]
        let labels = Array(labelsDictionary.values)
        coordinator?.toChecklistDetails(self, checklistID: checklist.CheckListID!, indexPath: indexPath, checklistLabel: label, labels: labels, mainIndex: mainIndex)
    }
    //MARK:- Web API
    func getLabels() {
        let url = RKAPI.labelsURL
        AF.request(url, method: .get, headers: RKAPI.username).responseString { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                let n = jsonObject.count
                for i in 0..<n {
                    if let title = jsonObject[i]["Title"].string, let color = jsonObject[i]["Color"].int, let labelId = jsonObject[i]["LabelID"].string {
                        let label = Label(Title: title, Color: color, LabelID: labelId)
                        self.labelsDictionary[label.LabelID] = label
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    func request() {
        AF.request(checklistManager.url , headers: HTTPHeaders(["username": User.globalUsername])).responseJSON { (data) in
            do {
                self.mainChecklists = try self.checklistManager.setFinalArrayOfModels(data: data.data!)
                self.mainChecklists = self.mainChecklists.sorted(by: {$0.CreationDate! < $1.CreationDate!})
                ReminderCollection.setReminder(checklists: self.mainChecklists)
                ReminderCollection.update(checklists: self.mainChecklists)
                self.checklists = self.mainChecklists.filter { (checklist: Checklist) -> Bool in
                    return checklist.Category! == 2
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.configureEmptyNoteMessage(isHidden: !self.checklists.isEmpty)
                    self.handleNotification()
                }
            }catch Error.checkListIsEmpty {
                self.configureEmptyNoteMessage(isHidden: false)
            }
            catch {
                print(error)
            }
        }
    }
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: K.notesCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: editView.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    func configureEmptyChecklistLabel() {
        self.emptyChecklistLbl.text = "موردی برای نمایش یافت نشد"
        self.emptyChecklistLbl.font = GlobalSettings.shared().boldSystemFont(size: 15)
        self.emptyChecklistLbl.textColor = GlobalSettings.shared().darkGray
        self.emptyChecklistLbl.textAlignment = .center
        self.emptyChecklistLbl.sizeToFit()
        emptyChecklistLbl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(emptyChecklistLbl)
        NSLayoutConstraint.activate([
            emptyChecklistLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            emptyChecklistLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
    }
    func configureEmptyNoteMessage(isHidden: Bool) {
        self.tableView.isHidden = !isHidden
        self.emptyChecklistLbl.isHidden = isHidden
    }
    //MARK:- Edit View
    func configureEditView() {
        editView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(editView)
        NSLayoutConstraint.activate([
            editView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            editView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            editView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            editView.heightAnchor.constraint(equalToConstant: 40)])
        editButton = editView.editButton
        editButton.addTarget(self, action: #selector(didTapEditButton(sender:)), for: .touchUpInside)
        selectButton = editView.selectButton
        selectButton.isHidden = true
        selectButton.addTarget(self, action: #selector(didTapSelectButton(sender:)), for: .touchUpInside)
        editView.addArchiveButton()
        archiveButton = editView.archiveButton
        archiveButton.setImage(UIImage(systemName: "archivebox.fill"), for: .normal)
        archiveButton.setTitle("حذف از آرشیو", for: .normal)
        archiveButton.isHidden = true
        archiveButton.addTarget(self, action: #selector(didTapArchiveButton(sender:)), for: .touchUpInside)
    }
    func getChecklistID() -> [String] {
        var ids = [String]()
        for i in selectedItems {
            ids.append(checklists[i].CheckListID!)
        }
        return ids
    }
}
