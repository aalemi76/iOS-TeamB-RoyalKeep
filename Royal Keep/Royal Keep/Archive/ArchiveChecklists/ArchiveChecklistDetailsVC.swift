//
//  ArchiveChecklistDetailsVC.swift
//  Royal Keep
//
//  Created by Catalina on 7/21/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire
import SwiftyJSON
protocol ArchiveChecklistDetailsVCDelegate: class {
    func backIconTouchUpInisde()
}
class ArchiveChecklistDetailsVC: UIViewController {
    //MARK:- Variables
    var coordinator: ArchiveCoordinator?
    weak var delegate: ArchiveChecklistDetailsVCDelegate?
    var manager = CheckListManager()
    var backIcon: UIImageView!
    let pageControl = UIPageControl()
    let mainScrollView = UIScrollView()
    var detailView = AddDetailView()
    var detailScrollView: UIScrollView!
    var contentView: UIView!
    var dateLabel = UILabel()
    let reminderLabel = UILabel()
    let reminderButton = UIButton(type: .system)
    var titleText = UITextView()
    var labelButton = RKButtonLabel()
    var descriptionText = UITextView()
    var tableView: UITableView!
    var tableContentView: UIView!
    var checklistID: String!
    var checklist: Checklist!
    var indexPath: IndexPath!
    var mainIndex: Int!
    var labels = [Label]()
    var checkListLabel: Label? {
        didSet {
            configureLabelButton()
        }
    }
    var date: Date!
    var checklistItems = [ChecklistItem]()
    var dueDate: Date? {
        willSet {
            if let newDate = newValue {
                reminderButton.setTitle(persianLongDF.string(from: newDate), for: .normal)
            } else {
                reminderButton.setTitle("بدون یادآور", for: .normal)
            }
        }
    }
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let persianDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .short
        formatter.dateFormat = "dd - MMMM - yyyy"
        return formatter
    }()
    let persianLongDF: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .long
        formatter.dateFormat = "dd - MMMM - yyyy - HH:mm"
        return formatter
    }()
    let emptyItem = UILabel()
    let completeAll = UIButton(type: .system)
    let archiveButton = UIButton(type: .system)
    //MARK:- Actions
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        delegate?.backIconTouchUpInisde()
        navigationController?.popViewController(animated: true)
    }
    @objc func didTapArchive(sender: UIButton) {
        let category: Int
            checklist.Category == 2 ? (category = 0) : (category = 2)
            let queue = DispatchQueue.global()
            queue.async {
                AF.request(RKAPI.checklistsSetCategory, method: .post, parameters: ["ItemIDs": [self.checklist.CheckListID!], "Category": category], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                    switch response.result {
                    case .success:
                        DispatchQueue.main.async {
                            self.presentAlert()
                            self.checklist.Category = 0
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    func presentAlert() {
        let alert = UIAlertController(title: "حذف از آرشیو", message: "چک لیست با موفقیت از لیست آرشیو ها حذف گردید.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            guard let tap = self.backIcon.gestureRecognizers?[0] as? UITapGestureRecognizer else {return}
            self.didTapBackIcon(tapGestureRecognizer: tap)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = GlobalSettings.shared().lightGray
        getChecklist()
        configurePageControll()
        configureMainScrollView()
        configureDetailView()
        configureReminderComponent()
        configureLabelButton()
        configureTableView()
        configureEmptyItemMessage()
        view.bringSubviewToFront(pageControl)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard checklist.reminderDate != nil else {return}
        ReminderCollection.checklists.updateValue(checklist, forKey: mainIndex)
        ReminderCollection.saveChecklists()
    }
    //MARK:- Web API
    func getChecklist() {
        guard let checklistID = checklistID else {return}
        if let checklist = ReminderCollection.getChecklist(indexPath: mainIndex) {
            dueDate = checklist.reminderDate
        }
        var activityView: RKActivityIndicatorView? = RKActivityIndicatorView()
        cofigureActivityIndicator(activityView: activityView!)
        activityView?.showSpinner()
        let url = RKAPI.checklistDetailsURL
        AF.request(url, method: .get, parameters: ["CheckListID": checklistID], encoding: URLEncoding(destination: .queryString), headers: RKAPI.username).responseJSON { response in
            switch response.result {
            case .success:
                activityView?.showCheckmark()
                let jsonObject = JSON(response.data ?? Data())
                if let checklist = CheckListManager.makeChecklist(jsonObject: jsonObject) {
                    self.checklist = checklist
                    self.checklist.reminderDate = self.dueDate
                    self.configureChecklistDetail()
                    self.configureButtonTitle(isCompleted: !checklist.isChecklistCompleted())
                    self.toggleEmptyItem(isHidden: checklist.hasItem())
                }
                DispatchQueue.main.async {
                    self.configureArchiveButton()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    activityView?.dismiss()
                    activityView?.removeFromSuperview()
                    activityView = nil
                }
            case .failure(let error):
                print("Error encountered while geting checklist: \(error.localizedDescription)")
            }
        }
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.title = "جزيیات چک لیست"
        navigationController.menuIcon.isHidden = true
        navigationController.addBackIcon()
        backIcon = navigationController.backIcon
        let backTap = UITapGestureRecognizer(target: self, action: #selector(didTapBackIcon(tapGestureRecognizer:)))
        backIcon.addGestureRecognizer(backTap)
        navigationController.backIcon.isHidden = false
    }
    func configurePageControll() {
        pageControl.pageIndicatorTintColor = .darkGray
        pageControl.currentPageIndicatorTintColor = GlobalSettings.shared().mainColor
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageControl)
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
        pageControl.numberOfPages = 2
        pageControl.currentPage = 0
    }
    func configureMainScrollView() {
        mainScrollView.delegate = self
        mainScrollView.backgroundColor = .systemGray4
        mainScrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainScrollView)
        NSLayoutConstraint.activate([
            mainScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        mainScrollView.contentSize = CGSize(width: view.bounds.width * 2, height: view.bounds.height - 90)
        mainScrollView.isPagingEnabled = true
        mainScrollView.showsHorizontalScrollIndicator = false
    }
    func configureDetailView() {
        detailView = AddDetailView(frame: CGRect(x: 15, y: 15, width: view.bounds.width - 30, height: view.bounds.height - 155))
        detailView.topDistance = 50
        mainScrollView.addSubview(detailView)
        makeRound(detailView)
        detailScrollView = detailView.scrollView
        contentView = detailView.contentView
        dateLabel = detailView.dateLabel
        titleText = detailView.titleText
        titleText.isUserInteractionEnabled = false
        labelButton = detailView.labelButton
        labelButton.isUserInteractionEnabled = false
        descriptionText = detailView.descriptionText
        descriptionText.isUserInteractionEnabled = false
    }
   func configureArchiveButton() {
        archiveButton.tintColor = GlobalSettings.shared().mainColor
        if checklist.Category == 2 {
            archiveButton.setBackgroundImage(UIImage(systemName: "archivebox.fill"), for: .normal)
        } else {
            archiveButton.setBackgroundImage(UIImage(systemName: "archivebox"), for: .normal)
        }
        archiveButton.translatesAutoresizingMaskIntoConstraints = false
        detailView.addSubview(archiveButton)
        NSLayoutConstraint.activate([
            archiveButton.leadingAnchor.constraint(equalTo: detailView.leadingAnchor, constant: 15),
            archiveButton.centerYAnchor.constraint(equalTo: dateLabel.centerYAnchor),
            archiveButton.widthAnchor.constraint(equalToConstant: 40),
            archiveButton.heightAnchor.constraint(equalToConstant: 40)])
        archiveButton.addTarget(self, action: #selector(didTapArchive(sender:)), for: .touchUpInside)
    }
    func configureReminderComponent() {
        reminderLabel.text = "تنظیمات یادآور:"
        reminderLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        reminderLabel.textColor = GlobalSettings.shared().darkGray
        reminderLabel.textAlignment = .right
        reminderLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reminderLabel)
        NSLayoutConstraint.activate([
            reminderLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 10),
            reminderLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)])
        reminderButton.titleLabel?.font = GlobalSettings.shared().mediumSystemFont(size: 17)
        reminderButton.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        reminderButton.tintColor = GlobalSettings.shared().mainColor
        reminderButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(reminderButton)
        if let dueDate = dueDate {
            reminderButton.setTitle(persianLongDF.string(from: dueDate), for: .normal)
        } else {
            reminderButton.setTitle("بدون یادآور", for: .normal)
        }
        NSLayoutConstraint.activate([
            reminderButton.trailingAnchor.constraint(equalTo: reminderLabel.leadingAnchor, constant: -10),
            reminderButton.centerYAnchor.constraint(equalTo: reminderLabel.centerYAnchor)])
    }
    func configureLabelButton() {
        guard let label = checkListLabel else {
            labelButton.setTitle("بدون برچسب", for: .normal)
            labelButton.isHidden = true
            return
        }
        let hex = String(label.Color, radix: 16)
        labelButton.colorView.backgroundColor = UIColor.init(hex: hex)
        labelButton.setTitle(label.Title, for: .normal)
        labelButton.isHidden = false
    }
    func configureTableView() {
        tableContentView = UIView(frame: CGRect(x: view.bounds.width + 15, y: 15, width: view.bounds.width - 30, height: view.bounds.height - 155))
        tableContentView.backgroundColor = GlobalSettings.shared().lightGray
        tableView = UITableView(frame: .zero, style: .grouped)
        mainScrollView.addSubview(tableContentView)
        makeRound(tableContentView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableContentView.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: tableContentView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: tableContentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: tableContentView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: tableContentView.bottomAnchor)])
        tableView.register(ChecklistItemTableViewCell.self, forCellReuseIdentifier: ChecklistItemTableViewCell.reuseID)
        tableView.delegate = self
        tableView.dataSource = self
    }
    func configureEmptyItemMessage() {
        self.emptyItem.text = "موردی برای نمایش یافت نشد"
        self.emptyItem.font = GlobalSettings.shared().boldSystemFont(size: 15)
        self.emptyItem.textColor = GlobalSettings.shared().darkGray
        self.emptyItem.textAlignment = .center
        self.emptyItem.sizeToFit()
        emptyItem.translatesAutoresizingMaskIntoConstraints = false
        tableContentView.addSubview(emptyItem)
        NSLayoutConstraint.activate([
            emptyItem.centerYAnchor.constraint(equalTo: tableContentView.centerYAnchor),
            emptyItem.centerXAnchor.constraint(equalTo: tableContentView.centerXAnchor)])
    }
    func toggleEmptyItem(isHidden: Bool) {
        tableView.isHidden = !isHidden
        emptyItem.isHidden = isHidden
    }
    func configureButtonTitle(isCompleted: Bool) {
        if isCompleted {
            completeAll.setTitle("کامل کردن همه موارد", for: .normal)
        } else {
            completeAll.setTitle("ناتمام کردن همه موارد", for: .normal)
        }
    }
    func cofigureActivityIndicator(activityView: UIView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityView.widthAnchor.constraint(equalToConstant: 80),
            activityView.heightAnchor.constraint(equalToConstant: 80)])
    }
    func configureChecklistDetail() {
        date = dateFormatter.date(from: self.checklist.CreationDate!)
        dateLabel.text = persianDateFormatter.string(from: date)
        titleText.text = checklist.Title
        descriptionText.text = checklist.Description
        checklistItems = checklist.CheckListItems ?? [ChecklistItem]()
        tableView.reloadData()
        if descriptionText.text == "" {
            self.descriptionText.text = "-"
        }
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
}
