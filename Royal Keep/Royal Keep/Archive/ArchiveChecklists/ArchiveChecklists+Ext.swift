//
//  ArchiveChecklists+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 7/20/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
extension ArchiveChecklistsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklists.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.notesCellIdentifier, for: indexPath) as! NoteTableViewCell
        let checklist = checklists[indexPath.row]
        cell.titleLabel.text = checklist.Title
        cell.descriptionText.text = checklist.Description
        if let labelId = checklist.LabelID, let label = labelsDictionary[labelId] {
            let hex = String(label.Color, radix: 16)
            cell.label.backgroundColor = UIColor.init(hex: hex)
        } else {
            cell.label.backgroundColor = GlobalSettings.shared().lightGray
        }
        if let reminderDate = checklist.reminderDate {
            cell.hasReminder = true
        } else {
            cell.hasReminder = false
        }
        cell.isCompleted = checklist.isChecklistCompleted()
        cell.checkBox.isUserInteractionEnabled = false
        cell.inEditingMode = inEditingMode
        cell.isCellSelected = selectedItems.contains(indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let checklist = checklists[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! NoteTableViewCell
        if inEditingMode {
            if cell.isCellSelected {
                selectedItems = selectedItems.filter { $0 != indexPath.row }
                print(selectedItems)
                isAllSelected = false
            } else {
                selectedItems.append(indexPath.row)
                selectedItems.count == checklists.count ?  isAllSelected = true : ()
            }
            cell.isCellSelected = !cell.isCellSelected
        } else {
            toChecklistDetails(checklist: checklist, indexPath: indexPath)
        }
    }
    func toChecklistDetails(checklist: Checklist, indexPath: IndexPath) {
        let index = mainChecklists.firstIndex(of: checklist)
        let label = labelsDictionary[checklist.LabelID ?? ""]
        let labels = Array(labelsDictionary.values)
        self.coordinator?.toChecklistDetails(self, checklistID: checklist.CheckListID!, indexPath: indexPath, checklistLabel: label, labels: labels, mainIndex: index!)
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        guard !inEditingMode else {return nil}
        let checklist = checklists[indexPath.row]
        let archive = UIAction(title: "حذف از آرشیو", image: UIImage(systemName: "archivebox")) { [weak self] _ in
            self?.archive(checklist: checklist)
        }
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [archive])
        }
    }
    func archive(checklist: Checklist) {
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.checklistsSetCategory, method: .post, parameters: ["ItemIDs": [checklist.CheckListID!], "Category": 0], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
extension ArchiveChecklistsVC: ArchiveChecklistDetailsVCDelegate {
    func backIconTouchUpInisde() {
        request()
    }
}
