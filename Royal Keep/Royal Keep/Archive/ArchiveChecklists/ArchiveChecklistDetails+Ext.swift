//
//  ArchiveChecklistDetails+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 7/21/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
extension ArchiveChecklistDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklistItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = checklistItems[indexPath.row]
        let cell = ChecklistItemTableViewCell()
        cell.delegate = self
        cell.titleLabel.text = item.Title
        cell.titleText.text = item.Title
        cell.descriptionText.text = item.Description
        cell.isCompleted = item.Completed!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        let label = UILabel()
        label.text = "لیست آیتم ها"
        label.font = GlobalSettings.shared().boldSystemFont(size: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15)])
        return view
    }
}
extension ArchiveChecklistDetailsVC: ChecklistItemTableViewCellDelegate {
    func moreButtonTouchUpInside(_ cell: ChecklistItemTableViewCell) {
        if cell.isExpanded {
            cell.isExpanded = false
            cell.titleText.text != "" ? (cell.titleLabel.text = cell.titleText.text) : (cell.titleLabel.text = "عنوان")
            cell.removeComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        } else {
            cell.isExpanded = true
            cell.addComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
}
extension ArchiveChecklistDetailsVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}
