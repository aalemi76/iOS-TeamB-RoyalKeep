//
//  File.swift
//  Royal Keep
//
//  Created by Catalina on 7/4/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let checklistManager = CheckListManager()

class ChecklistsViewController: SharedViewController, Storyboarded {
    //MARK:- Variables
    var coordinator: ChecklistCoordinator?
    var checklists = [Checklist]()
    var mainChecklists = [Checklist]()
    let addButton = UIButton(type: .system)
    var mainIndex: Int?
    var labelsDictionary = [String: Label]()
    @IBOutlet weak var emptyChecklistLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var searchResult = [Checklist]()
    var inSearchMode: Bool = false {
        didSet {
            searchResult.removeAll()
            tableView.reloadData()
        }
    }
    let editView = EditView()
    var editButton, selectButton, archiveButton: UIButton!
    var isAllSelected: Bool = false {
        willSet {
            if newValue {
                selectButton.setTitle("غیر فعال کردن همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                selectButton.setTitle("انتخاب همه", for: .normal)
                selectButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            }
        }
    }
    var inEditingMode: Bool = false {
        willSet {
            addButton.isHidden = newValue
            let nav = navigationController as? RKNavigationController
            nav?.menuIcon.isHidden = newValue
            searchController.searchBar.isHidden = newValue
            archiveButton.isHidden = !newValue
            selectButton.isHidden = !newValue
            newValue ? editButton.setTitle("انصراف", for: .normal) : editButton.setTitle("ویرایش", for: .normal)
        }
    }
    var selectedItems = [Int]()
    //MARK:- Actions
    @objc func addButtonDidTap(sender: UIButton) {
        let vc = AddChecklistViewController.instantiate()
        vc.labels = Array(labelsDictionary.values)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }
    @objc func didTapBackIcon(tapGestureRecognizer: UITapGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
    @objc func didTapEditButton(sender: UIButton) {
        selectedItems.removeAll()
        isAllSelected = false
        inEditingMode = !inEditingMode
        tableView.reloadData()
    }
    @objc func didTapSelectButton(sender: UIButton) {
        guard checklists.count != 0 else {return}
        if selectedItems.count < checklists.count {
            selectedItems = Array(0..<checklists.count)
            isAllSelected = true
            tableView.reloadData()
        } else {
            selectedItems.removeAll()
            isAllSelected = false
            tableView.reloadData()
        }
    }
    @objc func didTapArchiveButton(sender: UIButton) {
        guard selectedItems.count != 0 else {return}
        let ids = getChecklistID()
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.checklistsSetCategory, method: .post, parameters: ["ItemIDs": ids, "Category": 2], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    func presentAlert() {
        let alert = UIAlertController(title: "افزودن به آرشیو", message: "چک لیست ها با موفقیت به آرشیو اضافه شد.", preferredStyle: .alert)
        let action = UIAlertAction(title: "فهمیدم", style: .default) { _ in
            self.inEditingMode = false
            self.request()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSideMenu()
        sideMenuVC.delegate = self
        configureButton()
        configureEditView()
        setViewComponents(forViews: [emptyChecklistLbl, tableView, addButton, editView])
        getLabels()
        tableView.topAnchor.constraint(equalTo: editView.bottomAnchor).isActive = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "NoteTableViewCell", bundle: nil), forCellReuseIdentifier: K.notesCellIdentifier)
        configureSearchController()
        request()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if isSideMenuOpened {
            closeSideMenu()
        }
    }
    //MARK:- View Layout
    func configureNavigationBar() {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.setNavigationBarHidden(false, animated: true)
        navigationItem.title = "چک لیست ها"
        navigationController.menuIcon.isHidden = false
        navigationController.addMenuIcon()
        navigationController.backIcon.isHidden = true
        navigationController.editIcon.isHidden = true
    }
    func configureButton() {
        addButton.setBackgroundImage(UIImage(systemName: "plus.circle.fill"), for: .normal)
        addButton.tintColor = GlobalSettings.shared().mainColor
        addButton.addTarget(self, action: #selector(addButtonDidTap(sender:)), for: .touchUpInside)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(addButton)
        NSLayoutConstraint.activate([
            addButton.widthAnchor.constraint(equalToConstant: 50),
            addButton.heightAnchor.constraint(equalToConstant: 50),
            addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            addButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)])
    }
    func handleNotification() {
        guard let mainIndex = mainIndex else {return}
        let checklist = mainChecklists[mainIndex]
        let index = checklists.firstIndex(of: checklist)
        let indexPath = IndexPath(row: index!, section: 0)
        self.mainIndex = nil
        let label = labelsDictionary[checklist.LabelID ?? ""]
        let labels = Array(labelsDictionary.values)
        coordinator?.toChecklistDetails(self, checklistID: checklist.CheckListID!, indexPath: indexPath, checklistLabel: label, labels: labels, mainIndex: mainIndex)
    }
    //MARK:- Web API
    func getLabels() {
        let url = RKAPI.labelsURL
        AF.request(url, method: .get, headers: RKAPI.username).responseString { response in
            switch response.result {
            case .success:
                let jsonObject = JSON(response.data ?? Data())
                let n = jsonObject.count
                for i in 0..<n {
                    if let title = jsonObject[i]["Title"].string, let color = jsonObject[i]["Color"].int, let labelId = jsonObject[i]["LabelID"].string {
                        let label = Label(Title: title, Color: color, LabelID: labelId)
                        self.labelsDictionary[label.LabelID] = label
                    }
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    
    func request() {
        AF.request(checklistManager.url , headers: HTTPHeaders(["username": User.globalUsername])).responseJSON { (data) in
            do {
                self.mainChecklists = try checklistManager.setFinalArrayOfModels(data: data.data!)
                self.mainChecklists = self.mainChecklists.sorted(by: {$0.CreationDate! < $1.CreationDate!})
                ReminderCollection.setReminder(checklists: self.mainChecklists)
                ReminderCollection.update(checklists: self.mainChecklists)
                self.checklists = self.mainChecklists.filter { (checklist: Checklist) -> Bool in
                    return checklist.Category! != 2
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.configureEmptyNoteMessage(isHidden: !self.checklists.isEmpty)
                    self.handleNotification()
                }
            }catch Error.checkListIsEmpty {
                self.configureEmptyNoteMessage(isHidden: false)
            }
            catch {
                print(error)
            }
        }
    }
    func editChecklist(checklist: Checklist) {
        let url = URL(string: RKAPI.updateChecklistsURL)!
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try? encoder.encode(checklist)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "PUT"
        urlRequest.headers = RKAPI.headers
        urlRequest.httpBody = data
        AF.request(urlRequest).responseJSON { response in
            switch response.result {
            case .success:
                debugPrint(response)
            case .failure(let error):
                print("Error encountered while geting checklist: \(error.localizedDescription)")
            }
        }
    }
    func configureEmptyNoteMessage(isHidden: Bool) {
        self.tableView.isHidden = !isHidden
        self.emptyChecklistLbl.isHidden = isHidden
        self.emptyChecklistLbl.text = "موردی برای نمایش یافت نشد"
        self.emptyChecklistLbl.font = GlobalSettings.shared().boldSystemFont(size: 15)
        self.emptyChecklistLbl.textColor = GlobalSettings.shared().darkGray
        self.emptyChecklistLbl.textAlignment = .center
        self.emptyChecklistLbl.sizeToFit()
        emptyChecklistLbl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            emptyChecklistLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            emptyChecklistLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
    }
    //MARK:- Edit View
    func configureEditView() {
        editView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(editView)
        NSLayoutConstraint.activate([
            editView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            editView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            editView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            editView.heightAnchor.constraint(equalToConstant: 40)])
        editButton = editView.editButton
        editButton.addTarget(self, action: #selector(didTapEditButton(sender:)), for: .touchUpInside)
        selectButton = editView.selectButton
        selectButton.isHidden = true
        selectButton.addTarget(self, action: #selector(didTapSelectButton(sender:)), for: .touchUpInside)
        editView.addArchiveButton()
        archiveButton = editView.archiveButton
        archiveButton.isHidden = true
        archiveButton.addTarget(self, action: #selector(didTapArchiveButton(sender:)), for: .touchUpInside)
    }
    func getChecklistID() -> [String] {
        var ids = [String]()
        for i in selectedItems {
            ids.append(checklists[i].CheckListID!)
        }
        return ids
    }
    //MARK:- Search Configurations
    func configureSearchController() {
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.definesPresentationContext = true
        searchController.searchBar.semanticContentAttribute = .forceRightToLeft
        searchController.searchBar.setValue("انصراف", forKey:"cancelButtonText")
        searchController.searchBar.tintColor = GlobalSettings.shared().mainColor
        searchController.searchBar.searchTextField.textAlignment = .right
        searchController.searchBar.searchTextField.font = GlobalSettings.shared().mediumSystemFont(size: 15)
        searchController.searchBar.searchTextField.textColor = GlobalSettings.shared().darkGray
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "جستجو"
        searchController.searchBar.searchBarStyle = .minimal
        tableView.tableHeaderView = searchController.searchBar
        tableView.tableHeaderView?.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        tableView.tableHeaderView?.layer.borderWidth = 1
    }
    func search() {
        searchResult = checklists.filter { (checklist: Checklist) -> Bool in
            guard let rawTitle = searchController.searchBar.text else { return false }
            let title = rawTitle.trimmingCharacters(in: .whitespacesAndNewlines)
            guard let checklistTitle = checklist.Title else { return false }
            return checklistTitle.lowercased().contains(title.lowercased())
        }
        tableView.reloadData()
    }
}
