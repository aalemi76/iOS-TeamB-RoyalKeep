//
//  ChecklistViewController+Ext.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/14/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
extension ChecklistsViewController: SideMenuDelegate {
    func logoutButtonTouchUpInside() {
        closeSideMenu()
        let alert = UIAlertController(title: "خروج از برنامه!", message: "در صورت خروج همه ی اطلاعات شما حذف خواهد شد.", preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "خروج", style: .destructive) { _ in
            do {
                try FileManager.default.removeItem(at: User.userArchiveURL)
                User.resetUser()
                ReminderCollection.removeAllNotes()
                ReminderCollection.removeAllChecklists()
                self.coordinator?.toLoginView()
            } catch {
                print(error)
            }
        }
        alert.addAction(doneAction)
        let cancelAction = UIAlertAction(title: "انصراف", style: .default, handler: nil)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}
extension ChecklistsViewController :UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let result: Int
        inSearchMode ? (result = searchResult.count) : (result = checklists.count)
        return result
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.notesCellIdentifier, for: indexPath) as! NoteTableViewCell
        let checklist: Checklist
        inSearchMode ? (checklist = searchResult[indexPath.row]) : (checklist = checklists[indexPath.row])
        cell.titleLabel.text = checklist.Title
        cell.descriptionText.text = checklist.Description
        if let labelId = checklist.LabelID, let label = labelsDictionary[labelId] {
            let hex = String(label.Color, radix: 16)
            cell.label.backgroundColor = UIColor.init(hex: hex)
        } else {
            cell.label.backgroundColor = GlobalSettings.shared().lightGray
        }
        if let reminderDate = checklist.reminderDate {
            cell.hasReminder = true
        } else {
            cell.hasReminder = false
        }
        cell.isCompleted = checklist.isChecklistCompleted()
        cell.checkBox.isUserInteractionEnabled = checklist.hasItem()
        cell.inEditingMode = inEditingMode
        cell.isCellSelected = selectedItems.contains(indexPath.row)
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let checklist: Checklist
        let cell = tableView.cellForRow(at: indexPath) as! NoteTableViewCell
        inSearchMode ? (checklist = searchResult[indexPath.row]) : (checklist = checklists[indexPath.row])
        if inSearchMode {
        searchController.dismiss(animated: true) {
            self.toChecklistDetails(checklist: checklist, indexPath: indexPath)}
            return
        } else if inEditingMode {
            if cell.isCellSelected {
                selectedItems = selectedItems.filter { $0 != indexPath.row }
                print(selectedItems)
                isAllSelected = false
            } else {
                selectedItems.append(indexPath.row)
                selectedItems.count == checklists.count ?  isAllSelected = true : ()
            }
            cell.isCellSelected = !cell.isCellSelected
        } else {
            toChecklistDetails(checklist: checklist, indexPath: indexPath)
        }
    }
    func toChecklistDetails(checklist: Checklist, indexPath: IndexPath) {
        let index = mainChecklists.firstIndex(of: checklist)
        let label = labelsDictionary[checklist.LabelID ?? ""]
        let labels = Array(labelsDictionary.values)
        self.coordinator?.toChecklistDetails(self, checklistID: checklist.CheckListID!, indexPath: indexPath, checklistLabel: label, labels: labels, mainIndex: index!)
    }
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        guard !inEditingMode && !inSearchMode else {return nil}
        let checklist = checklists[indexPath.row]
        let archive = UIAction(title: "آرشیو", image: UIImage(systemName: "archivebox.fill")) { [weak self] _ in
            self?.archive(checklist: checklist)
        }
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [archive])
        }
    }
    func archive(checklist: Checklist) {
        let queue = DispatchQueue.global()
        queue.async {
            AF.request(RKAPI.checklistsSetCategory, method: .post, parameters: ["ItemIDs": [checklist.CheckListID!], "Category": 2], encoding: JSONEncoding.prettyPrinted, headers: RKAPI.username).response { response in
                switch response.result {
                case .success:
                    DispatchQueue.main.async {
                        self.presentAlert()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
extension ChecklistsViewController: NoteTableViewCellDelegate {
    func checkBoxButtonTouchUpInside(_ cell: NoteTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {return}
        let checklist: Checklist
        inSearchMode ? (checklist = searchResult[indexPath.row]) : (checklist = checklists[indexPath.row])
        checklist.toggleChecklistItems(state: cell.isCompleted)
        editChecklist(checklist: checklist)
    }
}
extension ChecklistsViewController: AddChecklistViewControllerDelegate {
    func addNewChecklist(_ controller: AddChecklistViewController, didAddItem item: Checklist?, at indexPath: IndexPath?) {
        request()
    }
}
extension ChecklistsViewController: ChecklistDetailsViewControllerDelegate {
    func backIconTouchUpInisde(isEdited: Bool) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        if isEdited {
            navigationController.menuIcon.isHidden = false
            editView.isHidden = false
            inSearchMode = false
            searchController.searchBar.text = ""
            request()
        }
    }
}
extension ChecklistsViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.menuIcon.isHidden = true
        editView.isHidden = true
        inSearchMode = true
        return
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        guard let navigationController = navigationController as? RKNavigationController else {return}
        navigationController.menuIcon.isHidden = false
        editView.isHidden = false
        inSearchMode = false
        return
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search()
        return
    }
}
