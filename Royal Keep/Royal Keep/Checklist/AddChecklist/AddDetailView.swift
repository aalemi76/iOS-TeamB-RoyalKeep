//
//  AddChecklistView.swift
//  Royal Keep
//
//  Created by Catalina on 7/6/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class AddDetailView: UIView {
    let scrollView = UIScrollView()
    let contentView = UIView()
    let dateLabel = UILabel()
    let titleLabel = UILabel()
    let titleText = UITextView()
    let label = UILabel()
    let labelButton = RKButtonLabel()
    let descriptionLabel = UILabel()
    let descriptionText = UITextView()
    var topDistance: CGFloat = 25 {
        willSet {
            titleLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: newValue).isActive = true
        }
    }
    init() {
        super.init(frame: .zero)
        configure()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configure() {
        addScrollView()
        addContentView()
        addDateLabel()
        addTitleLabel()
        addTitleTextView()
        addLabel()
        addLabelButton()
        addDescriptionLabel()
        addDescriptionTextView()
    }
    func addScrollView() {
        scrollView.layoutIfNeeded()
        scrollView.backgroundColor = GlobalSettings.shared().lightGray
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    func addContentView() {
        contentView.sizeToFit()
        contentView.backgroundColor = GlobalSettings.shared().lightGray
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
        ])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: bounds.height)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
    }
    func addDateLabel() {
        dateLabel.font = GlobalSettings.shared().mediumSystemFont(size: 20)
        dateLabel.textColor = GlobalSettings.shared().darkGray
        dateLabel.textAlignment = .center
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(dateLabel)
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 25),
            dateLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
    }
    func addTitleLabel() {
        titleLabel.text = "عنوان چک لیست :"
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        titleLabel.textColor = .black
        titleLabel.textAlignment = .right
        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)])
    }
    func addTitleTextView() {
        titleText.sizeToFit()
        titleText.isScrollEnabled = false
        titleText.backgroundColor = .clear
        titleText.isUserInteractionEnabled = true
        titleText.translatesAutoresizingMaskIntoConstraints = false
        titleText.font = GlobalSettings.shared().boldSystemFont(size: 18)
        titleText.textColor = .black
        titleText.textAlignment = .right
        contentView.addSubview(titleText)
        NSLayoutConstraint.activate([
            titleText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            titleText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        makeRound(titleText)
    }
    func addLabel() {
        label.text = "برچسب :"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = GlobalSettings.shared().boldSystemFont(size: 18)
        label.textColor = .black
        label.textAlignment = .right
        contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: titleText.bottomAnchor, constant: 50),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
    }
    func addLabelButton() {
        labelButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(labelButton)
        NSLayoutConstraint.activate([
            labelButton.trailingAnchor.constraint(equalTo: label.leadingAnchor, constant: -15),
            labelButton.centerYAnchor.constraint(equalTo: label.centerYAnchor),
            labelButton.widthAnchor.constraint(equalToConstant: 120),
            labelButton.heightAnchor.constraint(equalToConstant: 30)])
    }
    func addDescriptionLabel() {
        descriptionLabel.text = "متن چک لیست :"
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.font = GlobalSettings.shared().boldSystemFont(size: 18)
        descriptionLabel.textColor = .black
        descriptionLabel.textAlignment = .right
        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 25),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)])
    }
    func addDescriptionTextView() {
        descriptionText.sizeToFit()
        descriptionText.layoutIfNeeded()
        descriptionText.isScrollEnabled = false
        descriptionText.backgroundColor = .clear
        descriptionText.isUserInteractionEnabled = true
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.font = GlobalSettings.shared().boldSystemFont(size: 15)
        descriptionText.textColor = GlobalSettings.shared().darkGray
        descriptionText.textAlignment = .right
        contentView.addSubview(descriptionText)
        NSLayoutConstraint.activate([
            descriptionText.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10),
            descriptionText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            descriptionText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            descriptionText.bottomAnchor.constraint(greaterThanOrEqualTo: contentView.bottomAnchor, constant: -20)])
        makeRound(descriptionText)
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
}
