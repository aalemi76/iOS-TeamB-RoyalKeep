//
//  AddChecklistViewController.swift
//  Royal Keep
//
//  Created by Catalina on 7/6/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol AddChecklistViewControllerDelegate: class {
    func addNewChecklist(_ controller: AddChecklistViewController, didAddItem item: Checklist?, at indexPath: IndexPath?)
}
class AddChecklistViewController: UIViewController, Storyboarded {
    
    enum CheckListAction {
        case addCheckLsit
        case cancelEditing
    }
    //MARK:- Variables
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    weak var delegate: AddChecklistViewControllerDelegate?
    var titleView: TitleView!
    var checkmarkIcon: UIImageView!
    var xmarkIcon: UIImageView!
    let pageControl = UIPageControl()
    let mainScrollView = UIScrollView()
    var detailView = AddDetailView()
    var detailScrollView: UIScrollView!
    var contentView: UIView!
    var dateLabel: UILabel!
    var titleText = UITextView()
    var labelButton: RKButtonLabel!
    var descriptionText = UITextView()
    var tableView: UITableView!
    var itemToEdit: Checklist?
    var indexPath: IndexPath?
    var numberOfRows: Int!
    var labels = [Label]()
    var checkListLabel: Label?
    var actionManager: UserActionManagment!
    var date = Date()
    var newChecklist: Checklist!
    var checklistItems = [ChecklistItem]()
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    let persianDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .persian)
        formatter.locale = Locale(identifier: "fa_IR")
        formatter.dateStyle = .short
        formatter.dateFormat = "dd - MMMM - yyyy"
        return formatter
    }()
    var orginalNumberOfRows = Int()
    //MARK:- Actions
    @objc func viewDidTap(tapGestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardViewEndFrame = view.convert(keyboardSize, from: view.window)
            detailScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 50, right: 0)
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 50, right: 0)
        }
        detailScrollView.scrollIndicatorInsets = detailScrollView.contentInset
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        detailScrollView.contentInset = .zero
        detailScrollView.scrollIndicatorInsets = detailScrollView.contentInset
        tableView.contentInset = .zero
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    @objc func checkmarkIconTouchUpInside() {
        if checkActions(action: .addCheckLsit) && actionManager.actionChecker(for: .addCheckList)  {
            view.endEditing(true)
            modifyChecklist()
            showAlertView(alert: .ok, message: "چک لیست شما با موفقیت اضافه شد")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.dismiss(animated: true, completion: nil)
            }
            
        } else {
            showAlertView(alert: .error, message: "لطفا ورودی ها را کنترل نمایید")
        }
    }
    @objc func xmarkIconTouchUpInside() {
        if checkActions(action: .cancelEditing)   {
            let alert = UIAlertController(title: "اخطار", message: "همه ی نوشته های خود را از دست میدهید", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "خیر", style: .cancel ) {
                action in
                return
            }
            
            let okAction = UIAlertAction(title: "بلی", style: .destructive ) {
                action in
                self.dismiss(animated: true, completion: nil)
            }
            
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true)
            
        } else {
            
            dismiss(animated: true, completion: nil)
        }
    }
    @objc func didTapLabelBtn() {
        let vc = CustomPickerViewController()
        vc.labels = labels
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        if let label = checkListLabel {
            vc.selectedItem = label
            for (i, item) in labels.enumerated() {
                if item.Color == label.Color {
                    vc.selectedIndex = i
                    break
                }
            }
        }
      
        present(vc, animated: true, completion: nil)
        vc.cancelCompletion = { picker in
            picker.dismiss(animated: true, completion: nil)
        }
        vc.removeCompletion = { picker in
            picker.dismiss(animated: true) {
                self.checkListLabel = nil
                self.isEditing = true
                self.configureLabelButton()
            }
        }
        vc.doneCompletion = { picker, label in
            picker.dismiss(animated: true) {
                self.checkListLabel = label
                self.isEditing = true
                self.configureLabelButton()
            }
        }
    }
    
    func checkActions(action: CheckListAction) -> Bool {
        
        let txtVTitle_strLenght = titleText.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        let txtVDescription_strLenght = descriptionText.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        
        switch action {
        case .addCheckLsit:
            
                
            if titleText.text!.isEmpty {
                return false
            } else if txtVTitle_strLenght.count == 0 {
                return false
            }
//            else if actionManager.actionChecker(for: .addCheckList) {
//                return true
//            }
//            else if actionManager.addItemText == "" {
//                    return false
//            }
            else {
                return true
            }
            
        case .cancelEditing:
            
            if itemToEdit != nil {
                orginalNumberOfRows = itemToEdit!.CheckListItems!.count
                print("orginalNumberOfRows: \(orginalNumberOfRows)")
                print("numberOfRows: \(numberOfRows)")
                if numberOfRows > orginalNumberOfRows || numberOfRows < orginalNumberOfRows {
                    return  true
                }  else if isEditing == true {
                    return true
                } else {
                    return false
                }
            } else if checkChangesInItem(){
                return true
            }  else {
                if !titleText.text!.isEmpty {
                    if txtVTitle_strLenght.count == 0 {
                        return false
                    }
                    return true
                } else if !descriptionText.text.isEmpty {
                    
                    if txtVDescription_strLenght.count == 0 {
                         return false
                     }
                     return true
                } else if (labelButton.labelTitle.text != "بدون برچسب"){
                    return true
                } else {
                    return false
                }

            }
            
        }
    }
    //MARK:- View Controller Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewDidTap(tapGestureRecognizer:)))
        tapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGestureRecognizer)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        configureTitleView()
        configurePageControll()
        configureMainScrollView()
        configureDetailView()
        configureItemDetail()
        configureDateLabel()
        configureLabelButton()
        configureTableView()
        titleText.delegate = self
        descriptionText.delegate = self
        view.bringSubviewToFront(pageControl)
        actionManager = UserActionManagment(viewController: self)
        
        if itemToEdit != nil {
            actionManager.addItemText = "itemToEdit"
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        orginalNumberOfRows = tableView.numberOfRows(inSection: 0)
    }
    //MARK:- Web API
    func isItemTitleFill(forCell cell: AddItemTableViewCell) -> Bool {
        if cell.titleText.text != "" {
            return true
        } else {
            return false
        }
    }
    func cofigureActivityIndicator(activityView: UIView) {
        activityView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityView)
        NSLayoutConstraint.activate([
            activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityView.widthAnchor.constraint(equalToConstant: 80),
            activityView.heightAnchor.constraint(equalToConstant: 80)])
    }
    func configureChecklistItems() {
        if itemToEdit != nil {
            let n = checklistItems.count
            let numOfNewItems = tableView.numberOfRows(inSection: 0) - n
            for i in 0..<n {
                let index = IndexPath(row: i, section: 0)
                let cell = tableView.cellForRow(at: index) as! AddItemTableViewCell
                var item = checklistItems[i]
                item.Title = cell.titleText.text
                item.Description = cell.descriptionText.text
                checklistItems.remove(at: i)
                checklistItems.insert(item, at: i)
            }
            for i in n..<n+numOfNewItems {
                let index = IndexPath(row: i, section: 0)
                let cell = tableView.cellForRow(at: index) as! AddItemTableViewCell
                let temp = ChecklistItem(Title: cell.titleText.text, Description: cell.descriptionText.text)
                checklistItems.append(temp)
            }
        } else {
            let n = tableView.numberOfRows(inSection: 0)
            for i in 0..<n {
                let index = IndexPath(row: i, section: 0)
                let cell = tableView.cellForRow(at: index) as! AddItemTableViewCell
                let temp = ChecklistItem(Title: cell.titleText.text, Description: cell.descriptionText.text)
                checklistItems.append(temp)
            }
        }
    }
    func modifyChecklist() {
        var activityView: RKActivityIndicatorView? = RKActivityIndicatorView()
        cofigureActivityIndicator(activityView: activityView!)
        activityView?.showSpinner()
        let title = titleText.text!
        let description = descriptionText.text ?? ""
        let creationDate = dateFormatter.string(from: date)
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        var url: URL
        var method: String
        var headers: HTTPHeaders
        var data: Data?
        if let item = itemToEdit {
            let parameters = Checklist(Title: title, Description: description, LabelID: checkListLabel?.LabelID, CheckListItems: checklistItems, Category: item.Category, CheckListID: item.CheckListID!)
            data = try? encoder.encode(parameters)
            url = URL(string: RKAPI.updateChecklistsURL)!
            method = "PUT"
            headers = RKAPI.headers
        } else {
            let parameters = Checklist(Title: title, Description: description, CreationDate: creationDate, LabelID: checkListLabel?.LabelID, CheckListItems: checklistItems)
            data = try? encoder.encode(parameters)
            url = URL(string: RKAPI.createChecklistsURL)!
            method = "POST"
            headers = RKAPI.headers
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.headers = headers
        urlRequest.httpBody = data
        urlRequest.httpMethod = method
        AF.request(urlRequest).responseJSON(completionHandler: {
            response in
            switch response.result {
            case .success:
                print(response)
                activityView?.showCheckmark()
                let jsonObject = JSON(response.data ?? Data())
                if let title = jsonObject["Title"].string, let checklistId = jsonObject["CheckListID"].string, let creationDate = jsonObject["CreationDate"].string {
                    let description = jsonObject["Description"].string
                    let labelId = jsonObject["LabelID"].string
                    let category = jsonObject["Category"].int
                    var checklistItems = [ChecklistItem]()
                    if let items = jsonObject["CheckListItems"].array {
                        for item in  items {
                            let temp = ChecklistItem(Title: item["Title"].string, Description: item["Description"].string, Completed: item["Completed"].bool, ID: item["ID"].string)
                            checklistItems.append(temp)
                        }
                    }
                    self.newChecklist = Checklist(Title: title, Description: description, CreationDate: creationDate, LabelID: labelId, CheckListItems: checklistItems, Category: category, CheckListID: checklistId)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        activityView?.dismiss()
                        activityView?.removeFromSuperview()
                        activityView = nil
                        //self.showAlertView(alert: .ok, message: "یادداشت شما با موفقیت ثبت شد")
                        self.delegate?.addNewChecklist(self, didAddItem: self.newChecklist, at: self.indexPath)
                    }
                }
            case let .failure(error):
                print(error)
            }
        })
    }
    //MARK:- View Layout
    func configureTitleView() {
        if let _ = itemToEdit {
            let title = "ویرایش چک لیست"
            titleView = TitleView(title: title)
        } else {
            titleView = TitleView(title: "افزودن چک لیست جدید")
        }
        titleView.addCheckmarkIcon()
        titleView.addXmarkIcon()
        checkmarkIcon = titleView.checkmarkIcon
        let checkTap = UITapGestureRecognizer(target: self, action: #selector(checkmarkIconTouchUpInside))
        checkmarkIcon.addGestureRecognizer(checkTap)
        xmarkIcon = titleView.xmarkIcon
        let xTap = UITapGestureRecognizer(target: self, action: #selector(xmarkIconTouchUpInside))
        xmarkIcon.addGestureRecognizer(xTap)
        titleView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(titleView)
        NSLayoutConstraint.activate([
            titleView.topAnchor.constraint(equalTo: view.topAnchor),
            titleView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            titleView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            titleView.heightAnchor.constraint(equalToConstant: 90)])
    }
    func configureDateLabel() {
        if let item = itemToEdit {
            date = dateFormatter.date(from: item.CreationDate!)!
            dateLabel.text = persianDateFormatter.string(from: date)
        } else {
            dateLabel.text = persianDateFormatter.string(from: date)
        }
    }
    func configurePageControll() {
        pageControl.pageIndicatorTintColor = .darkGray
        pageControl.currentPageIndicatorTintColor = GlobalSettings.shared().mainColor
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(pageControl)
        NSLayoutConstraint.activate([
            pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20),
            pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor)])
        pageControl.numberOfPages = 2
        pageControl.currentPage = 0
    }
    func configureMainScrollView() {
        mainScrollView.delegate = self
        mainScrollView.backgroundColor = .systemGray4
        mainScrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainScrollView)
        NSLayoutConstraint.activate([
            mainScrollView.topAnchor.constraint(equalTo: titleView.bottomAnchor),
            mainScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        mainScrollView.contentSize = CGSize(width: view.bounds.width * 2, height: view.bounds.height - 90)
        mainScrollView.isPagingEnabled = true
        mainScrollView.showsHorizontalScrollIndicator = false
    }
    func configureDetailView() {
        detailView = AddDetailView(frame: CGRect(x: 15, y: 15, width: view.bounds.width - 30, height: view.bounds.height - 155))
        detailView.topDistance = 25
        mainScrollView.addSubview(detailView)
        makeRound(detailView)
        detailScrollView = detailView.scrollView
        contentView = detailView.contentView
        dateLabel = detailView.dateLabel
        titleText = detailView.titleText
        labelButton = detailView.labelButton
        descriptionText = detailView.descriptionText
    }
    func configureItemDetail() {
        guard let item = itemToEdit else {
            numberOfRows = 0
            return
        }
        numberOfRows = checklistItems.count
        titleText.text = item.Title
        descriptionText.text = item.Description
        for label in labels {
            if label.LabelID == item.LabelID {
                let hex = String(label.Color, radix: 16)
                labelButton.colorView.backgroundColor = UIColor.init(hex: hex)
                labelButton.setTitle(label.Title, for: .normal)
            }
        }
    }
    func configureLabelButton() {
        labelButton.addTarget(self, action: #selector(didTapLabelBtn), for: .touchUpInside)
        guard let label = checkListLabel else {
            labelButton.colorView.backgroundColor = UIColor.white
            labelButton.setTitle("بدون برچسب", for: .normal)
            return
        }
        let hex = String(label.Color, radix: 16)
        labelButton.colorView.backgroundColor = UIColor.init(hex: hex)
        labelButton.setTitle(label.Title, for: .normal)
    }
    func configureTableView() {
        tableView = UITableView(frame: CGRect(x: view.bounds.width + 15, y: 15, width: view.bounds.width - 30, height: view.bounds.height - 155), style: .grouped)
        mainScrollView.addSubview(tableView)
        makeRound(tableView)
        tableView.register(AddItemTableViewCell.self, forCellReuseIdentifier: AddItemTableViewCell.reuseID)
        tableView.delegate = self
        tableView.dataSource = self
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
    //MARK:- Helper Method
    
    func checkEmptyItemRows() -> Bool {
        let numberOfRows = tableView.numberOfRows(inSection: 0)
        var result = Bool()
        if numberOfRows > 0 {
            for row in 0..<numberOfRows {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: 0)) as! AddItemTableViewCell
                if cell.titleText.text.isEmpty {
                        result =  true
                        break
                } else if cell.titleText.text!.trimmingCharacters(in: CharacterSet.whitespaces).count == 0 {
                        result =  true
                        break
                } else {
                        result = false
                }
            }
        } else {
            result = false
        }
        return result
    }
    func checkChangesInItem() -> Bool {
       
        print(orginalNumberOfRows)
        if itemToEdit != nil {
            if numberOfRows > orginalNumberOfRows || numberOfRows! < orginalNumberOfRows {
                self.isEditing =  true
            } else {
                self.isEditing =  false
            }
        }
        if numberOfRows > orginalNumberOfRows || numberOfRows! < orginalNumberOfRows {
            return true
        } else {
            return false
        }
    }
        
}
