//
//  AddNewItemCell.swift
//  Royal Keep
//
//  Created by Catalina on 7/7/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
class AddNewItemCell: UITableViewCell {
    static let reuseID = "AddNewItemCell"
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configure() {
        let label = UILabel()
        label.text  = "افزودن آیتم جدید"
        label.textAlignment = .right
        label.font = GlobalSettings.shared().boldSystemFont(size: 15)
        label.textColor = GlobalSettings.shared().darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
            label.centerXAnchor.constraint(equalTo: centerXAnchor)])
        let button = UIButton(type: .system)
        button.setBackgroundImage(UIImage(systemName: "plus.circle.fill"), for: .normal)
        button.tintColor = GlobalSettings.shared().mainColor
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        NSLayoutConstraint.activate([
            button.leadingAnchor.constraint(equalTo: label.trailingAnchor, constant: 5),
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            button.widthAnchor.constraint(equalTo: heightAnchor, constant: -30),
            button.heightAnchor.constraint(equalTo: heightAnchor, constant: -30)])
    }
}
