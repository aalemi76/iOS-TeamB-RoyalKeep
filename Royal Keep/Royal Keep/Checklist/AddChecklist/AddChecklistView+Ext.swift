//
//  AddChecklistView+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 7/6/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
extension AddChecklistViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}
extension AddChecklistViewController: UITableViewDelegate, UITableViewDataSource {
    
    enum ItemAction {
        case addItem
        case cancelEditing
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 55
        } else {
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 55
        } else {
            return UITableView.automaticDimension
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 1
        } else {
            return numberOfRows
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 0 else { return nil }
        let view = UIView()
        view.backgroundColor = .white
        let label = UILabel()
        label.text = "لیست آیتم ها"
        label.font = GlobalSettings.shared().boldSystemFont(size: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15)])
        return view
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            let cell = AddNewItemCell()
            return cell
        } else {
            let cell = AddItemTableViewCell()
            cell.titleText.delegate = self
            cell.descriptionText.delegate = self
            cell.delegate = self
            if checklistItems.count - 1 >= indexPath.row {
                let item = checklistItems[indexPath.row]
                cell.titleLabel.text = item.Title
                cell.titleText.text = item.Title
                cell.descriptionText.text = item.Description
            } else {
                cell.titleText.text = ""
                cell.descriptionText.text = ""
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.section == 1 else {return}
        if numberOfRows < 1 {
            let newIndex = IndexPath(row: numberOfRows, section: 0)
            numberOfRows += 1
            tableView.insertRows(at: [newIndex], with: .fade)
            tableView.scrollToRow(at: newIndex, at: .bottom, animated: true)
            let cell = tableView.cellForRow(at: newIndex) as! AddItemTableViewCell
            cell.isExpanded = true
            cell.addComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        } else {
            if actionManager.actionChecker(for: .addCheckListItem) {
                let newIndex = IndexPath(row: numberOfRows, section: 0)
                numberOfRows += 1
                tableView.insertRows(at: [newIndex], with: .fade)
                tableView.scrollToRow(at: newIndex, at: .bottom, animated: true)
                let cell = tableView.cellForRow(at: newIndex) as! AddItemTableViewCell
                cell.isExpanded = true
                cell.addComponents()
                self.tableView.beginUpdates()
                self.tableView.endUpdates()
            } else {
                showAlertView(alert: .error, message: "عنوان ایتم خالی هست")
                return
            }
            actionManager.addItemText = ""
        }
    }
    
    func checkItemAction(action: ItemAction, cell: AddItemTableViewCell) -> Bool {
        switch action {
        case .addItem:
            let txtView = cell.titleText
            let txtView_strLenght = txtView.text!.trimmingCharacters(in: CharacterSet.whitespaces)
            
            if txtView.text.isEmpty {
                return false
            } else if txtView_strLenght.count == 0 {
                return false
            } else if checkEmptyItemRows(){
                return false
            } else {
                return true
            }
        case .cancelEditing:
            return true
        }
    }
    
    func showAlertView(alert: Alert, message: String) {
        var timer: Timer!
        let screenWidth  = UIScreen.main.bounds.width * (90/100)
        let screenHeight = UIScreen.main.bounds.height * (15/100)
        let xPosition    = (UIScreen.main.bounds.width - screenWidth) / 2
        
        
        switch alert {
        case .ok:
            let alertView = RKAlertView(alert: alert, message: message)
            alertView.frame.origin = CGPoint(x: xPosition, y: -screenHeight)
            
            self.view.addSubview(alertView)
            
            UIView.animate(withDuration: 0.8){
                alertView.frame.origin.y += screenHeight + 40
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false, block: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    alertView.frame.origin.y -= screenHeight + 40 }
                    , completion: { _ in alertView.removeFromSuperview() })
            })
            
        case .error:
            let alertView = RKAlertView(alert: alert, message: message)
            alertView.frame.origin = CGPoint(x: xPosition, y: -screenHeight)
            
            self.view.addSubview(alertView)
            
            UIView.animate(withDuration: 0.8){
                alertView.frame.origin.y += screenHeight + 40
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false, block: { _ in
                UIView.animate(withDuration: 0.8, animations: {
                    alertView.frame.origin.y -= screenHeight + 40 }
                    , completion: { _ in alertView.removeFromSuperview() })
            })
        }
        
    }
    
    func lastRowInTableView() -> AddItemTableViewCell? {
        let lastRowInInSection = tableView.numberOfRows(inSection: 0)
        let indexPath = IndexPath(row: lastRowInInSection - 1, section: 0)
        return tableView.cellForRow(at: indexPath) as? AddItemTableViewCell
    }
    
}
extension AddChecklistViewController: AddItemTableViewCellDelegate {
    func moreButtonTouchUpInside(_ cell: AddItemTableViewCell) {
        view.endEditing(true)
        if cell.isExpanded {
            cell.isExpanded = false
            cell.titleText.text != "" ? (cell.titleLabel.text = cell.titleText.text) : (cell.titleLabel.text = "عنوان")
            cell.removeComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        } else {
            cell.isExpanded = true
            cell.addComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
    func deleteButtonTouchUpInside(_ cell: AddItemTableViewCell) {
        view.endEditing(true)
        guard let indexPath = tableView.indexPath(for: cell) else {return}
        guard itemToEdit != nil && checklistItems.count - 1 >= indexPath.row else {
            numberOfRows -= 1
            tableView.deleteRows(at: [indexPath], with: .fade)
            if numberOfRows == 0 {
                actionManager.addItemText = ""
            } else {
                actionManager.addItemText = "Delete"
            }
            return
        }
        numberOfRows -= 1
        checklistItems.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
        
    }
}
extension AddChecklistViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let oldHeight = textView.frame.size.height
        let newHeight = textView.sizeThatFits(textView.frame.size).height
        if oldHeight != newHeight {
            UIView.setAnimationsEnabled(false)
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            UIView.setAnimationsEnabled(true)
        }
        guard let cell = textView.superview?.superview as? AddItemTableViewCell else {return}
        actionManager.addItemText = cell.titleText.text
        if let item = itemToEdit {
            isEditing = true
            let currentText = item.Description
            if textView.text == currentText {
                self.isEditing = false
            } else {
                self.isEditing = true
            }
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let cell = textView.superview?.superview as? AddItemTableViewCell else {return}
        guard let indexPath = tableView.indexPath(for: cell) else {return}
        if indexPath.row > checklistItems.count - 1 {
            let item = ChecklistItem(Title: cell.titleText.text, Description: cell.descriptionText.text)
            checklistItems.append(item)
        } else {
            let item = checklistItems[indexPath.row]
            if cell.titleText.text != "" {
                item.Title = cell.titleText.text
                item.Description = cell.descriptionText.text
            }
        }
    }
    
}
