//
//  UserActionManagment.swift
//  Royal Keep
//
//  Created by ArmanG on 4/26/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import Foundation
import UIKit

struct UserActionManagment{
    enum ActionType {
        case addCheckList
        case addCheckListItem
        case cancelOperation
    }
    var vc: AddChecklistViewController!
    var addItemText: String = ""
  //  var itemTextView = UITextView()
   
    init(viewController: AddChecklistViewController) {
        self.vc = viewController
    }
    var result = Bool()
    
    mutating func reportObjectState(scope: String) {
        print("******************************************************************************")        
        print("the used scope in: \n")
        print("\(scope)\n")
        
        print("______________( Information In Type )______________________\n")
        print("addItemText: \(addItemText)\n")
        print("\(actionChecker(for: .addCheckList))\n")
        
        print("******************************************************************************\n")
    }
    mutating func actionChecker(for action: ActionType) -> Bool {
        print("IN ACTION CHECKER: \(addItemText)")
        
        switch action {
        case .addCheckListItem:
            if addItemText.isEmpty {
                result =  false
            } else if addItemText.trimmingCharacters(in: CharacterSet.whitespaces).count == 0 {
                   result =  false
                } else {
                    result =  true
                }
        case .addCheckList:
            if vc.numberOfRows == 0 {
                print("1")
                result = true
            } else if addItemText.isEmpty || addItemText.trimmingCharacters(in: CharacterSet.whitespaces).count == 0 {
                print("2")
                result = false
            } else if vc.numberOfRows > 0 && vc.itemToEdit != nil {
                print("3")
                result = true
            } else {
                result = true
            }
            print("")
        case .cancelOperation:
            print("")
            }
//            print(vc.secondTextView.text)
//            if vc.firstTxtView.text.isEmpty {
//                    return "عنوان نباید خالی باشد"
//                } else if vc.firstTxtView.text!.trimmingCharacters(in: CharacterSet.whitespaces).count == 0 {
//                    return  "فضای خالی نباید عنوان باشد"
//                } else {
//                    return  nil
//                }
        print("BEFOR RETURN RESULT: \(result)")
        return result
    }
}
