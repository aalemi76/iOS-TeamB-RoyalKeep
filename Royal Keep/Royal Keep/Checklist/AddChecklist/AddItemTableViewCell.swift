//
//  AddItemTableViewCell.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/17/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import UIKit
protocol AddItemTableViewCellDelegate: class {
    func moreButtonTouchUpInside(_ cell: AddItemTableViewCell)
    func deleteButtonTouchUpInside(_ cell: AddItemTableViewCell)
}
class AddItemTableViewCell: UITableViewCell {
    //MARK: - Properties
    var titleLabel = UILabel()
    var titleText = UITextView()
    var descriptionLabel = UILabel()
    var descriptionText = UITextView()
    var expendButton = UIButton(type: .system)
    var deleteButton = UIButton(type: .system)
    var isExpanded = false {
        willSet {
            if newValue {
                expendButton.setBackgroundImage(UIImage(systemName: "chevron.up.circle.fill"), for: .normal)
                titleLabel.text = "عنوان"
            } else {
                expendButton.setBackgroundImage(UIImage(systemName: "chevron.down.circle.fill"), for: .normal)
            }
        }
    }
    static let reuseID = "AddItemTableViewCell"
    weak var delegate: AddItemTableViewCellDelegate?
    @objc func didTapMoreButton(sender: UIButton) {
        delegate?.moreButtonTouchUpInside(self)
    }
    @objc func didTapDeleteButton(sender: UIButton) {
        delegate?.deleteButtonTouchUpInside(self)
    }
    //MARK: - View life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        sizeToFit()
        layoutIfNeeded()
        configureContentView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK: - Handlers
    func configureContentView() {
        contentView.sizeToFit()
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor)])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: bounds.height)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
        setExpendButton()
        setDeleteButton()
        setTitleLabel()
    }
    func addComponents() {
        setTitleTextView()
        setDescriptionLabel()
        setDescriptionTextView()
    }
    func removeComponents() {
        titleText.removeFromSuperview()
        descriptionLabel.removeFromSuperview()
        descriptionText.removeFromSuperview()
    }
    func setTitleLabel() {
        titleLabel.text = "عنوان"
        titleLabel.adjustsFontSizeToFitWidth = false
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.font = GlobalSettings.shared().boldSystemFont(size: 16)
        titleLabel.textColor = GlobalSettings.shared().darkGray
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: deleteButton.leadingAnchor, constant: -10),
            titleLabel.leadingAnchor.constraint(equalTo: expendButton.trailingAnchor, constant: 10)])
    }
    func setTitleTextView() {
        titleText.sizeToFit()
        titleText.isScrollEnabled = false
        titleText.backgroundColor = .clear
        titleText.isUserInteractionEnabled = true
        titleText.translatesAutoresizingMaskIntoConstraints = false
        titleText.font = GlobalSettings.shared().systemFont(size: 15)
        titleText.textColor = .black
        titleText.textAlignment = .right
        contentView.addSubview(titleText)
        NSLayoutConstraint.activate([
            titleText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            titleText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        makeRound(titleText)
    }
    func setDescriptionLabel() {
        descriptionLabel.font = GlobalSettings.shared().boldSystemFont(size: 15)
        descriptionLabel.textColor = GlobalSettings.shared().darkGray
        descriptionLabel.text = "توضیحات"
        descriptionLabel.textAlignment = .right
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleText.bottomAnchor , constant: 15 ),
            descriptionLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)])
    }
    func setDescriptionTextView() {
        descriptionText.sizeToFit()
        descriptionText.layoutIfNeeded()
        descriptionText.isScrollEnabled = false
        descriptionText.backgroundColor = .clear
        descriptionText.isUserInteractionEnabled = true
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.font = GlobalSettings.shared().systemFont(size: 14)
        descriptionText.textColor = GlobalSettings.shared().darkGray
        descriptionText.textAlignment = .right
        contentView.addSubview(descriptionText)
        NSLayoutConstraint.activate([
            descriptionText.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10),
            descriptionText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            descriptionText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            descriptionText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)])
        makeRound(descriptionText)
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
    func setExpendButton() {
        expendButton.setBackgroundImage(UIImage(systemName: "chevron.down.circle.fill"), for: .normal)
        expendButton.tintColor = GlobalSettings.shared().mainColor
        contentView.addSubview(expendButton)
        expendButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            expendButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            expendButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            expendButton.widthAnchor.constraint(equalToConstant: 30),
            expendButton.heightAnchor.constraint(equalToConstant: 30)])
        expendButton.addTarget(self, action: #selector(didTapMoreButton(sender:)), for: .touchUpInside)
    }
    func setDeleteButton() {
        deleteButton.setBackgroundImage(UIImage(systemName: "multiply.circle.fill"), for: .normal)
        deleteButton.tintColor = .systemRed
        deleteButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(deleteButton)
        NSLayoutConstraint.activate([
            deleteButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            deleteButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            deleteButton.widthAnchor.constraint(equalToConstant: 30),
            deleteButton.heightAnchor.constraint(equalToConstant: 30)])
        deleteButton.addTarget(self, action: #selector(didTapDeleteButton(sender:)), for: .touchUpInside)
    }
}
