//
//  Checklist.swift
//  Royal Keep
//
//  Created by Catalina on 7/4/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import Foundation
class Checklist: NSObject, NSCoding, Codable {
    var Title: String?
    var Description: String?
    var CreationDate: String?
    var LabelID: String?
    var CheckListItems: [ChecklistItem]?
    var Category: Int?
    var CheckListID: String?
    var reminderDate: Date?
    init(Title: String?, Description: String?, CreationDate: String?, LabelID: String?, CheckListItems: [ChecklistItem]?, Category: Int?, CheckListID: String?) {
        self.Title = Title
        self.Description = Description
        self.CreationDate = CreationDate
        self.LabelID = LabelID
        self.CheckListItems = CheckListItems
        self.Category = Category
        self.CheckListID = CheckListID
    }
    init(Title: String?, Description: String?, LabelID: String?, CheckListItems: [ChecklistItem]?, Category: Int?, CheckListID: String?) {
        self.Title = Title
        self.Description = Description
        self.LabelID = LabelID
        self.CheckListItems = CheckListItems
        self.Category = Category
        self.CheckListID = CheckListID
    }
    init(Title: String?, Description: String?, CreationDate: String?, LabelID: String?, CheckListItems: [ChecklistItem]?) {
        self.Title = Title
        self.Description = Description
        self.CreationDate = CreationDate
        self.LabelID = LabelID
        self.CheckListItems = CheckListItems
    }
    func encode(with coder: NSCoder) {
        coder.encode(Title, forKey: "Title")
        coder.encode(Description, forKey: "Description")
        coder.encode(CreationDate, forKey: "CreationDate")
        coder.encode(LabelID, forKey: "LabelID")
        coder.encode(CheckListItems, forKey: "CheckListItems")
        coder.encode(Category, forKey: "Category")
        coder.encode(CheckListID, forKey: "CheckListID")
        coder.encode(reminderDate, forKey: "reminderDate")
    }
    required init?(coder: NSCoder) {
        Title = coder.decodeObject(forKey: "Title") as! String?
        Description = coder.decodeObject(forKey: "Description") as! String?
        CreationDate = coder.decodeObject(forKey: "CreationDate") as! String?
        LabelID = coder.decodeObject(forKey: "LabelID") as! String?
        CheckListItems = coder.decodeObject(forKey: "CheckListItems") as! [ChecklistItem]?
        Category = coder.decodeObject(forKey: "Category") as! Int?
        CheckListID = coder.decodeObject(forKey: "CheckListID") as! String?
        reminderDate = coder.decodeObject(forKey: "reminderDate") as! Date?
        super.init()
    }
    func hasItem() -> Bool {
        guard let items = CheckListItems, items.count != 0 else {return false}
        return true
    }
    func isChecklistCompleted() -> Bool {
        guard let items = CheckListItems, items.count != 0 else {return false}
        for item in items {
            if !item.Completed! {
                return false
            }
        }
        return true
    }
    func toggleChecklistItems(state: Bool) {
        guard let items = CheckListItems, items.count != 0 else {return}
        for item in items {
            item.Completed = state
        }
    }
}
