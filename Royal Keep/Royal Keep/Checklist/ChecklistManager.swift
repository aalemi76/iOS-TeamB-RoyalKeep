//
//  ChecklistManager.swift
//  Royal Keep
//
//  Created by Amirhossein matloubi on 4/14/1399 AP.
//  Copyright © 1399 Chargoon. All rights reserved.
//

import Foundation
import SwiftyJSON


struct CheckListManager {
    let url = RKAPI.checklistsURL
        func setFinalArrayOfModels(data:Data) throws ->[Checklist] {
            let json = getJson(data: data)
            let parsedJsonData = generateDic(for: json)
            if let nonOptionalJsonData = parsedJsonData {
                do {
                    let finalArrayOfModels = try checklistModel(with: nonOptionalJsonData)
                    return finalArrayOfModels
                } catch (let error) {throw error}
            } else {throw Error.notes}
        }
        
        private func checklistInits (data:[String:Any]) throws -> Checklist {
            guard let title = data["Title"] as? String else {
                throw Error.checklistTitle
            }
            let description = data["Description"] as? String
            guard let creationDate = data["CreationDate"] as? String else {
                throw Error.checklistDate }
            let labelId = data["LabelID"] as? String
            let checklistId = data["CheckListID"] as? String
            let category = data["Category"] as? Int
            var checklistItems = [ChecklistItem]()
            if let items = data["CheckListItems"] as? [[String: Any]] {
                for item in  items {
                    let temp = ChecklistItem(Title: item["Title"] as? String, Description: item["Description"] as? String, Completed: item["Completed"] as? Bool, ID: item["ID"] as? String)
                    checklistItems.append(temp)
                }
            }
            let model = Checklist(Title: title, Description: description, CreationDate: creationDate, LabelID: labelId, CheckListItems: checklistItems, Category :category, CheckListID: checklistId)
            return model
        }
        private  func getJson(data:Data) -> JSON {
            let json = JSON(data)
            return json
        }
        private   func generateDic(for jsonData:JSON) -> [[String:Any]]? {
            
            if let dic = jsonData.arrayObject as? [[String:Any]] {
                return dic
            }else {
                return nil
            }
        }
        private func checklistModel(with dic:[[String:Any]]) throws -> [Checklist] {
            var models = [Checklist]()
            if !dic.isEmpty {
                let counter = dic.count - 1
                for i in 0...counter {
                    let j = dic[i]
                    do {
                        let modeledData = try checklistInits(data: j)
                        models.append(modeledData)
                    }catch(let error) {throw error}
                }
                return models
            } else {throw Error.checkListIsEmpty}
        }
    static func makeChecklist(jsonObject: JSON) -> Checklist? {
        if let title = jsonObject["Title"].string, let checklistId = jsonObject["CheckListID"].string, let creationDate = jsonObject["CreationDate"].string {
            let description = jsonObject["Description"].string
            let labelId = jsonObject["LabelID"].string
            let category = jsonObject["Category"].int
            var checklistItems = [ChecklistItem]()
            if let items = jsonObject["CheckListItems"].array {
                for item in  items {
                    let temp = ChecklistItem(Title: item["Title"].string, Description: item["Description"].string, Completed: item["Completed"].bool, ID: item["ID"].string)
                    checklistItems.append(temp)
                }
            }
            let checklist = Checklist(Title: title, Description: description, CreationDate: creationDate, LabelID: labelId, CheckListItems: checklistItems, Category: category, CheckListID: checklistId)
            return checklist
        } else {
            return nil
        }
    }
}
