//
//  ChecklistItem.swift
//  Royal Keep
//
//  Created by Catalina on 7/4/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import Foundation
class ChecklistItem: NSObject, NSCoding, Codable {
    var Title: String?
    var Description: String?
    var Completed: Bool?
    var ID: String?
    init(Title: String?, Description: String?, Completed: Bool?, ID: String?) {
        self.Title = Title
        self.Description = Description
        self.Completed = Completed
        self.ID = ID
    }
    init(Title: String?, Description: String?) {
        self.Title = Title
        self.Description = Description
    }
    func encode(with coder: NSCoder) {
        coder.encode(Title, forKey: "Title")
        coder.encode(Description, forKey: "Description")
        coder.encode(Completed, forKey: "Completed")
        coder.encode(ID, forKey: "ID")
    }
    required init?(coder: NSCoder) {
        Title = coder.decodeObject(forKey: "Title") as! String?
        Description = coder.decodeObject(forKey: "Description") as! String?
        Completed = coder.decodeObject(forKey: "Completed") as! Bool?
        ID = coder.decodeObject(forKey: "ID") as! String?
        super.init()
    }
}
