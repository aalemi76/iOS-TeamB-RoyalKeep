//
//  ChecklistItemTableViewCell.swift
//  Royal Keep
//
//  Created by Catalina on 7/13/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
protocol ChecklistItemTableViewCellDelegate: class {
    func moreButtonTouchUpInside(_ cell: ChecklistItemTableViewCell)
}
class ChecklistItemTableViewCell: UITableViewCell {
    //MARK: - Properties
    static let reuseID = "ChecklistItemTableViewCell"
    weak var delegate: ChecklistItemTableViewCellDelegate?
    var titleLabel = UILabel()
    var titleText = UITextView()
    var descriptionLabel = UILabel()
    var descriptionText = UITextView()
    var moreButton = UIButton(type: .system)
    var completionButton = UIButton(type: .system)
    var isCompleted: Bool = false {
        didSet {
            configureCompletionButton()
        }
    }
    var isExpanded = false {
        willSet {
            if newValue {
                moreButton.setBackgroundImage(UIImage(systemName: "chevron.up.circle.fill"), for: .normal)
                titleLabel.text = "عنوان"
            } else {
                moreButton.setBackgroundImage(UIImage(systemName: "chevron.down.circle.fill"), for: .normal)
            }
        }
    }
    @objc func didTapMoreButton(sender: UIButton) {
        delegate?.moreButtonTouchUpInside(self)
    }
    //MARK: - View life Cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        sizeToFit()
        layoutIfNeeded()
        configureContentView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //MARK: - Handlers
    func configureContentView() {
        contentView.sizeToFit()
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            contentView.widthAnchor.constraint(equalTo: widthAnchor)])
        let heightAnchor = contentView.heightAnchor.constraint(greaterThanOrEqualToConstant: bounds.height)
        heightAnchor.priority = UILayoutPriority(rawValue: 249)
        heightAnchor.isActive = true
        addMoreButton()
        addCompletionButton()
        addTitleLabel()
    }
    func addComponents() {
        addTitleTextView()
        addDescriptionLabel()
        addDescriptionTextView()
    }
    func removeComponents() {
        titleText.removeFromSuperview()
        descriptionLabel.removeFromSuperview()
        descriptionText.removeFromSuperview()
    }
    func addTitleLabel() {
        titleLabel.text = "عنوان"
        titleLabel.adjustsFontSizeToFitWidth = false
        titleLabel.lineBreakMode = .byTruncatingTail
        titleLabel.font = GlobalSettings.shared().boldSystemFont(size: 16)
        titleLabel.textColor = GlobalSettings.shared().darkGray
        titleLabel.textAlignment = .center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: completionButton.leadingAnchor, constant: -10),
            titleLabel.leadingAnchor.constraint(equalTo: moreButton.trailingAnchor, constant: 10)])
    }
    func addTitleTextView() {
        titleText.sizeToFit()
        titleText.isScrollEnabled = false
        titleText.backgroundColor = .clear
        titleText.isUserInteractionEnabled = false
        titleText.translatesAutoresizingMaskIntoConstraints = false
        titleText.font = GlobalSettings.shared().systemFont(size: 15)
        titleText.textColor = .black
        titleText.textAlignment = .right
        contentView.addSubview(titleText)
        NSLayoutConstraint.activate([
            titleText.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            titleText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20)
        ])
        makeRound(titleText)
    }
    func addDescriptionLabel() {
        descriptionLabel.font = GlobalSettings.shared().boldSystemFont(size: 15)
        descriptionLabel.textColor = GlobalSettings.shared().darkGray
        descriptionLabel.text = "توضیحات"
        descriptionLabel.textAlignment = .right
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(descriptionLabel)
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleText.bottomAnchor , constant: 15 ),
            descriptionLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)])
    }
    func addDescriptionTextView() {
        descriptionText.sizeToFit()
        descriptionText.layoutIfNeeded()
        descriptionText.isScrollEnabled = false
        descriptionText.backgroundColor = .clear
        descriptionText.isUserInteractionEnabled = false
        descriptionText.translatesAutoresizingMaskIntoConstraints = false
        descriptionText.font = GlobalSettings.shared().systemFont(size: 14)
        descriptionText.textColor = GlobalSettings.shared().darkGray
        descriptionText.textAlignment = .right
        contentView.addSubview(descriptionText)
        NSLayoutConstraint.activate([
            descriptionText.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10),
            descriptionText.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            descriptionText.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            descriptionText.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20)])
        makeRound(descriptionText)
    }
    func addMoreButton() {
        moreButton.setBackgroundImage(UIImage(systemName: "chevron.down.circle.fill"), for: .normal)
        moreButton.tintColor = GlobalSettings.shared().mainColor
        contentView.addSubview(moreButton)
        moreButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            moreButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            moreButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            moreButton.widthAnchor.constraint(equalToConstant: 30),
            moreButton.heightAnchor.constraint(equalToConstant: 30)])
        moreButton.addTarget(self, action: #selector(didTapMoreButton(sender:)), for: .touchUpInside)
    }
    func addCompletionButton() {
        completionButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(completionButton)
        NSLayoutConstraint.activate([
            completionButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            completionButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
            completionButton.widthAnchor.constraint(equalToConstant: 30),
            completionButton.heightAnchor.constraint(equalToConstant: 30)])
    }
    func configureCompletionButton() {
        if isCompleted {
            completionButton.setBackgroundImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
            completionButton.tintColor = GlobalSettings.shared().darkGreen
        } else {
            completionButton.setBackgroundImage(UIImage(systemName: "xmark.circle.fill"), for: .normal)
            completionButton.tintColor = GlobalSettings.shared().mainColor
        }
    }
    func makeRound(_ view: UIView, cornerRadius: CGFloat = 10){
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
        view.layer.borderColor = GlobalSettings.shared().darkGray.cgColor
        view.layer.borderWidth = 1
    }
}
