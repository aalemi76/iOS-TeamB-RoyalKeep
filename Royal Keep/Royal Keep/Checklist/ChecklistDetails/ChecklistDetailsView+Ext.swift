//
//  ChecklistDetailsView+Ext.swift
//  Royal Keep
//
//  Created by Catalina on 7/13/20.
//  Copyright © 2020 Chargoon. All rights reserved.
//

import UIKit
extension ChecklistDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklistItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = checklistItems[indexPath.row]
        let cell = ChecklistItemTableViewCell()
        cell.delegate = self
        cell.titleLabel.text = item.Title
        cell.titleText.text = item.Title
        cell.descriptionText.text = item.Description
        cell.isCompleted = item.Completed!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isChecklistEdited = true
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! ChecklistItemTableViewCell
        let item = checklist.CheckListItems![indexPath.row]
        editChecklist(item: item, cell: cell)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        let label = UILabel()
        label.text = "لیست آیتم ها"
        label.font = GlobalSettings.shared().boldSystemFont(size: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15)])
        completeAll.setTitleColor(GlobalSettings.shared().mainColor, for: .normal)
        completeAll.titleLabel?.font = GlobalSettings.shared().boldSystemFont(size: 14)
        completeAll.addTarget(self, action: #selector(didTapCompleteAll(sender:)), for: .touchUpInside)
        completeAll.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(completeAll)
        NSLayoutConstraint.activate([
            completeAll.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            completeAll.centerYAnchor.constraint(equalTo: view.centerYAnchor)])
        return view
    }
}
extension ChecklistDetailsViewController: ChecklistItemTableViewCellDelegate {
    func moreButtonTouchUpInside(_ cell: ChecklistItemTableViewCell) {
        if cell.isExpanded {
            cell.isExpanded = false
            cell.titleText.text != "" ? (cell.titleLabel.text = cell.titleText.text) : (cell.titleLabel.text = "عنوان")
            cell.removeComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        } else {
            cell.isExpanded = true
            cell.addComponents()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        }
    }
}
extension ChecklistDetailsViewController: AddChecklistViewControllerDelegate {
    func addNewChecklist(_ controller: AddChecklistViewController, didAddItem item: Checklist?, at indexPath: IndexPath?) {
        getChecklist()
        guard let labelID = item?.LabelID else {
            checkListLabel = nil
            return}
        for label in labels {
            if label.LabelID == labelID {
                checkListLabel = label
                break
            }
        }
    }
}
extension ChecklistDetailsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}
